#include "st.task.h"

bool STTask::isOutdated() {
    if (!isComplete) {
        return QDate::currentDate() > date;
    } else {
        return false;
    }
}
