#include "st.icalexporter.h"

#include <QtCore>
#include <QMessageBox>

STICalExporter::STICalExporter(STDataProvider *inProvider, QObject *inParent)
    : QObject(inParent), provider(inProvider) {
    qsrand(QDateTime::currentMSecsSinceEpoch());
}

void STICalExporter::setProvider(STDataProvider *inValue) {
    provider = inValue;
}

void STICalExporter::exportSchedule(QString inFileName) {
    if (provider == NULL) {
        qDebug() << "Provider is NULL";
        return;
    }

    QFile file(inFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text |
                   QIODevice::Truncate)) {
        qDebug() << "Can't open file for writing";
        return;
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");

    out << "BEGIN:VCALENDAR" << endl;
    out << "PRODID:-//iStodo team//iStodo Schedule 1.0" << endl;
    out << "VERSION:2.0" << endl;
    out << "CALSCALE:GREGORIAN" << endl;
    out << "METHOD:PUBLISH" << endl;
    out << "X-WR-CALNAME:iStodo schedule" << endl;
    out << "X-WR-TIMEZONE:UTC" << endl;

    QVector<QTime> *bells = provider->getBells();
    int pairLength = provider->getPairLength();
    QDate endDate = provider->getEnd();
    int cycleWeekCount = provider->getCountOfWeek();

    int uid = 0;

    for (QDate date = provider->getBegin(); date < endDate;
         date = date.addDays(1)) {

        int cycleWeekNumber = provider->getWeekFromDate(date) % cycleWeekCount;
        if (cycleWeekNumber == 0) {
            cycleWeekNumber = cycleWeekCount;
        }

        QVector<STPair *> *schedule = provider->getSheduleOnDay(
                    cycleWeekNumber, date.dayOfWeek());

        STPair *pair = NULL;
        STTeacher *teacher = NULL;
        STSubject *subject = NULL;
        STPair::Type *type = NULL;
        for (int i = 0; i < schedule->size(); ++i) {
            pair = schedule->at(i);

            if (!pair) {
                qDebug() << "Error: can't get pair from provider";
                return;
            }

            if (pair->teacherId != INVALID_ID) {
                teacher = provider->getTeacher(pair->teacherId);
            } else {
                teacher = NULL;
            }

            if (pair->subjectId != INVALID_ID) {
                subject = provider->getSubject(pair->subjectId);
            } else {
                subject = NULL;
            }

            if (pair->typeId != INVALID_ID) {
                type = provider->getPairType(pair->typeId);
            } else {
                type = NULL;
            }

            QString format = "yyyyMMddThhmmss";
            QDateTime dateTime(date, bells->at(pair->gridNumber - 1));
            dateTime = dateTime.toUTC();

            out << "BEGIN:VEVENT" << endl;
            out << "DTSTART:" << dateTime.toString(format) << "Z" << endl;
            dateTime = dateTime.addSecs(pairLength * 60);
            out << "DTEND:" << dateTime.toString(format) << "Z" << endl;
            dateTime = QDateTime::currentDateTimeUtc();
            out << "DTSTAMP:" << dateTime.toString(format) << "Z" << endl;
            out << "UID:"
                << "STPair_" << uid << "@istodo.ru" << endl;
            out << "CREATED:" << dateTime.toString(format) << "Z" << endl;
            out << "DESCRIPTION:";

            if (subject) {
                if (!subject->name.isEmpty() && !subject->name.isNull()) {
                    out << subject->name << " ";
                } else {
                    out << subject->nick << " ";
                }
            } else {
                out << "unknow subject ";
            }

            if (type) {
                if (!type->fullName.isEmpty() && !type->fullName.isNull()) {
                    out << type->fullName << " ";
                } else {
                    out << type->shortName << " ";
                }
            } else {
                out << "unknow pairtype ";
            }

            if (teacher) {
                if (!teacher->name.isEmpty() && !teacher->name.isNull()) {
                    out << teacher->name << " ";
                } else {
                    out << teacher->nick << " ";
                }
            } else {
                out << "unknow teacher ";
            }

            out << pair->auditory << " " << pair->additions << endl;
            out << "LAST-MODIFIED:" << dateTime.toString(format) << "Z" << endl;
            out << "LOCATION:" << endl;
            out << "SEQUENCE:0" << endl;
            out << "STATUS:CONFIRMED" << endl;

            out << "SUMMARY:";
            if (subject) {
                out << subject->nick << " ";
            }
            if (type) {
                out << type->shortName << " ";
            }
            out << pair->auditory << " ";
            out << endl;

            out << "TRANSP:OPAQUE" << endl;
            out << "END:VEVENT" << endl;

            if (teacher) {
                delete teacher;
            }
            if (subject) {
                delete subject;
            }
            if (type) {
                delete type;
            }

            uid++;
        }

        clearQVector<STPair>(*schedule);
        delete schedule;
    }
    delete bells;

    out << "END:VCALENDAR" << endl;
}

void STICalExporter::exportTasks(QString inFileName) {
    if (provider == NULL) {
        qDebug() << "Provider is NULL";
        return;
    }

    QFile file(inFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text |
                   QIODevice::Truncate)) {
        qDebug() << "Can't open file for writing";
        return;
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");

    out << "BEGIN:VCALENDAR" << endl;
    out << "PRODID:-//iStodo team//iStodo Tasks 1.0" << endl;
    out << "VERSION:2.0" << endl;
    out << "CALSCALE:GREGORIAN" << endl;
    out << "METHOD:PUBLISH" << endl;
    out << "X-WR-CALNAME:iStodo tasks" << endl;

    QVector<STTask *> *tasks = provider->getAllTasks();
    int uid = 0;

    STTask *task = NULL;
    STSubject *subject = NULL;
    for (int i = 0; i < tasks->size(); ++i) {
        task = tasks->at(i);

        if (!task) {
            qDebug() << "Can't get task from provider";
            return;
        }

        // Check complete
        if (task->isComplete) {
            continue;
        }

        if (task->subjectId != INVALID_ID) {
            subject = provider->getSubject(task->subjectId);
        } else {
            subject = NULL;
        }

        QString format = "yyyyMMddThhmmss";
        QDateTime dateTime(task->date);
        dateTime = dateTime.toUTC();

        out << "BEGIN:VTODO" << endl;
        out << "UID:"
            << "STTask_" << uid << "@istodo.ru" << endl;
        out << "DTSTART:" << dateTime.toString(format) << "Z" << endl;
        out << "DUE:" << dateTime.toString(format) << "Z" << endl;
        dateTime = QDateTime::currentDateTimeUtc();
        out << "DTSTAMP:" << dateTime.toString(format) << "Z" << endl;

        out << "SUMMARY:";
        out << task->task << ": ";

        if (subject) {
            if (!subject->name.isEmpty() && !subject->name.isNull()) {
                out << subject->name << endl;
            } else {
                out << subject->nick << endl;
            }
        } else {
            out << tr("General") << endl;
        }

        out << "CLASS:CONFIDENTIAL" << endl;
        out << "CATEGORIES:EDUCATION" << endl;

        out << "PRIORITY:";
        // switch info: http://www.kanzaki.com/docs/ical/priority.html
        switch (task->prior) {
            case STTask::LOW: {
                out << 8;
                break;
            }
            case STTask::NORMAL: {
                out << 5;
                break;
            }
            case STTask::HIGH: {
                out << 3;
                break;
            }
            case STTask::ALARM: {
                out << 1;
                break;
            }
            default: {
                qDebug() << "Unknow priority";
                out << 0;
                break;
            }
        }
        out << endl;

        if (task->isComplete) {
            out << "STATUS:COMPLETED" << endl;
        } else {
            out << "STATUS:NEEDS-ACTION" << endl;
        }

        out << "END:VTODO" << endl;

        if (subject) {
            delete subject;
        }

        uid++;
    }

    clearQVector<STTask>(*tasks);
    delete tasks;

    out << "END:VCALENDAR" << endl;
}
