/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_PAIR_H
#define ST_PAIR_H

#include <QString>

class STPair {
    public:
        struct Type {
            public:
                int id;

                QString fullName;
                QString shortName;
        };

        int id;

        int subjectId;
        int typeId;
        int teacherId;

        int week;
        int dayOfWeek;
        int gridNumber;

        QString auditory;
        QString additions;
};
#endif  // ST_PAIR_H
