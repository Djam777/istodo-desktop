/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_SCRIPT_H
#define ST_SCRIPT_H

#define DP_CREATE_GLOBAL "\
CREATE TABLE IF NOT EXISTS STGlobal  \
( \
    num INTEGER Primary Key, \
    data INTEGER \
);"

#define DP_CREATE_BELLS "\
CREATE TABLE IF NOT EXISTS STBells  \
( \
    num INTEGER Primary Key, \
    h INTEGER, \
    m INTEGER \
);"

#define DP_CREATE_PAIRTYPE "\
CREATE TABLE IF NOT EXISTS STPairType \
( \
    id INTEGER Primary Key, \
    fullName TEXT, \
    shortName TEXT, \
    check( shortName is NOT NULL and shortName<>\'\' ) \
);"

#define DP_CREATE_TEACHER "\
CREATE TABLE IF NOT EXISTS STTeacher \
( \
    id INTEGER Primary Key, \
    name TEXT, \
    nick TEXT, \
    additions TEXT, \
    check( nick is NOT NULL and nick<>\'\' ) \
);"

#define DP_CREATE_SUBJECT "\
CREATE TABLE IF NOT EXISTS STSubject \
( \
    id INTEGER Primary Key, \
    name TEXT, \
    nick TEXT, \
    check( nick is NOT NULL and nick<>\'\' ) \
);"

#define DP_CREATE_TASK "\
CREATE TABLE IF NOT EXISTS STTask  \
( \
    id INTEGER Primary Key, \
    subjectId INTEGER, \
    seriesNumber INTEGER, \
    isComplete BOOLEAN, \
    dateD INTEGER, \
    dateM INTEGER, \
    dateY INTEGER, \
    task TEXT, \
    description TEXT, \
    prior INTEGER, \
    check( task is NOT NULL and task<>\'\' ), \
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade \
);"

#define DP_CREATE_PAIR "\
CREATE TABLE IF NOT EXISTS STPair \
( \
    id INTEGER Primary Key, \
    subjectId INTEGER NOT NULL, \
    typeId INTEGER, \
    teacherId INTEGER, \
    week INTEGER, \
    dayOfWeek INTEGER, \
    gridNumber INTEGER, \
    auditory TEXT, \
    additions TEXT, \
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade, \
    foreign Key (typeId) references STPairType (id) on delete cascade on update cascade, \
    foreign Key (teacherId) references STTeacher (id) on delete cascade on update cascade, \
    foreign Key (gridNumber) references STBells (num) on delete cascade on update cascade \
);"

#endif // ST_SCRIPT_H
