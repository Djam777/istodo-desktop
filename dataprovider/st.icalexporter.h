/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_ICALEXPORTER_H
#define ST_ICALEXPORTER_H

#include <QObject>
#include "st.dataprovider.h"

class STICalExporter : public QObject {
        Q_OBJECT

    public:
        explicit STICalExporter(STDataProvider *inProvider = 0,
                                QObject *inParent = 0);
        void setProvider(STDataProvider *inValue);

        void exportSchedule(QString inFileName);
        void exportTasks(QString inFileName);

    private:
        STDataProvider *provider;
};

#endif  // ST_ICALEXPORTER_H
