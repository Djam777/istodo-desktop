INCLUDEPATH += $$PWD/..

QT += core sql

SOURCES += \
    $$PWD/st.task.cpp \
    $$PWD/st.dataprovider.cpp \
    $$PWD/st.icalexporter.cpp

HEADERS += \
    $$PWD/st.teacher.h \
    $$PWD/st.task.h \
    $$PWD/st.subject.h \
    $$PWD/st.pair.h \
    $$PWD/st.dataprovider.h \
    $$PWD/st.script.h \
    $$PWD/st.icalexporter.h
