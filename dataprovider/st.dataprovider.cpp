#include "st.dataprovider.h"
#include "st.script.h"

#define DP_QUERY_ERROR \
    qFatal("%s", query.lastError().text().toStdString().c_str())

STDataProvider::STDataProvider(QString inName) {
    // Get name
    if (inName.isEmpty() || inName.isNull()) {
        QString path =
                QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(path).exists()) {
            QDir().mkpath(path);
        }
        inName = path + QDir::separator() + DB_NAME;
    } else {
        inName = DB_NAME;
    }
    // Open database
    // qDebug() << "Database name:" << inName;

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(inName);
    if (!db.open()) {
        qFatal("%s", db.lastError().text().toStdString().c_str());
    }

    // Alloc vectors
    bells = new QVector<QTime>;
    pairTypes = new QVector<STPair::Type*>;
    pairs = new QVector<STPair*>;
    subjects = new QVector<STSubject*>;
    tasks = new QVector<STTask*>;
    teachers = new QVector<STTeacher*>;

    // We can't translate static member or QStringList
    daysOfWeek[0] = "null"; daysOfWeek[1] = tr("Mo"); daysOfWeek[2] = tr("Tu");
    daysOfWeek[3] = tr("We"); daysOfWeek[4] = tr("Th"); daysOfWeek[5] = tr("Fr");
    daysOfWeek[6] = tr("Sa"); daysOfWeek[7] = tr("Su");

    // Init
    initFunc();
}

STDataProvider::~STDataProvider() {
    // Close database
    QSqlDatabase::database().close();

    // Dealoc vectors
    clearQVector<STPair::Type>(*pairTypes);
    clearQVector<STPair>(*pairs);
    clearQVector<STSubject>(*subjects);
    clearQVector<STTask>(*tasks);
    clearQVector<STTeacher>(*teachers);

    delete bells;
    delete pairTypes;
    delete pairs;
    delete subjects;
    delete tasks;
    delete teachers;
}

STDataProvider& STDataProvider::getInstance() {
    static STDataProvider singleton;
    return singleton;
}

bool STDataProvider::isEmpty() {
    QSqlQuery query;
    query.prepare("select count(num) from STGlobal;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    int count = query.value(0).toInt();
    if (count <= 1) {
        return true;
    }
    return false;
}

QString STDataProvider::getDatabaseName() {
    return QSqlDatabase::database().databaseName();
}

int STDataProvider::getVersion() {
    QSqlQuery query;
    query.prepare("select data from STGlobal where num=?;");
    query.addBindValue(version);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt();
}

QString *STDataProvider::getDaysOfWeek()
{
    return daysOfWeek;
}

int STDataProvider::getPairLength() {
    QSqlQuery query;
    query.prepare("select data from STGlobal where num=?;");
    query.addBindValue(pairLength);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt();
}

void STDataProvider::setPairLength(int inPairLength) {
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(pairLength);
    query.addBindValue(inPairLength);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit globalChanged();
}

QDate STDataProvider::getBegin() {
    QSqlQuery query;
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(dBegin);
    query.addBindValue(yBegin);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    int d = -1;
    int m = -1;
    int y = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case dBegin: {
                d = query.value(1).toInt();
                break;
            }
            case mBegin: {
                m = query.value(1).toInt();
                break;
            }
            case yBegin: {
                y = query.value(1).toInt();
                break;
            }
            default: { qFatal("Unknow num of key"); }
        }
    }

    return QDate(y, m, d);
}

void STDataProvider::setBegin(QDate inDate) {
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(dBegin);
    query.addBindValue(inDate.day());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(mBegin);
    query.addBindValue(inDate.month());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(yBegin);
    query.addBindValue(inDate.year());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit globalChanged();
}

QDate STDataProvider::getEnd() {
    QSqlQuery query;
    query.prepare("select num,data from STGlobal where num>=? and num<=?;");
    query.addBindValue(dEnd);
    query.addBindValue(yEnd);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    int d = -1;
    int m = -1;
    int y = -1;
    while (query.next()) {
        switch (query.value(0).toInt()) {
            case dEnd: {
                d = query.value(1).toInt();
                break;
            }
            case mEnd: {
                m = query.value(1).toInt();
                break;
            }
            case yEnd: {
                y = query.value(1).toInt();
                break;
            }
            default: { qFatal("Unknow num of key"); }
        }
    }

    return QDate(y, m, d);
}

void STDataProvider::setEnd(QDate inDate) {
    QSqlQuery query;
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(dEnd);
    query.addBindValue(inDate.day());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(mEnd);
    query.addBindValue(inDate.month());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(yEnd);
    query.addBindValue(inDate.year());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit globalChanged();
}

int STDataProvider::getCountOfWeek() {
    QSqlQuery query;
    query.prepare("select max(week) from STPair;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt();
}

QDate STDataProvider::getDateFromWeek(int inWeek, int inDayOfWeek) {
    QDate date = getBegin();

    int week = inWeek;

    if (date.dayOfWeek() != Qt::Monday) {
        while (date.dayOfWeek() != Qt::Monday) {
            if (date.dayOfWeek() == inDayOfWeek && inWeek == 1) {
                return date;
            }
            date = date.addDays(-1);
        }
        week--;
    }

    week--;

    date = date.addDays(7 * week);
    date = date.addDays(inDayOfWeek - 1);

    return date;
}

int STDataProvider::getWeekFromDate(QDate inDate) {
    QDate begin = getBegin();

    if (inDate < begin) {
        return -1;
    }

    int week = 1;
    if (begin.dayOfWeek() != Qt::Monday) {
        int dayOfWeek = begin.dayOfWeek();
        while (dayOfWeek != Qt::Monday) {
            if (begin == inDate) {
                return week;
            }
            begin = begin.addDays(1);
            dayOfWeek = begin.dayOfWeek();
        }

        week++;
    }

    return week + begin.daysTo(inDate) / 7;
}

QVector<QTime>* STDataProvider::getBells() {
    return new QVector<QTime>(*bells);
}

QTime STDataProvider::getBell(int inNum) {
    if (inNum >= bells->size()) {
        qFatal("Bad index for bell");
    }

    return bells->at(inNum);
}

int STDataProvider::addBell(QTime inTime) {
    QSqlQuery query;
    query.prepare("insert into STBells values (NULL,?,?);");
    query.addBindValue(inTime.hour());
    query.addBindValue(inTime.minute());
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    bells->append(inTime);

    emit bellsChanged();

    return query.lastInsertId().toInt();
}

void STDataProvider::modBell(int inBell, QTime inTime) {
    if (inBell >= bells->size()) {
        qFatal("Bad index for bell");
    }

    QSqlQuery query;
    query.prepare("update STBells set h=?, m=? where num=?;");
    query.addBindValue(inTime.hour());
    query.addBindValue(inTime.minute());
    query.addBindValue(inBell + 1);  // Number from 1
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    bells->replace(inBell, inTime);

    emit bellsChanged();
}

void STDataProvider::delBell() {
    QSqlQuery query;
    query.prepare(
                "delete from STBells where num in (select max(num) from STBells);");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    bells->pop_back();

    fillFunc();
}

QVector<STPair::Type*>* STDataProvider::getPairTypes() {
    return deepCopyQVector<STPair::Type>(*pairTypes);
}

QVector<STPair::Type*>* STDataProvider::getPairTypesOnSubject(
        STSubject inSubject) {
    QVector<STPair::Type*> ret;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->subjectId == inSubject.id) {
            int index = getIndexById(*pairTypes, pair->typeId);
            ret.append(pairTypes->at(index));
        }
    }
    return deepCopyQVector<STPair::Type>(ret);
}

STPair::Type* STDataProvider::getPairType(int inId) {
    int index = getIndexById(*pairTypes, inId);
    return new STPair::Type(*pairTypes->at(index));
}

int STDataProvider::addPairType(STPair::Type inPairType) {
    QSqlQuery query;
    query.prepare("insert into STPairType values (NULL,?,?);");
    query.addBindValue(inPairType.fullName);
    query.addBindValue(inPairType.shortName);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inPairType.id = query.lastInsertId().toInt();
    pairTypes->append(new STPair::Type(inPairType));

    emit pairTypesChanged();

    return inPairType.id;
}

void STDataProvider::modPairType(STPair::Type inPairType) {
    int index = getIndexById(*pairTypes, inPairType.id);
    delete pairTypes->at(index);
    pairTypes->replace(index, new STPair::Type(inPairType));

    QSqlQuery query;
    query.prepare("update STPairType set fullName=?, shortName=? where id=?;");
    query.addBindValue(inPairType.fullName);
    query.addBindValue(inPairType.shortName);
    query.addBindValue(inPairType.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit pairTypesChanged();
}

void STDataProvider::delPairType(STPair::Type inPairType) {
    delPairType(inPairType.id);
}

void STDataProvider::delPairType(int inId) {
    if (inId == INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*pairTypes, inId);
    delete pairTypes->at(index);
    pairTypes->remove(index);

    QSqlQuery query;
    query.prepare("delete from STPairType where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

QVector<STPair*>* STDataProvider::getSheduleOnDay(int inWeek, int inDayOfWeek) {
    QVector<STPair*> ret;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->week == inWeek && pair->dayOfWeek == inDayOfWeek) {
            ret.append(pair);
        }
    }

    return deepCopyQVector<STPair>(ret);
}

QVector<QVector<STPair*>*>* STDataProvider::getSheduleOnWeek(int inWeek) {
    // first vector have 7 elements, it's day of week
    // second vector is pair for day (don't sorted)
    QVector<QVector<STPair*>*> ret;
    for (int i = Qt::Monday; i <= Qt::Sunday; i++) {
        ret.push_back(getSheduleOnDay(inWeek, i));
    }

    return deepCopyQVector<QVector<STPair*> >(ret);
}

QVector<QString*>* STDataProvider::getAuditoryOnPair(STSubject inSubject,
                                                     STPair::Type inPairType) {
    QVector<QString*>* ret = new QVector<QString*>;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->subjectId == inSubject.id && pair->typeId == inPairType.id) {
            ret->append(new QString(pair->auditory));
        }
    }

    return ret;
}

QVector<QString*>* STDataProvider::getAuditoryOnSubject(STSubject inSubject) {
    QVector<QString*>* ret = new QVector<QString*>;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->subjectId == inSubject.id) {
            ret->append(new QString(pair->auditory));
        }
    }

    return ret;
}

STPair* STDataProvider::getPair(int inId) {
    int index = getIndexById(*pairs, inId);
    return new STPair(*pairs->at(index));
}

int STDataProvider::addPair(STPair inPair) {
    QSqlQuery query;
    query.prepare("insert into STPair values (NULL,?,?,?,?,?,?,?,?);");
    query.addBindValue(getVariantFromId(inPair.subjectId));
    query.addBindValue(getVariantFromId(inPair.typeId));
    query.addBindValue(getVariantFromId(inPair.teacherId));
    query.addBindValue(inPair.week);
    query.addBindValue(inPair.dayOfWeek);
    query.addBindValue(inPair.gridNumber);
    query.addBindValue(inPair.auditory);
    query.addBindValue(inPair.additions);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inPair.id = query.lastInsertId().toInt();
    pairs->append(new STPair(inPair));

    emit pairsChanged();

    return inPair.id;
}

void STDataProvider::modPair(STPair inPair) {
    int index = getIndexById(*pairs, inPair.id);
    delete pairs->at(index);
    pairs->replace(index, new STPair(inPair));

    QSqlQuery query;
    query.prepare(
                "update STPair set subjectId=?, typeId=?, teacherId=?, week=?, "
                "dayOfWeek=?, gridNumber=?, auditory=?, additions=? where id=?;");
    query.addBindValue(getVariantFromId(inPair.subjectId));
    query.addBindValue(getVariantFromId(inPair.typeId));
    query.addBindValue(getVariantFromId(inPair.teacherId));
    query.addBindValue(inPair.week);
    query.addBindValue(inPair.dayOfWeek);
    query.addBindValue(inPair.gridNumber);
    query.addBindValue(inPair.auditory);
    query.addBindValue(inPair.additions);
    query.addBindValue(inPair.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit pairsChanged();
}

void STDataProvider::delScheduleOnWeek(int inWeek) {
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->week == inWeek) {
            delete pair;
            pairs->remove(i);
            i = 0;
            // WARNING: bad method i=0
        }
        if (pair->week > inWeek) {
            pair->week--;
        }
    }

    QSqlQuery query;
    query.prepare("delete from STPair where week=?;");
    query.addBindValue(inWeek);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("update STPair set week=week-1 where week>=?;");
    query.addBindValue(inWeek);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit pairsChanged();
    // Don't use fillFunc() here!
    // BUG: Если использовать fillFunc(), то будет бага qt
    // при удаление пары. На самом деле при удаление пары
    // каскадов нет, поэтому перезаполнять векторы не обязательно.
    // fillFunc();
}

void STDataProvider::cloneWeek(int inWeek) {
    // Copy week with week number, and past new week before inWeek
    QVector<STPair*> newPairs;

    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->week == inWeek) {
            newPairs.append(new STPair(*pair));
        }
        if (pair->week >= inWeek) {
            pair->week++;
        }
    }

    QSqlQuery query;
    query.prepare("update STPair set week=week+1 where week>=?;");
    query.addBindValue(inWeek);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    for (int i = 0; i < newPairs.size(); i++) {
        this->addPair(*newPairs[i]);
    }

    emit pairsChanged();
}

void STDataProvider::delPair(STPair inPair) { delPair(inPair.id); }

void STDataProvider::delPair(int inId) {
    if (inId == INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*pairs, inId);
    pairs->remove(index);

    QSqlQuery query;
    query.prepare("delete from STPair where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit pairsChanged();
    // Don't use fillFunc() here!
    // BUG: Если использовать fillFunc(), то будет бага qt
    // при удаление пары. На самом деле при удаление пары
    // каскадов нет, поэтому перезаполнять векторы не обязательно.
    // fillFunc();
}

QVector<STSubject*>* STDataProvider::getSubjects() {
    return deepCopyQVector<STSubject>(*subjects);
}

STSubject* STDataProvider::getSubject(int inId) {
    int index = getIndexById(*subjects, inId);
    return new STSubject(*subjects->at(index));
}

int STDataProvider::addSubject(STSubject inSubject) {
    QSqlQuery query;
    query.prepare("insert into STSubject values (NULL,?,?);");
    query.addBindValue(inSubject.name);
    query.addBindValue(inSubject.nick);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inSubject.id = query.lastInsertId().toInt();
    subjects->append(new STSubject(inSubject));

    emit subjectsChanged();

    return inSubject.id;
}

void STDataProvider::modSubject(STSubject inSubject) {
    int index = getIndexById(*subjects, inSubject.id);
    delete subjects->at(index);
    subjects->replace(index, new STSubject(inSubject));

    QSqlQuery query;
    query.prepare("update STSubject set name=?, nick=? where id=?;");
    query.addBindValue(inSubject.name);
    query.addBindValue(inSubject.nick);
    query.addBindValue(inSubject.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit subjectsChanged();
}

void STDataProvider::delSubject(STSubject inSubject) {
    delSubject(inSubject.id);
}

void STDataProvider::delSubject(int inId) {
    if (inId == INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*subjects, inId);
    delete subjects->at(index);
    subjects->remove(index);

    QSqlQuery query;
    query.prepare("delete from STSubject where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

QVector<STTask*>* STDataProvider::getTaskOnDate(QDate inDate) {
    QVector<STTask*> ret;
    STTask* task;
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->date == inDate) {
            ret.append(task);
        }
    }

    return deepCopyQVector<STTask>(ret);
}

QVector<QVector<STTask*>*>* STDataProvider::getTaskOnWeekNumber(
        int inWeekNumber) {
    QVector<QVector<STTask*>*> ret;
    for (int i = Qt::Monday; i <= Qt::Sunday; i++) {
        ret.append(getTaskOnDate(getDateFromWeek(inWeekNumber, i)));
    }

    return deepCopyQVector<QVector<STTask*> >(ret);
}

QVector<STTask*>* STDataProvider::getAllTasks() {
    return deepCopyQVector<STTask>(*tasks);
}

QVector<STTask*>* STDataProvider::getOutdatedTasks() {
    QVector<STTask*> ret;
    STTask* task;
    QDate currentDate = QDate::currentDate();
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->date < currentDate && !task->isComplete) {
            ret.append(task);
        }
    }

    return deepCopyQVector<STTask>(ret);
}

STTask* STDataProvider::getTask(int inId) {
    int index = getIndexById(*tasks, inId);
    return new STTask(*tasks->at(index));
}

int STDataProvider::addTask(STTask inTask) {
    QSqlQuery query;
    query.prepare("PRAGMA foreign_keys = ON;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare("insert into STTask values (NULL,?,?,?,?,?,?,?,?,?);");
    query.addBindValue(getVariantFromId(inTask.subjectId));
    query.addBindValue(inTask.seriesNumber);
    query.addBindValue(inTask.isComplete);
    query.addBindValue(inTask.date.day());
    query.addBindValue(inTask.date.month());
    query.addBindValue(inTask.date.year());
    query.addBindValue(inTask.task);
    query.addBindValue(inTask.description);
    query.addBindValue(inTask.prior);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inTask.id = query.lastInsertId().toInt();
    tasks->append(new STTask(inTask));

    emit tasksChanged();

    return inTask.id;
}

void STDataProvider::modTask(STTask inTask) {
    int index = getIndexById(*tasks, inTask.id);
    delete tasks->at(index);
    tasks->replace(index, new STTask(inTask));

    QSqlQuery query;
    query.prepare(
                "update STTask set subjectId=?, seriesNumber=?, isComplete=?, dateD=?, "
                "dateM=?, dateY=?, task=?, description=?, prior=? where id=?;");
    query.addBindValue(getVariantFromId(inTask.subjectId));
    query.addBindValue(inTask.seriesNumber);
    query.addBindValue(inTask.isComplete);
    query.addBindValue(inTask.date.day());
    query.addBindValue(inTask.date.month());
    query.addBindValue(inTask.date.year());
    query.addBindValue(inTask.task);
    query.addBindValue(inTask.description);
    query.addBindValue(inTask.prior);
    query.addBindValue(inTask.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit tasksChanged();
}

void STDataProvider::delTask(STTask inTask) { delTask(inTask.id); }

void STDataProvider::delTask(int inId) {
    if (inId == INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*tasks, inId);
    delete tasks->at(index);
    tasks->remove(index);

    QSqlQuery query;
    query.prepare("delete from STTask where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

int STDataProvider::getNewSeriesNumber() {
    QSqlQuery query;
    query.prepare("select max(seriesNumber) from STTask;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.next();

    return query.value(0).toInt() + 1;
}

void STDataProvider::delTaskSeries(int inSeries) {
    STTask* task;
    for (int i = 0; i < tasks->size(); i++) {
        task = tasks->at(i);
        if (task->seriesNumber == inSeries) {
            delete task;
            tasks->remove(i);
            i = 0;
            // WARNING: bad method i=0
        }
    }

    QSqlQuery query;
    query.prepare("delete from STTask where seriesNumber=?;");
    query.addBindValue(inSeries);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

QVector<STTeacher*>* STDataProvider::getTeachers() {
    return deepCopyQVector<STTeacher>(*teachers);
}

QVector<STTeacher*>* STDataProvider::getTeachersOnPair(
        STSubject inSubject, STPair::Type inPairType) {
    QVector<STTeacher*> ret;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->subjectId == inSubject.id && pair->typeId == inPairType.id) {
            int index = getIndexById(*teachers, pair->teacherId);
            ret.append(teachers->at(index));
        }
    }
    return deepCopyQVector<STTeacher>(ret);
}

QVector<STTeacher*>* STDataProvider::getTeachersOnSubject(STSubject inSubject) {
    QVector<STTeacher*> ret;
    STPair* pair;
    for (int i = 0; i < pairs->size(); i++) {
        pair = pairs->at(i);
        if (pair->subjectId == inSubject.id) {
            int index = getIndexById(*teachers, pair->teacherId);
            ret.append(teachers->at(index));
        }
    }
    return deepCopyQVector<STTeacher>(ret);
}

STTeacher* STDataProvider::getTeacher(int inId) {
    int index = getIndexById(*teachers, inId);
    return new STTeacher(*teachers->at(index));
}

int STDataProvider::addTeacher(STTeacher inTeacher) {
    QSqlQuery query;
    query.prepare("insert into STTeacher values (NULL,?,?,?);");
    query.addBindValue(inTeacher.name);
    query.addBindValue(inTeacher.nick);
    query.addBindValue(inTeacher.additions);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    inTeacher.id = query.lastInsertId().toInt();
    teachers->append(new STTeacher(inTeacher));

    emit teachersChanged();

    return inTeacher.id;
}

void STDataProvider::modTeacher(STTeacher inTeacher) {
    int index = getIndexById(*teachers, inTeacher.id);
    delete teachers->at(index);
    teachers->replace(index, new STTeacher(inTeacher));

    QSqlQuery query;
    query.prepare("update STTeacher set name=?, nick=?, additions=? where id=?;");
    query.addBindValue(inTeacher.name);
    query.addBindValue(inTeacher.nick);
    query.addBindValue(inTeacher.additions);
    query.addBindValue(inTeacher.id);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    emit teachersChanged();
}

void STDataProvider::delTeacher(STTeacher inTeacher) {
    delTeacher(inTeacher.id);
}

void STDataProvider::delTeacher(int inId) {
    if (inId == INVALID_ID) {
        qDebug() << "Delete invalid id";
        return;
    }

    int index = getIndexById(*teachers, inId);
    delete teachers->at(index);
    teachers->remove(index);

    QSqlQuery query;
    query.prepare("delete from STTeacher where id=?;");
    query.addBindValue(inId);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

void STDataProvider::importDatabase(QString inFileName) {
    if (inFileName.isEmpty() || inFileName.isNull()) {
        return;
    }

    QString dbName = getDatabaseName();

    QSqlDatabase db = QSqlDatabase::database();
    QSqlQuery query;
    query.clear();
    db.close();

    if (!QFile::remove(dbName)) {
        QMessageBox::warning(
                    NULL, tr("Error"),
                    tr("Can't remove file with the old data"));
        return;
    }

    if (!QFile::copy(inFileName, dbName)) {
        // WARNING: remove was ok, database is clear :(
        QMessageBox::critical(
                    NULL, tr("Error"),
                    tr("Can't copy file with the new data"));
        return;
    }

    db.setDatabaseName(dbName);
    if (!db.open()) {
        qFatal("%s", db.lastError().text().toStdString().c_str());
    }

    initFunc();

    emit globalChanged();
    emit bellsChanged();
    emit pairTypesChanged();
    emit pairsChanged();
    emit subjectsChanged();
    emit tasksChanged();
    emit teachersChanged();
    emit updated();
}

void STDataProvider::initFunc() {
    //  Enable foreign
    QSqlQuery query;
    query.prepare("PRAGMA foreign_keys = ON;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    //  Create all tables, if they not exist
    query.clear();
    query.prepare(DP_CREATE_GLOBAL);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_BELLS);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_PAIRTYPE);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_TEACHER);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_SUBJECT);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_TASK);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    query.clear();
    query.prepare(DP_CREATE_PAIR);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    //  Set version
    query.clear();
    query.prepare("insert or replace into STGlobal values (?,?);");
    query.addBindValue(version);
    query.addBindValue(DP_VERSION);
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }

    fillFunc();
}

void STDataProvider::fillFunc() {
    //  Fill vectors
    QSqlQuery query;

    query.clear();
    query.prepare("select * from STBells;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    bells->clear();
    while (query.next()) {
        bells->append(QTime(query.value(1).toInt(), query.value(2).toInt()));
    }

    query.clear();
    query.prepare("select * from STPairType;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STPair::Type>(*pairTypes);
    STPair::Type* pairType;
    while (query.next()) {
        pairType = new STPair::Type;
        pairType->id = query.value(0).toInt();
        pairType->fullName = query.value(1).toString();
        pairType->shortName = query.value(2).toString();
        pairTypes->append(pairType);
    }

    query.clear();
    query.prepare("select * from STTeacher;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STTeacher>(*teachers);
    STTeacher* teacher;
    while (query.next()) {
        teacher = new STTeacher;
        teacher->id = query.value(0).toInt();
        teacher->name = query.value(1).toString();
        teacher->nick = query.value(2).toString();
        teacher->additions = query.value(3).toString();
        teachers->append(teacher);
    }

    query.clear();
    query.prepare("select * from STSubject;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STSubject>(*subjects);
    STSubject* subject;
    while (query.next()) {
        subject = new STSubject;
        subject->id = query.value(0).toInt();
        subject->name = query.value(1).toString();
        subject->nick = query.value(2).toString();
        subjects->append(subject);
    }

    query.clear();
    query.prepare("select * from STTask;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STTask>(*tasks);
    STTask* task;
    while (query.next()) {
        task = new STTask;
        task->id = query.value(0).toInt();
        task->subjectId = getIdFromVariant(query.value(1));
        task->seriesNumber = query.value(2).toInt();
        task->isComplete = query.value(3).toBool();
        task->date = QDate(query.value(6).toInt(), query.value(5).toInt(),
                           query.value(4).toInt());
        task->task = query.value(7).toString();
        task->description = query.value(8).toString();
        task->prior = query.value(9).toInt();
        tasks->append(task);
    }

    query.clear();
    query.prepare("select * from STPair;");
    if (!query.exec()) {
        DP_QUERY_ERROR;
    }
    clearQVector<STPair>(*pairs);
    STPair* pair;
    while (query.next()) {
        pair = new STPair;
        pair->id = query.value(0).toInt();
        pair->subjectId = getIdFromVariant(query.value(1));
        pair->typeId = getIdFromVariant(query.value(2));
        pair->teacherId = getIdFromVariant(query.value(3));
        pair->week = query.value(4).toInt();
        pair->dayOfWeek = query.value(5).toInt();
        pair->gridNumber = query.value(6).toInt();
        pair->auditory = query.value(7).toString();
        pair->additions = query.value(8).toString();
        pairs->append(pair);
    }

    emit globalChanged();
    emit bellsChanged();
    emit pairTypesChanged();
    emit pairsChanged();
    emit subjectsChanged();
    emit tasksChanged();
    emit teachersChanged();
}

int STDataProvider::getIdFromVariant(QVariant inVar) {
    if (inVar.isNull() ||
            (inVar.type() != QVariant::Int && inVar.type() != QVariant::LongLong)) {
        return INVALID_ID;
    }
    return inVar.toInt();
}

QVariant STDataProvider::getVariantFromId(int inId) {
    if (inId == INVALID_ID) {
        return QVariant(QVariant::Int);
    }
    return QVariant(inId);
}
