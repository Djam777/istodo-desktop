/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_DATAPROVIDER_H
#define ST_DATAPROVIDER_H

#include <QtCore>
#include <QtSql>
#include <QMessageBox>
#include "../const.h"
#include "st.pair.h"
#include "st.subject.h"
#include "st.task.h"
#include "st.teacher.h"

template <typename T>
void clearQVector(QVector<T*>& inVector) {
    for (int i = 0; i < inVector.size(); i++) {
        delete inVector.at(i);
    }
    inVector.clear();
}

template <typename T>
QVector<T*>* deepCopyQVector(QVector<T*>& inVector) {
    QVector<T*>* ret = new QVector<T*>();
    ret->reserve(inVector.size());
    for (int i = 0; i < inVector.size(); i++) {
        ret->append(new T(*inVector.at(i)));
    }
    return ret;
}

class STDataProvider : public QObject {
        Q_OBJECT

    signals:
        void globalChanged();
        void bellsChanged();
        void pairTypesChanged();
        void pairsChanged();
        void subjectsChanged();
        void tasksChanged();
        void teachersChanged();
        void updated();

    public:
        STDataProvider(QString inName = QString());
        ~STDataProvider();

        static STDataProvider& getInstance();

        //  Check
        bool isEmpty();
        QString getDatabaseName();
        int getVersion();

        QString *getDaysOfWeek();

        int getPairLength();
        void setPairLength(int inPairLength);

        QDate getBegin();
        void setBegin(QDate inDate);
        QDate getEnd();
        void setEnd(QDate inDate);

        int getCountOfWeek();
        QDate getDateFromWeek(int inWeek, int inDayOfWeek);
        int getWeekFromDate(QDate inDate);

        QVector<QTime>* getBells();
        QTime getBell(int inNum);
        int addBell(QTime inTime);
        void modBell(int inBell, QTime inTime);
        void delBell();

        QVector<STPair::Type*>* getPairTypes();
        QVector<STPair::Type*>* getPairTypesOnSubject(STSubject inSubject);
        STPair::Type* getPairType(int inId);
        int addPairType(STPair::Type inPairType);
        void modPairType(STPair::Type inPairType);
        void delPairType(STPair::Type inPairType);
        void delPairType(int inId);

        QVector<STPair*>* getSheduleOnDay(int inWeek, int inDayOfWeek);
        QVector<QVector<STPair*>*>* getSheduleOnWeek(int inWeek);
        QVector<QString*>* getAuditoryOnPair(STSubject inSubject,
                                             STPair::Type inPairType);
        QVector<QString*>* getAuditoryOnSubject(STSubject inSubject);
        STPair* getPair(int inId);
        int addPair(STPair inPair);
        void modPair(STPair inPair);
        void delScheduleOnWeek(int inWeek);
        void cloneWeek(int inWeek);
        void delPair(STPair inPair);
        void delPair(int inId);

        QVector<STSubject*>* getSubjects();
        STSubject* getSubject(int inId);
        int addSubject(STSubject inSubject);
        void modSubject(STSubject inSubject);
        void delSubject(STSubject inSubject);
        void delSubject(int inId);

        QVector<STTask*>* getTaskOnDate(QDate inDate);
        QVector<QVector<STTask*>*>* getTaskOnWeekNumber(int inWeekNumber);
        QVector<STTask*>* getAllTasks();
        QVector<STTask*>* getOutdatedTasks();
        STTask* getTask(int inId);
        int addTask(STTask inTask);
        void modTask(STTask inTask);
        void delTask(STTask inTask);
        void delTask(int inId);

        int getNewSeriesNumber();
        void delTaskSeries(int inSeries);

        QVector<STTeacher*>* getTeachers();
        QVector<STTeacher*>* getTeachersOnPair(STSubject inSubject,
                                               STPair::Type inPairType);
        QVector<STTeacher*>* getTeachersOnSubject(STSubject inSubject);
        STTeacher* getTeacher(int inId);
        int addTeacher(STTeacher inTeacher);
        void modTeacher(STTeacher inTeacher);
        void delTeacher(STTeacher inTeacher);
        void delTeacher(int inId);

        void importDatabase(QString inFileName);

    private:
        enum STGlobal {
            version,  // Database version
            pairLength,
            dBegin,
            mBegin,
            yBegin,
            dEnd,
            mEnd,
            yEnd
        };

        QVector<QTime>* bells;
        QVector<STPair::Type*>* pairTypes;
        QVector<STPair*>* pairs;
        QVector<STSubject*>* subjects;
        QVector<STTask*>* tasks;
        QVector<STTeacher*>* teachers;
        QString daysOfWeek[8];

        void initFunc();
        void fillFunc();

        int getIdFromVariant(QVariant inVar);
        QVariant getVariantFromId(int inId);

        template <class T>
        int getIndexById(QVector<T*>& inVector, int inId) {
            for (int i = 0; i < inVector.size(); i++) {
                if (inVector.at(i)->id == inId) {
                    return i;
                }
            }
            qFatal("Can't find index by id");
            return -1;
        }
};

#endif  // ST_DATAPROVIDER_H
