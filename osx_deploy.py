# -*- coding: utf-8 -*-

import os
import glob
import shutil
from subprocess import call

# Setup app info
version  = "1.2"
appName  = "iStodo"
devName  = "Yakov Eremin"
pathToQt = "/Users/eremin/Qt5.2.0/5.2.0/clang_64/" 
entitlements = "iStodoEntitlements.plist"

fullApp  = appName +".app"
dirName  = appName + "_" + version

# if we need only libqsqlite.dylib
sqliteOnly = True

sqldriversDir = fullApp+"/Contents/PlugIns/sqldrivers/"
frameworksDir = fullApp+"/Contents/Frameworks/"
pluginsDir    = fullApp+"/Contents/PlugIns/"

print("Prepearing to deploy...")
# Check files and paths 
if not os.path.exists(pathToQt) or not os.path.isdir(pathToQt):
	print("Incorrect path to Qt")
	exit()
if not os.path.exists(fullApp) or not os.path.isdir(fullApp):
	print("App bundle not found")
	exit()
if not os.path.exists(entitlements) or os.path.isdir(entitlements):
	print("Entitlements file not found")
	exit()

#remove old build
if os.path.exists(dirName):
	shutil.rmtree(dirName)

os.makedirs(dirName)

# Copy all necessary files to new folder
shutil.copy(entitlements, dirName)
shutil.copytree(fullApp, dirName+"/"+fullApp)

# Copy Qt libs for create independent app
os.chdir(os.getcwd()+"/"+dirName)
print("\nDeploying Qt to .app bundle...")
call([pathToQt+"bin/macdeployqt", fullApp])
print("...done\n")

# Other libs in Qt 5.2(at least) will be rejected from Mac App Store anyway
if sqliteOnly and os.path.exists(sqldriversDir):
	sqllibs = glob.glob(sqldriversDir+"*.dylib")
	for lib in sqllibs:
		if os.path.basename(lib) != "libqsqlite.dylib":
			os.remove(lib)

# Copy plists for frameworks (it's fix macdeployqt bug)
frameworks = os.listdir(frameworksDir)
for framework in frameworks:
	shutil.copy(pathToQt+"lib/"+framework+"/Contents/Info.plist", frameworksDir+framework+"/Resources/")

print("\nSigning frameworks, dylibs, and binary...")
# Sign frameworks (it's strange? but we can't sign "Versions" folder)
os.system('codesign -s "3rd Party Mac Developer Application: '+devName+'" '+frameworksDir+"*")

# Sign plugins
pluginGroups = os.listdir(pluginsDir)
for group in pluginGroups:
	os.system('codesign -s "3rd Party Mac Developer Application: '+devName+'"  '+pluginsDir+group+"/*")

# Sign app
os.system('codesign --entitlements '+entitlements+' -s "3rd Party Mac Developer Application: '+devName+'" '+fullApp)

print("\nCheck signing:")
os.system("codesign --display --verbose=4 "+fullApp)

# 	- - -
print("\nBuilding package...")
os.system('productbuild --component "'+fullApp+'" /Applications --sign "3rd Party Mac Developer Installer: '+devName+'" --product "'+fullApp+'/Contents/Info.plist" '+appName+'.pkg')
print("...done\n")

print('\nFor test install, run follow command: sudo installer -store -pkg '+dirName+'/'+appName+'.pkg -target /')