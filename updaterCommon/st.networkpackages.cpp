#include "st.networkpackages.h"

void STUpdateRequest::write(QDataStream &inStream) {
    inStream << buildNumber;
}

void STUpdateRequest::read(QDataStream &inStream) {
    inStream >> buildNumber;
}

void STUpdateResponse::write(QDataStream &inStream) {
    inStream << isNeedUpdate;
}

void STUpdateResponse::read(QDataStream &inStream) {
    inStream >> isNeedUpdate;
}

void STNotResponse::write(QDataStream &inStream) {
    inStream << false;
}

void STNotResponse::read(QDataStream &inStream) {
    bool tempBool;
    inStream >> tempBool;
}
