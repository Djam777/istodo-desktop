/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_NETWORKPACKAGES_H
#define ST_NETWORKPACKAGES_H

#include <QtCore>
#include <QtNetwork>

#define STODO_UPDATER_NAME "istodo.ru"
#define STODO_UPDATER_PORT 28931

class STNetworkPackage {
    public:
        const static int packageId = 0;

        virtual void write(QDataStream &inStream) = 0;
        virtual void read(QDataStream &inStream) = 0;
};

class STUpdateRequest : public STNetworkPackage {
    public:
        const static int packageId = 1;

        int buildNumber;

        virtual void write(QDataStream &inStream);
        virtual void read(QDataStream &inStream);
};

class STUpdateResponse : public STNetworkPackage {
    public:
        const static int packageId = 2;

        bool isNeedUpdate;

        virtual void write(QDataStream &inStream);
        virtual void read(QDataStream &inStream);
};

class STNotResponse : public STNetworkPackage {
    public:
        const static int packageId = 3;

        virtual void write(QDataStream &inStream);
        virtual void read(QDataStream &inStream);
};

#endif  // ST_NETWORKPACKAGES_H
