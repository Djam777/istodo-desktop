PRAGMA foreign_keys = ON;
CREATE TABLE IF NOT EXISTS STGlobal 
(
    num INTEGER Primary Key,
    data INTEGER
);
CREATE TABLE IF NOT EXISTS STBells 
(
    num INTEGER Primary Key,
    h INTEGER,
    m INTEGER
);
CREATE TABLE IF NOT EXISTS STPairType
(
    id INTEGER Primary Key,
    fullName TEXT,
    shortName TEXT
);
CREATE TABLE IF NOT EXISTS STTeacher
(
    id INTEGER Primary Key,
    name TEXT,
    nick TEXT,
    additions TEXT
);
CREATE TABLE IF NOT EXISTS STSubject
(
    id INTEGER Primary Key,
    name TEXT,
    nick TEXT
);
CREATE TABLE IF NOT EXISTS STTask 
(
    id INTEGER Primary Key,
    subjectId INTEGER,
    dateD INTEGER,
    dateM INTEGER,
    dateY INTEGER,
    task TEXT,
    prior INTEGER,
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade
);
CREATE TABLE IF NOT EXISTS STPair
(
    id INTEGER Primary Key,
    subjectId INTEGER NOT NULL,
    typeId INTEGER,
    teacherId INTEGER,
    week INTEGER,
    dayOfWeek INTEGER,
    gridNumber INTEGER NOT NULL,
    audithory TEXT,
    foreign Key (subjectId) references STSubject (id) on delete cascade on update cascade,
    foreign Key (typeId) references STPairType (id) on delete restrict on update cascade,
    foreign Key (teacherId) references STTeacher (id) on delete restrict on update cascade,
    foreign Key (gridNumber) references STBells (num) on delete restrict on update cascade
 );