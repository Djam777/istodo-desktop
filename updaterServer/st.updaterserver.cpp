#include "st.updaterserver.h"

STUpdaterServer::STUpdaterServer(QObject *parent) : QObject(parent) {
    connect(&server, SIGNAL(newConnection()), SLOT(acceptConnection()));

    if (!server.isListening() &&
            !server.listen(QHostAddress::Any, STODO_UPDATER_PORT)) {
        qDebug() << "Unable to start server: " << server.errorString();
        exit(1);
    }

    qDebug() << "Server started" << QTime::currentTime();

    currentDate = QDate::currentDate();
    connect(&timer, SIGNAL(timeout()), this, SLOT(endOfTime()));
    timer.start(1000 * 60 * (60 - QTime::currentTime().minute()));

    currentLog.setFileName(getFileName(currentDate));
    if (currentLog.exists()) {
        if (currentLog.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QString allStr, winStr, linStr, osxStr;
            while (!currentLog.atEnd()) {
                allStr = currentLog.readLine();
                winStr = currentLog.readLine();
                linStr = currentLog.readLine();
                osxStr = currentLog.readLine();
            }
            QStringList lst = allStr.split(" ");
            usersCount = lst.at(0).toInt();
            currentLog.close();

            windowsUsersCount = winStr.toInt();
            linuxUsersCount = linStr.toInt();
            osxUsersCount = osxStr.toInt();
        }
    } else {
        usersCount = 0;
        windowsUsersCount = 0;
        linuxUsersCount = 0;
        osxUsersCount = 0;
    }
}

QString STUpdaterServer::getFileName(QDate inDate) {
    if (!QDir().exists(STODO_LOG_PATH)) {
        QDir().mkdir(STODO_LOG_PATH);
    }
    return STODO_LOG_PATH + currentDate.toString("/dd.MM.yy.txt");
}

void STUpdaterServer::acceptConnection() {
    QTcpSocket *tcpServerConnection = server.nextPendingConnection();
    connect(tcpServerConnection, SIGNAL(readyRead()), SLOT(firstRead()));

    // Check second-time client
    QHostAddress buffAddr = tcpServerConnection->peerAddress();
    if (addrArr.indexOf(buffAddr) != -1) {
        isOldClient = true;
    } else {
        isOldClient = false;
        addrArr.push_back(buffAddr);
    }
}

void STUpdaterServer::firstRead() {
    QTcpSocket *client = (QTcpSocket *)QObject::sender();
    client->disconnect();

    QDataStream stream(client);

    int packageId;
    stream >> packageId;
    switch (packageId) {
        case STUpdateRequest::packageId: {
            // Update block
            STUpdateRequest request;
            request.read(stream);
            qDebug() << QTime::currentTime().toString()
                     << " new request build: " << request.buildNumber;
            if (request.buildNumber < STODO_CURRENT_BUILD) {
                STUpdateResponse response;
                response.isNeedUpdate = true;
                stream << STUpdateResponse::packageId;
                response.write(stream);
            } else {
                STNotResponse response;
                stream << STNotResponse::packageId;
                response.write(stream);
            }

            // Statistics block
            if (request.buildNumber == 9000) {
                // dev version
                qDebug() << "it's dev, skip...";
                break;
            }

            if (isOldClient) {
                // second-time clent
                qDebug() << "it's old, skip...";
                break;
            }

            usersCount++;
            if (!request.buildNumber) {
                // beta version
                break;
            }

            switch (request.buildNumber % 10) {
                case STODO_PLATFORM_WINDOWS: {
                    windowsUsersCount++;
                    break;
                }
                case STODO_PLATFORM_LINUX: {
                    linuxUsersCount++;
                    break;
                }
                case STODO_PLATFORM_OSX: {
                    osxUsersCount++;
                    break;
                }
            }

            break;
        }
        default: {
            qDebug() << Q_FUNC_INFO << " Unknow package";
            break;
        }
    }
}

void STUpdaterServer::endOfTime() {
    currentLog.open(QIODevice::Append | QIODevice::Text);
    QTextStream out(&currentLog);
    out << usersCount << " " << QTime::currentTime().toString("hh:mm")
        << " Win & Lin & Mac\n";
    out << windowsUsersCount << "\n";
    out << linuxUsersCount << "\n";
    out << osxUsersCount << "\n";
    currentLog.close();

    QDate newDate = QDate::currentDate();
    if (currentDate != newDate) {
        currentDate = newDate;
        currentLog.setFileName(getFileName(currentDate));
        qDebug() << "=====next day=====";
        // reset vars
        usersCount = 0;
        windowsUsersCount = 0;
        linuxUsersCount = 0;
        osxUsersCount = 0;
        addrArr.clear();
    }

    timer.start(STATISTICS_UPDATE_TIME);
}
