#-------------------------------------------------
#
# Project created by QtCreator 2013-08-01T15:08:33
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = updaterServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

include(../updaterCommon/updaterCommon.pri)

SOURCES += \
    main.cpp \
    st.updaterserver.cpp

HEADERS += \
    st.updaterserver.h
