/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_UPDATERSERVER_H
#define ST_UPDATERSERVER_H

#include <QtCore>
#include <QtNetwork>
#include "../updaterCommon/st.networkpackages.h"

#define STODO_CURRENT_BUILD 0

#define STODO_PLATFORM_WINDOWS 1
#define STODO_PLATFORM_LINUX 2
#define STODO_PLATFORM_OSX 3

#define STATISTICS_UPDATE_TIME 1000 * 3600
#define STODO_LOG_PATH "iStodo_Log"

class STUpdaterServer : public QObject {
        Q_OBJECT

    public:
        explicit STUpdaterServer(QObject *parent = 0);

    private:
        QString getFileName(QDate inDate);

        QTcpServer server;

        QTimer timer;
        int osxUsersCount;
        int linuxUsersCount;
        int windowsUsersCount;
        int usersCount;
        bool isOldClient;

        QDate currentDate;
        QFile currentLog;

        QVector<QHostAddress> addrArr;

    private slots:
        void acceptConnection();
        void firstRead();
        void endOfTime();
};

#endif  // ST_UPDATERSERVER_H
