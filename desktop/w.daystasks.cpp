#include "w.daystasks.h"
#include "ui_w.daystasks.h"

WDaysTasks::WDaysTasks(QWidget *parent)
    : QWidget(parent), ui(new Ui::WDaysTasks) {
    ui->setupUi(this);
    currentState = STATE_CALENDAR;
}

WDaysTasks::~WDaysTasks() { delete ui; }

bool WDaysTasks::fillTasks(QDate inDate, STFilterElement inFilter) {
    currentDate = inDate;
    currentFilter = inFilter;
    currentState = STATE_CALENDAR;

    return refreshAll();
}

bool WDaysTasks::fillOverDue() {
    currentState = STATE_OVERDUE;
    return refreshAll();
}

bool WDaysTasks::fillAllTasks(STFilterElement inFilter) {
    currentFilter = inFilter;
    currentState = STATE_ALL;
    return refreshAll();
}

void WDaysTasks::taskSelected(int inId) { emit sigNeedTaskEdit(inId); }

bool WDaysTasks::refreshAll() {
    bool ret = false;

    // Clear old tasks
    while (QLayoutItem *item = ui->frmDaysTask->layout()->takeAt(0)) {
        if (QWidget *widget = item->widget()) {
            delete widget;
        }

        delete item;
    }

    int filtredSubjsCount = currentFilter.unselectedSubjects.size();
    bool taskFiltred = false;

    switch (currentState) {
        case STATE_CALENDAR: {
            ui->lbTaskBlock->setText(currentDate.toString("dddd, d MMMM"));
            QVector<STTask *> *tasks =
                    STDataProvider::getInstance().getTaskOnDate(currentDate);

            for (int i = 0; i < tasks->size(); i++) {
                // Filter completed tasks if needed
                if ((*tasks)[i]->isComplete && !currentFilter.showCompleted) {
                    continue;
                }

                for (int j = 0; j < filtredSubjsCount; j++) {
                    if ((*tasks)[i]->subjectId == currentFilter.unselectedSubjects[j]) {
                        taskFiltred = true;
                    }
                }

                if (taskFiltred) {
                    delete (*tasks)[i];
                    taskFiltred = false;
                    continue;
                }

                WTaskLine *buffLine = new WTaskLine;
                buffLine->fillTask((*tasks)[i]);

                connect(buffLine, SIGNAL(clicked(int)), SLOT(taskSelected(int)));

                ui->frmDaysTask->layout()->addWidget(buffLine);
                ret = true;
            }
            if (tasks != NULL) {
                // This tasks will be removed in task line destructor!
                // clearVectorWithContent<STTask*>(*tasks);
                delete tasks;
                tasks = NULL;
            }
            break;
        }
        case STATE_OVERDUE: {
            ui->lbTaskBlock->setText(tr("Overdue"));

            QVector<STTask *> *tasks =
                    STDataProvider::getInstance().getOutdatedTasks();

            for (int i = 0; i < tasks->size(); i++) {

                WTaskLine *buffLine = new WTaskLine;
                buffLine->fillTask((*tasks)[i]);

                connect(buffLine, SIGNAL(clicked(int)), SLOT(taskSelected(int)));

                ui->frmDaysTask->layout()->addWidget(buffLine);
                ret = true;
            }
            if (tasks != NULL) {
                // This tasks will be removed in task line destructor!
                // clearVectorWithContent<STTask*>(*tasks);
                delete tasks;
                tasks = NULL;
            }
            break;
        }
        case STATE_ALL: {
            ui->lbTaskBlock->setText(tr("All tasks"));

            QVector<STTask *> *tasks = STDataProvider::getInstance().getAllTasks();

            for (int i = 0; i < tasks->size(); i++) {
                // Filter completed tasks if needed
                if ((*tasks)[i]->isComplete && !currentFilter.showCompleted) {
                    continue;
                }

                for (int j = 0; j < filtredSubjsCount; j++) {
                    if ((*tasks)[i]->subjectId == currentFilter.unselectedSubjects[j]) {
                        taskFiltred = true;
                    }
                }

                if (taskFiltred) {
                    delete (*tasks)[i];
                    taskFiltred = false;
                    continue;
                }

                WTaskLine *buffLine = new WTaskLine;
                buffLine->fillTask((*tasks)[i]);

                connect(buffLine, SIGNAL(clicked(int)), SLOT(taskSelected(int)));

                ui->frmDaysTask->layout()->addWidget(buffLine);
                ret = true;
            }
            if (tasks != NULL) {
                // This tasks will be removed in task line destructor!
                // clearVectorWithContent<STTask*>(*tasks);
                delete tasks;
                tasks = NULL;
            }
            break;
        }
    }

    return ret;
}
