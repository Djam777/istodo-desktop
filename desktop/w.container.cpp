#include "w.container.h"
#include "ui_w.container.h"

WContainer::WContainer(QWidget *parent)
    : QWidget(parent), ui(new Ui::WContainer) {
    ui->setupUi(this);

    lblHeader = ui->lblHeader;
    pbSettings = ui->pbSettings;
    lwList = ui->lwList;

#ifdef Q_OS_MAC
    lwList->setAttribute(Qt::WA_MacShowFocusRect, 0);
#endif
}

WContainer::~WContainer() { delete ui; }

void WContainer::setType(int inType) {
    switch (inType) {
        case CONTAINER_TEACHERS: {
            ui->lblHeader->setText(tr("Teachers"));
            ui->lbStubText->setText(tr("add teachers"));
            ui->lbStubIcon->setObjectName("lbTeacherIcon");
            ui->lbStubIcon->setFixedSize(77, 128);
            break;
        }
        case CONTAINER_PAIR_TYPES: {
            ui->lblHeader->setText(tr("Types"));
            ui->lbStubText->setText(tr("add types"));
            ui->lbStubIcon->setObjectName("lbAboutIcon");
            ui->lbStubIcon->setFixedSize(64, 128);
            break;
        }
        case CONTAINER_SUBJECTS: {
            ui->lblHeader->setText(tr("Subjects"));
            ui->lbStubText->setText(tr("add subjects"));
            ui->lbStubIcon->setObjectName("lbBooksIcon");
            ui->lbStubIcon->setFixedSize(100, 128);
            break;
        }
    }
    ui->lbStubIcon->setStyle(NULL);
}

void WContainer::setState(bool isEdit) {
    if (isEdit) {
        pbSettings->setObjectName("pbExpandSingleBlue");
        ui->stackedWidget->setCurrentIndex(PAGE_LIST);
    } else {
        pbSettings->setObjectName("pbAddSingleBlue");
        ui->stackedWidget->setCurrentIndex(PAGE_STUB);
    }
    pbSettings->setStyle(NULL);
}
