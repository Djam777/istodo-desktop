/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_SCHEDULEONWEEK_H
#define W_SCHEDULEONWEEK_H

#define W_SLIDE_ANIMATION_PATH 2000
#define W_BLINK_ANIMATION_TIME 2000
#define W_BLINK_ANIMATION_COUNT 9  // Only not even: 5, 7, 9...

#include "include.h"

namespace Ui {
    class WScheduleOnWeek;
}

class WScheduleOnWeek : public QFrame {
        Q_OBJECT

    signals:
        void sigIncWeek();
        void sigDecWeek();
        void sigHome();
        void sigScheduleSetting();

    public:
        explicit WScheduleOnWeek(QWidget *parent = 0);
        ~WScheduleOnWeek();
        void resizeEvent(QResizeEvent *inRE);
        void enableSmallScreenSupport();

    public slots:
        void fillScheduleOnDate(QDate inDate);
        void slideToHide();
        void slideToShow();

    private:
        int blinkCount;
        Ui::WScheduleOnWeek *ui;
        QTimer endResizeTimer;

    private slots:
        void slotIncWeek();
        void slotDecWeek();
        void slotHome();
        void slotScheduleSetting();
        void blink();
        void computeNewWidth();
};

#endif  // W_SCHEDULEONWEEK_H
