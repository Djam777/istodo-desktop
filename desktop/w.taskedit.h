/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_TASKEDIT_H
#define W_TASKEDIT_H

#define W_SLIDE_ANIMATION_PATH 2000

#include "include.h"

enum {
    REPEAT_DAY = 1,
    REPEAT_WEEK = 7,
    REPEAT_MONTH = 30
};

namespace Ui {
    class WTaskEdit;
}

class WTaskEdit : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedSchedule();
        void sigNeedRefresh();

    public:
        explicit WTaskEdit(QWidget *parent = 0);
        ~WTaskEdit();
        void fillFields(int inId);
        void setDate(QDate inDate);

    public slots:
        void slideToHide();
        void slideToShow();

    private:
        Ui::WTaskEdit *ui;
        int currentId;
        int currentSeries;
        // void showEvent(QShowEvent *inSE);
        void resizeEvent(QResizeEvent *inRE);
        void clearFields();

    private slots:
        void toggleRepeat(bool inRepeat);
        void repeatPeriodChanged(int inNum);
        void saveAll();
        void saveTodo(QString inTitle, QDate inDate, int inSeries = INVALID_ID);
        void addTodo();
        void removeTodo();
        void removeSeries();
        void closeEditor();
        void finishEdit();
        void checkDates(QDate inDate);
        void somethingChanged();
};

#endif  // W_TASKEDIT_H
