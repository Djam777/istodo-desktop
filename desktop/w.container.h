/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_CONTAINER_H
#define W_CONTAINER_H

#include "include.h"

enum {
    CONTAINER_TEACHERS,
    CONTAINER_SUBJECTS,
    CONTAINER_PAIR_TYPES
};

enum {
    PAGE_LIST,
    PAGE_STUB
};

namespace Ui {
    class WContainer;
}

class WContainer : public QWidget {
        Q_OBJECT

    public:
        explicit WContainer(QWidget *parent = 0);
        ~WContainer();

        void setType(int inType);
        void setState(bool isEdit);

        QLabel *lblHeader;
        QPushButton *pbSettings;
        QListWidget *lwList;

    private:
        Ui::WContainer *ui;
};

#endif  // W_CONTAINER_H
