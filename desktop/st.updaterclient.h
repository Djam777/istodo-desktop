/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef ST_UPDATERCLIENT_H
#define ST_UPDATERCLIENT_H

#include <QtCore>
#include <QtWidgets>
#include <QtNetwork>
#include "../updaterCommon/st.networkpackages.h"
#include "const.h"

#define STODO_BUILD_NUMBER ST_VERSION

class STUpdaterClient : public QObject {
        Q_OBJECT

    signals:
        void needUpdate();

    public:
        STUpdaterClient();

    public slots:
        void checkUpdate();

    private:
        QTcpSocket client;

    private slots:
        void startUpdate();
        void stopUpdate();
};

#endif  // ST_UPDATERCLIENT_H
