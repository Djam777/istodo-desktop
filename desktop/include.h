/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef INCLUDE_H
#define INCLUDE_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtNetwork>
#include "../const.h"
#include "dataprovider/st.dataprovider.h"
#include "dataprovider/st.icalexporter.h"
#include "st.filterelement.h"
Q_DECLARE_METATYPE(QString*)
Q_DECLARE_METATYPE(STTeacher*)
Q_DECLARE_METATYPE(STPair::Type*)
Q_DECLARE_METATYPE(STSubject*)

class STUpdaterClient;
class WTaskEdit;
class DEditTeacher;
class DEditPairType;
class DEditSubject;
class DSettingSemester;
class WDragPairCreate;
class WDragPairMove;
class WDropPairContainer;
class WDropPairTrash;
class WEditSchedule;
class WCalendar;
class WContainer;
class WSettingSchedule;
class WStodo;
class WPageShow;

#endif // INCLUDE_H
