#include "w.about.h"
#include "ui_w.about.h"

#include "../const.h"

WAbout::WAbout(QWidget *parent) : QWidget(parent), ui(new Ui::WAbout) {
    ui->setupUi(this);
    ui->lbVersion->setText(ui->lbVersion->text().arg(ST_STR_VERSION));

    //    connect( ui->cbCheckUpdate,
    //             SIGNAL(toggled(bool)),
    //             SLOT(onCheckBox(bool)) );
}

WAbout::~WAbout() { delete ui; }

void WAbout::onCheckBox(bool toggled) {}
