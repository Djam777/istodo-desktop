#include "d.settingsemester.h"
#include "ui_d.settingsemester.h"

DSettingSemester::DSettingSemester(QWidget *parent)
    : QDialog(parent), ui(new Ui::DSettingSemester) {
    ui->setupUi(this);
    updateUi();

    connect(ui->pbChancel, SIGNAL(clicked()), SLOT(slotChancel()));
    connect(ui->pbReady, SIGNAL(clicked()), SLOT(slotOk()));
    ui->pbChancel->setObjectName("pbDiaRemove");

    setModal(true);
}

DSettingSemester::~DSettingSemester() { delete ui; }

void DSettingSemester::updateData() {
    QDate beginDate = ui->deStartSem->date();
    STDataProvider::getInstance().setBegin(beginDate);

    QDate finishDate = ui->deFinishSem->date();
    if (ui->rbCWeeks->isChecked()) {
        finishDate = beginDate.addDays(ui->sbLengthSem->value() * 7);
    }
    // Validate it!
    if (finishDate <= beginDate) {
        finishDate = beginDate.addMonths(1);
    }
    STDataProvider::getInstance().setEnd(finishDate);

    STDataProvider::getInstance().setPairLength(ui->sbLengthPair->value());
}

void DSettingSemester::updateUi() {
    QDate beginDate = QDate::currentDate();
    QDate finishDate = QDate::currentDate();
    int semLength;
    int pairLength;
    if (STDataProvider::getInstance().isEmpty()) {
        semLength = DSS_SEM_LENGTH;
        pairLength = DSS_PAIR_LENGTH;
        if (beginDate.month() >= 7) {
            beginDate.setDate(beginDate.year(), 9, 1);
            finishDate.setDate(finishDate.year(), 12, 31);
        } else {
            beginDate.setDate(beginDate.year(), 2, 1);
            finishDate.setDate(finishDate.year(), 5, 31);
        }
    } else {
        beginDate = STDataProvider::getInstance().getBegin();
        finishDate = STDataProvider::getInstance().getEnd();
        semLength = STDataProvider::getInstance().getBegin().daysTo(
                    STDataProvider::getInstance().getEnd()) /
                7;
        pairLength = STDataProvider::getInstance().getPairLength();
    }

    ui->deStartSem->setDate(beginDate);
    ui->deFinishSem->setDate(finishDate);
    ui->sbLengthSem->setValue(semLength);
    ui->sbLengthPair->setValue(pairLength);
}

void DSettingSemester::slotChancel() {
    if (STDataProvider::getInstance().isEmpty()) {
        updateData();
    }
    close();
}

void DSettingSemester::slotOk() {
    updateData();

    close();
}
