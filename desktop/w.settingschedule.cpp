#include "w.settingschedule.h"
#include "ui_w.settingschedule.h"

WSettingSchedule::WSettingSchedule(QWidget *parent)
    : QWidget(parent), ui(new Ui::WSettingSchedule) {
    ui->setupUi(this);

    editSubject = new DEditSubject(this);
    editPairType = new DEditPairType(this);
    editTeacher = new DEditTeacher(this);

    //  Containers
    ui->cTeachers->setType(CONTAINER_TEACHERS);
    ui->cPairTypes->setType(CONTAINER_PAIR_TYPES);
    ui->cSubjects->setType(CONTAINER_SUBJECTS);

    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(fillSubjectList()));
    connect(&STDataProvider::getInstance(), SIGNAL(pairTypesChanged()),
            SLOT(fillPairTypeList()));
    connect(&STDataProvider::getInstance(), SIGNAL(teachersChanged()),
            SLOT(fillTeacherList()));

    ui->cbGroup->lineEdit()->setStyleSheet("border-radius:2px");
    ui->cbAuditory->lineEdit()->setStyleSheet("border-radius:2px");

    connect(ui->pbClearAudit, SIGNAL(clicked()), ui->cbAuditory, SLOT(clearEditText()) );
    connect(ui->pbClearGr, SIGNAL(clicked()), ui->cbGroup, SLOT(clearEditText()) );

    // send schedule changed
    connect(ui->dropTrash, SIGNAL(sigPairRemoved()), SLOT(sendScheduleChanged()));
    connect(ui->wEditSchedule, SIGNAL(scheduleChanged()),
            SLOT(sendScheduleChanged()));
    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(sendScheduleChanged()));
    connect(&STDataProvider::getInstance(), SIGNAL(pairTypesChanged()),
            SLOT(sendScheduleChanged()));
    connect(&STDataProvider::getInstance(), SIGNAL(teachersChanged()),
            SLOT(sendScheduleChanged()));

    // update schedule edit
    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            ui->wEditSchedule, SLOT(updateSchedule()));
    connect(&STDataProvider::getInstance(), SIGNAL(pairTypesChanged()),
            ui->wEditSchedule, SLOT(updateSchedule()));
    connect(&STDataProvider::getInstance(), SIGNAL(teachersChanged()),
            ui->wEditSchedule, SLOT(updateSchedule()));

    // update height for edit schedule
    connect(ui->dropTrash, SIGNAL(sigPairRemoved()), ui->wEditSchedule,
            SLOT(computeNewHeight()));

    // open container dialog
    connect(ui->cSubjects->pbSettings, SIGNAL(clicked()), editSubject,
            SLOT(show()));
    connect(ui->cPairTypes->pbSettings, SIGNAL(clicked()), editPairType,
            SLOT(show()));
    connect(ui->cTeachers->pbSettings, SIGNAL(clicked()), editTeacher,
            SLOT(show()));

    // drag element
    connect(ui->cSubjects->lwList, SIGNAL(currentRowChanged(int)),
            SLOT(setDragSubject(int)));
    connect(ui->cPairTypes->lwList, SIGNAL(currentRowChanged(int)),
            SLOT(setDragPairType(int)));
    connect(ui->cTeachers->lwList, SIGNAL(currentRowChanged(int)),
            SLOT(setDragTeacher(int)));
    connect(ui->cbAuditory, SIGNAL(editTextChanged(QString)),
            SLOT(setDragAuditory(QString)));
    connect(ui->cbGroup, SIGNAL(editTextChanged(QString)),
            SLOT(setDragAdditions(QString)));

    // permanent tip
    connect(ui->pbHelp, SIGNAL(clicked()), SLOT(helpRequested()));
}

WSettingSchedule::~WSettingSchedule() {
    clearSubjectList();
    clearPairTypeList();
    clearTeacherList();

    delete ui;

    delete editSubject;
    delete editPairType;
    delete editTeacher;
}

void WSettingSchedule::enableFirstLaunchMode() {
    ui->wEditSchedule->enableFirstLaunchMode();
}

void WSettingSchedule::enableSmallScreenSupport() {
    ui->cSubjects->setFixedHeight(222);
    ui->cTeachers->setFixedHeight(222);
    ui->groupFrame->setFixedWidth(370);
    ui->permanentTip->hide();
}

void WSettingSchedule::showEvent(QShowEvent *inSE) {
    fillSubjectList();
    fillPairTypeList();
    fillTeacherList();

    inSE->accept();
}

void WSettingSchedule::addSubjectInList(STSubject *inSubject) {
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(inSubject->nick);

    QVariant var;
    var.setValue(inSubject);
    item->setData(Qt::UserRole, var);

    ui->cSubjects->lwList->addItem(item);
}

void WSettingSchedule::clearSubjectList() {
    QVariant var;
    STSubject *del = NULL;
    while (ui->cSubjects->lwList->count() > 0) {
        var = ui->cSubjects->lwList->item(0)->data(Qt::UserRole);
        del = var.value<STSubject *>();
        delete del;

        delete ui->cSubjects->lwList->takeItem(0);
    }
    ui->cSubjects->lwList->clear();
}

void WSettingSchedule::addPairTypeInList(STPair::Type *inPairType) {
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(inPairType->shortName);

    QVariant var;
    var.setValue(inPairType);
    item->setData(Qt::UserRole, var);

    ui->cPairTypes->lwList->addItem(item);
}

void WSettingSchedule::clearPairTypeList() {
    QVariant var;
    STPair::Type *del = NULL;
    while (ui->cPairTypes->lwList->count() > 0) {
        var = ui->cPairTypes->lwList->item(0)->data(Qt::UserRole);
        del = var.value<STPair::Type *>();
        delete del;

        delete ui->cPairTypes->lwList->takeItem(0);
    }
    ui->cPairTypes->lwList->clear();
}

void WSettingSchedule::addTeacherInList(STTeacher *inTeacher) {
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(inTeacher->nick);

    QVariant var;
    var.setValue(inTeacher);
    item->setData(Qt::UserRole, var);

    ui->cTeachers->lwList->addItem(item);
}

void WSettingSchedule::clearTeacherList() {
    QVariant var;
    STTeacher *del = NULL;
    while (ui->cTeachers->lwList->count() > 0) {
        var = ui->cTeachers->lwList->item(0)->data(Qt::UserRole);
        del = var.value<STTeacher *>();
        delete del;

        delete ui->cTeachers->lwList->takeItem(0);
    }
    ui->cTeachers->lwList->clear();
}

void WSettingSchedule::fillSubjectList() {
    clearSubjectList();

    QVector<STSubject *> *subjects = STDataProvider::getInstance().getSubjects();
    for (int i = 0; i < subjects->size(); i++) {
        addSubjectInList((*subjects)[i]);
    }
    // Change buttons for clearity
    if (subjects->size()) {
        ui->cSubjects->setState(true);
    } else {
        ui->cSubjects->setState(false);
    }

    delete subjects;
}

void WSettingSchedule::fillPairTypeList() {
    clearPairTypeList();

    QVector<STPair::Type *> *pairTypes =
            STDataProvider::getInstance().getPairTypes();
    for (int i = 0; i < pairTypes->size(); i++) {
        addPairTypeInList((*pairTypes)[i]);
    }
    // Change buttons for clearity
    if (pairTypes->size()) {
        ui->cPairTypes->setState(true);
    } else {
        ui->cPairTypes->setState(false);
    }

    delete pairTypes;
}

void WSettingSchedule::fillTeacherList() {
    clearTeacherList();

    QVector<STTeacher *> *teachers = STDataProvider::getInstance().getTeachers();
    for (int i = 0; i < teachers->size(); i++) {
        addTeacherInList((*teachers)[i]);
    }
    // Change buttons for clearity
    if (teachers->size()) {
        ui->cTeachers->setState(true);
    } else {
        ui->cTeachers->setState(false);
    }

    delete teachers;
}

void WSettingSchedule::setDragSubject(int inRow) {
    if (inRow < 0) {
        ui->dragPairCreate->setSubject(NULL);
    } else {
        QVariant var = ui->cSubjects->lwList->item(inRow)->data(Qt::UserRole);
        STSubject *obj = var.value<STSubject *>();
        ui->dragPairCreate->setSubject(obj);
    }
}

void WSettingSchedule::setDragPairType(int inRow) {
    if (inRow < 0) {
        ui->dragPairCreate->setSubject(NULL);
    } else {
        QVariant var = ui->cPairTypes->lwList->item(inRow)->data(Qt::UserRole);
        STPair::Type *obj = var.value<STPair::Type *>();
        ui->dragPairCreate->setPairType(obj);
    }
}

void WSettingSchedule::setDragTeacher(int inRow) {
    if (inRow < 0) {
        ui->dragPairCreate->setSubject(NULL);
    } else {
        QVariant var = ui->cTeachers->lwList->item(inRow)->data(Qt::UserRole);
        STTeacher *obj = var.value<STTeacher *>();
        ui->dragPairCreate->setTeacher(obj);
    }
}

void WSettingSchedule::setDragAuditory(QString inAuditory) {
    ui->dragPairCreate->setAuditory(inAuditory.left(7));
}

void WSettingSchedule::setDragAdditions(QString inAdditions) {
    ui->dragPairCreate->setAdditions(inAdditions.left(5));
}

void WSettingSchedule::sendScheduleChanged() { emit scheduleChanged(); }

void WSettingSchedule::helpRequested() { emit needHelp(); }
