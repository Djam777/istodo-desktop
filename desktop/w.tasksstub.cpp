#include "w.tasksstub.h"
#include "ui_w.tasksstub.h"

WTasksStub::WTasksStub(QWidget *parent)
    : QWidget(parent), ui(new Ui::WTasksStub) {
    ui->setupUi(this);
}

WTasksStub::~WTasksStub() { delete ui; }

void WTasksStub::setState(int inState) {
    switch (inState) {
        case STATE_CALENDAR: {
            ui->lbStubTitle->setText(tr("Week without tasks"));
            ui->lbStubText->setText(tr("or displaying a is blocked by filter"));

            break;
        }
        case STATE_OVERDUE: {
            ui->lbStubTitle->setText(tr("All tasks completed"));
            ui->lbStubText->setText("ignoscito saepe alteri, nunquam tibi");
            break;
        }
        case STATE_ALL: {
            ui->lbStubTitle->setText(tr("Tasks not found"));
            ui->lbStubText->setText(tr("or displaying a is blocked by filter"));
            break;
        }
    }
}
