/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_FILTER_H
#define W_FILTER_H

#include "include.h"

namespace Ui {
    class WFilter;
}

class WFilter : public QWidget {
        Q_OBJECT

    signals:
        void filtersChanged();

    public:
        explicit WFilter(QWidget *parent = 0);
        ~WFilter();

        STFilterElement currentFilter;

    private:
        Ui::WFilter *ui;
        QVector<int> currentSubjects;

    private slots:
        void checkAll();
        void uncheckAll();
        void oneSubjChanged();
        void subjectsChanged();
};

#endif  // W_FILTER_H
