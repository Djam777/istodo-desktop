#include "d.editteacher.h"
#include "ui_d.editteacher.h"

DEditTeacher::DEditTeacher(QWidget *parent)
    : QDialog(parent), ui(new Ui::DEditTeacher) {
    ui->setupUi(this);

    // Select only one item
    ui->twTeachers->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->twTeachers->setSelectionMode(QAbstractItemView::SingleSelection);

    ui->twTeachers->installEventFilter(this);

    ui->twTeachers->setObjectName("twDialog");
    ui->twTeachers->verticalHeader()->hide();

    ui->twTeachers->setColumnWidth(0, 41);
    ui->twTeachers->setColumnWidth(1, 170);
    ui->twTeachers->setColumnWidth(2, 280);

    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint);

    ui->twTeachers->hideColumn(0);
    ui->pbDelRightLight->setChecked(false);

    connect(ui->pbReady, SIGNAL(clicked()), SLOT(close()));
    connect(ui->pbAddLeftLight, SIGNAL(clicked()), SLOT(addTeacher()));
    connect(ui->pbDelRightLight, SIGNAL(toggled(bool)), SLOT(showDelete(bool)));
}

DEditTeacher::~DEditTeacher() {
    clearTable();

    delete ui;
}

void DEditTeacher::showEvent(QShowEvent *inSE) {
    fillTable();

    ui->twTeachers->resizeRowsToContents();

    inSE->accept();
}

void DEditTeacher::closeEvent(QCloseEvent *inCE) {
    disableTableEditMode();

    QSettings settings;
    if (isHaveFakeTeachers() && settings.value(SET_DELETE_ITEM).toBool()) {
        int rc = QMessageBox::warning(
                    this, tr("Warning"),
                    tr("All teachers with empty nickname will be removed, continue?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::YesToAll);
        if (rc == QMessageBox::No) {
            inCE->ignore();
            return;
        }
        if (rc == QMessageBox::YesToAll) {
            settings.setValue(SET_DELETE_ITEM, false);
        }
    }

    inCE->accept();
}

bool DEditTeacher::eventFilter(QObject *inObject, QEvent *inEvent) {
    if (inEvent->type() != QEvent::KeyPress) {
        return false;
    }
    QKeyEvent *keyEvent = NULL;
    keyEvent = static_cast<QKeyEvent *>(inEvent);

    QList<QTableWidgetItem *> selectedItems = ui->twTeachers->selectedItems();
    if (selectedItems.size() != 1) {
        return false;
    }
    QTableWidgetItem *item = selectedItems.first();

    int column = item->column();
    int row = item->row();

    if (keyEvent->key() == Qt::Key_Down) {
        row++;
    }
    if (keyEvent->key() == Qt::Key_Up) {
        row--;
    }

    switch (keyEvent->key()) {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        case Qt::Key_Up:
        case Qt::Key_Down: {
            disableTableEditMode();

            ui->twTeachers->setCurrentCell(row, column);
            ui->twTeachers->editItem(ui->twTeachers->item(row, column));
            keyEvent->accept();
            return true;
        }
        default: { break; }
    }
    return false;
}

bool DEditTeacher::isHaveFakeTeachers() {
    STTeacher *teacher = NULL;
    QVariant var;
    for (int i = 0; i < ui->twTeachers->rowCount(); i++) {
        var = ui->twTeachers->item(i, 0)->data(Qt::UserRole);
        teacher = var.value<STTeacher *>();
        if (teacher->id == INVALID_ID || teacher->nick == "") {
            return true;
        }
    }

    return false;
}

void DEditTeacher::clearTable() {
    ui->twTeachers->disconnect();

    QVariant var;
    STTeacher *del = NULL;
    while (ui->twTeachers->rowCount() > 0) {
        delete ui->twTeachers->cellWidget(0, 0);

        var = ui->twTeachers->item(0, 0)->data(Qt::UserRole);
        del = var.value<STTeacher *>();
        delete del;

        ui->twTeachers->removeRow(0);
    }
}

void DEditTeacher::addTeacherInTable(STTeacher *inTeacher) {
    disableTableEditMode();

    QPushButton *pb;
    QTableWidgetItem *item;
    QVariant var;

    int index = ui->twTeachers->rowCount();
    ui->twTeachers->insertRow(index);

    pb = new QPushButton;
    pb->setFixedSize(40, 24);
    pb->setAutoDefault(false);
    pb->setObjectName("pbDelMiddleDark");
    connect(pb, SIGNAL(clicked()), SLOT(delTeacher()));
    ui->twTeachers->setCellWidget(index, 0, pb);

    item = new QTableWidgetItem();
    var.setValue(inTeacher);
    item->setData(Qt::UserRole, var);
    ui->twTeachers->setItem(index, 0, item);

    item = new QTableWidgetItem();
    item->setText(inTeacher->nick);
    var.setValue(&inTeacher->nick);
    item->setData(Qt::UserRole, var);
    QBrush brsh(QColor(EMPTY_COLOR));
    item->setBackground(brsh);
    ui->twTeachers->setItem(index, 1, item);
    if (inTeacher->id == INVALID_ID) {
        // If it's new pair edit them
        ui->twTeachers->editItem(item);
        ui->twTeachers->setCurrentItem(item);
    }

    item = new QTableWidgetItem();
    item->setText(inTeacher->name);
    var.setValue(&inTeacher->name);
    item->setData(Qt::UserRole, var);
    ui->twTeachers->setItem(index, 2, item);

    item = new QTableWidgetItem();
    item->setText(inTeacher->additions);
    var.setValue(&inTeacher->additions);
    item->setData(Qt::UserRole, var);
    ui->twTeachers->setItem(index, 3, item);
}

void DEditTeacher::fillTable() {
    clearTable();

    QVector<STTeacher *> *teachers = STDataProvider::getInstance().getTeachers();
    for (int i = 0; i < teachers->size(); i++) {
        addTeacherInTable((*teachers)[i]);
    }
    delete teachers;

    connect(ui->twTeachers, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editTeacher(QTableWidgetItem *)));
}

void DEditTeacher::disableTableEditMode() {
    // This code need for disable edit mode
    QTableWidgetItem *item = ui->twTeachers->currentItem();

    ui->twTeachers->setDisabled(true);
    ui->twTeachers->setDisabled(false);

    if (item) {
        editTeacher(item);
    }
}

void DEditTeacher::addTeacher() {
    ui->twTeachers->disconnect();

    STTeacher *add = new STTeacher;
    add->id = INVALID_ID;

    addTeacherInTable(add);

    connect(ui->twTeachers, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editTeacher(QTableWidgetItem *)));
}

void DEditTeacher::editTeacher(QTableWidgetItem *inItem) {
    if (inItem->column() == 0) {
        return;
    }

    QVariant var = inItem->data(Qt::UserRole);
    QString &str = *var.value<QString *>();

    var = ui->twTeachers->item(inItem->row(), 0)->data(Qt::UserRole);
    STTeacher *edit = var.value<STTeacher *>();

    switch (inItem->column()) {
        // nick
        case 1: {
            if (inItem->text().length() > 22) {
                inItem->setText(inItem->text().mid(0, 22));
            }
            str = inItem->text();
            break;
        }
            // name
        case 2: {
            if (inItem->text() == "") {
                inItem->setText(str);
            } else {
                str = inItem->text();
            }
            break;
        }
            // other fields
        default: { str = inItem->text(); }
    }

    if (edit->nick != "") {
        if (edit->id == INVALID_ID) {
            edit->id = STDataProvider::getInstance().addTeacher(*edit);
        } else {
            STDataProvider::getInstance().modTeacher(*edit);
        }
    }
}

void DEditTeacher::delTeacher() {
    ui->twTeachers->disconnect();

    STTeacher *del = NULL;
    for (int i = 0; i < ui->twTeachers->rowCount(); i++) {
        if (sender() == ui->twTeachers->cellWidget(i, 0)) {
            QVariant var;
            var = ui->twTeachers->item(i, 0)->data(Qt::UserRole);
            del = var.value<STTeacher *>();
            ui->twTeachers->removeRow(i);
            break;
        }
    }
    if (del == NULL) {
        qDebug() << "Error on delete teacher";
        return;
    }

    STDataProvider::getInstance().delTeacher(del->id);
    delete del;

    connect(ui->twTeachers, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editTeacher(QTableWidgetItem *)));
}

void DEditTeacher::showDelete(bool inShow) {
    if (inShow) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this, tr("Warning"),
                                     tr("Teacher removing causes removing all classes with this teacher, continue?"),
                                     QMessageBox::Yes | QMessageBox::No);
        if (reply != QMessageBox::Yes) {
            ui->pbDelRightLight->blockSignals(true);
            ui->pbDelRightLight->setChecked(false);
            ui->pbDelRightLight->blockSignals(false);
            return;
        }
        ui->twTeachers->showColumn(0);
    } else {
        ui->twTeachers->hideColumn(0);
    }
}
