/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_ABOUT_H
#define W_ABOUT_H

#include <QWidget>

namespace Ui {
    class WAbout;
}

class WAbout : public QWidget {
        Q_OBJECT

    public:
        explicit WAbout(QWidget *parent = 0);
        ~WAbout();

    private:
        Ui::WAbout *ui;

    private slots:
        void onCheckBox(bool toggled);
};

#endif  // W_ABOUT_H
