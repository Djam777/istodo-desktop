#include "w.dragpaircreate.h"

WDragPairCreate::WDragPairCreate(QWidget *parent) : QLabel(parent) {
    //  Pair
    pair = new STPair;
    pair->subjectId = INVALID_ID;
    pair->typeId = INVALID_ID;
    pair->teacherId = INVALID_ID;

    pair->week = INVALID_ID;
    pair->dayOfWeek = INVALID_ID;
    pair->gridNumber = INVALID_ID;

    pair->auditory = "";
    pair->additions = "";

    setCursor(Qt::OpenHandCursor);
    setMinimumHeight(110);

    isLaunch = true;
}

WDragPairCreate::~WDragPairCreate() { delete pair; }

void WDragPairCreate::setSubject(STSubject *inSubject) {
    if (inSubject == NULL) {
        subject.clear();
        pair->subjectId = INVALID_ID;
    } else {
        subject = inSubject->nick;
        if (subject.length() > 13) {
            subject = subject.mid(0, 13);
        }
        pair->subjectId = inSubject->id;
    }
    updateText();
}

void WDragPairCreate::setPairType(STPair::Type *inType) {
    if (inType == NULL) {
        pairType.clear();
        pair->typeId = INVALID_ID;
    } else {
        // Check full type name
        if (!inType->fullName.isEmpty()) {
            pairType = inType->fullName;
        } else {
            pairType = inType->shortName;
        }
        // Check type lenght
        if (pairType.length() > 12) {
            pairType = pairType.mid(0, 12);
        }
        pair->typeId = inType->id;
    }
    updateText();
}

void WDragPairCreate::setTeacher(STTeacher *inTeacher) {
    if (inTeacher == NULL) {
        teacher.clear();
        pair->teacherId = INVALID_ID;
    } else {
        teacher = inTeacher->nick;
        if (teacher.length() > 12) {
            teacher = teacher.mid(0, 12);
        }
        pair->teacherId = inTeacher->id;
    }
    updateText();
}

void WDragPairCreate::setAuditory(QString inStr) {
    pair->auditory = inStr;
    updateText();
}

void WDragPairCreate::setAdditions(QString inStr) {
    pair->additions = inStr;
    updateText();
}

void WDragPairCreate::mousePressEvent(QMouseEvent *inME) {
    if (pair->subjectId != INVALID_ID && inME->button() == Qt::LeftButton) {
        QDrag *drag = new QDrag(this);

        QMimeData *mimeData = new QMimeData;
        QByteArray data((char *)&pair, sizeof(pair));
        mimeData->setData("STPair", data);

        QPixmap pix(":/img/other/images/others/dragTag.png");

        drag->setMimeData(mimeData);
        drag->setPixmap(pix);

        drag->exec(Qt::CopyAction);

        inME->accept();
    }
}

void WDragPairCreate::showEvent(QShowEvent*) {
    if (isLaunch) {
        QString strStart =
                " <html><body><img src=\":/img/icons/images/icons/dragIcon.png\" "
                "style=\"float: right;\"/>";
        strStart +=
                tr("<p style = \" margin:5px; margin-top:2px; font-family: Arial; "
                "font-size: 16px; color:#333; \">", "14px for ru locale");
        QString strEnd = "</p></body></html>";

        setText(strStart +
                tr("Select data<br>from <b>all</b> lists,<br>and drag<br>" \
                   "<b>this</b> element to timetable.") + strEnd);
        isLaunch = false;
    } else {
        updateText();
    }
}

void WDragPairCreate::updateText() {
    QString strStart =
            " <html><body><img src=\":/img/icons/images/icons/dragIcon.png\" "
            "style=\"float: right;\"/>";
    strStart +=
            "<p style = \" margin:5px; margin-top:6px; font-family: Arial; "
            "font-size: 16px; color:#333; \">";
    QString strSubj =
            "</p><div style = \" margin:5px; font-family: Arial; font-size: 26px; "
            "color:#333; \">";
    QString strEnd = "</div></body></html>";

    setText(strStart + teacher + "<br>" + pairType + "<br>" + pair->auditory +
            "  " + pair->additions + strSubj + subject + strEnd);
}
