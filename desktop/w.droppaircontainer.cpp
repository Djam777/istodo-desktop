#include "w.droppaircontainer.h"

WDropPairContainer::WDropPairContainer(int inWeek, int inDay, int inGrid,
                                       QWidget *inParent)
    : QFrame(inParent) {
    week = inWeek;
    day = inDay;
    grid = inGrid;

    QVBoxLayout *lay = new QVBoxLayout(this);
    lay->setMargin(0);
    lay->setSpacing(5);
    setLayout(lay);

    setAcceptDrops(true);
}

void WDropPairContainer::addPair(STPair *inPair) {
    if (!inPair) {
        return;
    }
    WDragPairMove *movePair = new WDragPairMove(inPair, this);
    this->layout()->addWidget(movePair);
}

void WDropPairContainer::dragEnterEvent(QDragEnterEvent *inDEE) {
    if (inDEE->mimeData()->hasFormat("STPair")) {
        inDEE->accept();
    } else {
        inDEE->ignore();
    }
}

void WDropPairContainer::dropEvent(QDropEvent *inDE) {
    if (inDE->mimeData()->hasFormat("STPair")) {
        const QMimeData *mimeData = inDE->mimeData();
        QByteArray *ba = new QByteArray(mimeData->data("STPair"));
        QDataStream ds(ba, QIODevice::ReadOnly);
        STPair *pair;
        ds.readRawData((char *)&pair, sizeof(pair));
        delete ba;

        pair->week = week;
        pair->dayOfWeek = day;
        pair->gridNumber = grid;

        if (inDE->dropAction() == Qt::MoveAction) {
            STDataProvider::getInstance().modPair(*pair);
        }
        if (inDE->dropAction() == Qt::CopyAction) {
            pair->id = STDataProvider::getInstance().addPair(*pair);
        }

        addPair(pair);
        emit sigPairAdded();

        inDE->accept();
    } else {
        inDE->ignore();
    }
}
