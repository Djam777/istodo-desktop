/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_STODO_H
#define W_STODO_H

#define W_STATE_SCHEDULE 0
#define W_STATE_TASK_DIALOG 1

#include "include.h"
#include "w.about.h"
#include "w.calendar.h"
#include "st.updaterclient.h"
#include "w.taskedit.h"
#include "w.settingschedule.h"
#include "w.settinggeneral.h"
#include "w.pageshow.h"
#include "d.settingsemester.h"

namespace Ui {
    class WStodo;
}

class WStodo : public QMainWindow {
        Q_OBJECT

    public:
        explicit WStodo(QWidget *parent = 0);
        ~WStodo();
        void enableSmallScreenSupport();

    protected:
        void showEvent(QShowEvent *inSE);
        void closeEvent(QCloseEvent *inCE);

    private:
        Ui::WStodo *ui;
        WSettingGeneral *settingGeneral;
        WSettingSchedule *settingSchedule;
        DSettingSemester *settingSemester;
        WAbout *about;
        QDate selectedDate;
        int currentState;
        STUpdaterClient *updater;
        WPageShow *updaterWidget;

    private slots:
        void showSettingSchedule();
        void changeSelectedDate(QDate inDate);

        void updateUiOnSelectedDate();
        void updateTasksUi();
        void updateSheduleUi();
        void updateTasks();
        void updateFilters();

        void slotIncWeek();
        void slotDecWeek();
        void slotHome();
        void slotIncMonth();
        void slotDecMonth();

        void switchStateToDialog(int inId);
        void switchStateToSchedule();
        void finishSwitch();

        void importDatabase();
        void exportDatabase();
        void exportICalSchedule();
        void exportICalTasks();
        void dbStartExport();
        void dbFinishExport();
        void dbStartImport();
        void dbFinishImport();
        void dbOnError();

        void openTwitter();
        void openVk();
        void openHelp();
        void openSupport();
};

#endif  // W_STODO_H
