#include "st.dropboxexporter.h"

STDropboxExporter::STDropboxExporter(QObject *parent) :
    QObject(parent)
{
    dropbox = new QDropbox("only_in_version", "from_site_istodo.ru");

    connect(dropbox, SIGNAL(tokenExpired()),
            this, SLOT(authorizeUser()));
    connect(dropbox, SIGNAL(requestTokenFinished(QString,QString)),
            this, SLOT(requestAccessToken()));
    connect(dropbox, SIGNAL(accessTokenFinished(QString,QString)),
            this, SLOT(authorized()));
    connect(dropbox, SIGNAL(errorOccured(QDropbox::Error)),
            this, SLOT(dropboxError()));
}

STDropboxExporter::~STDropboxExporter()
{
    // Double free? Crash in destructor
    //delete dropbox;
}

void STDropboxExporter::startImport()
{
    this->isImport = true;
    start();
}

void STDropboxExporter::startExport()
{
    this->isImport = false;
    start();
}

void STDropboxExporter::start()
{
    QSettings settings;
    if (settings.value(SET_TOKEN).toString().isEmpty() ||
            settings.value(SET_TOKEN_SECRET).toString().isEmpty()) {
        dropbox->requestToken();
    } else {
        dropbox->setToken( settings.value(SET_TOKEN).toString() );
        dropbox->setTokenSecret( settings.value(SET_TOKEN_SECRET).toString() );
        this->authorized();
    }
}

void STDropboxExporter::requestAccessToken()
{
    dropbox->requestAccessToken();
}

void STDropboxExporter::authorizeUser()
{
    QDesktopServices::openUrl(dropbox->authorizeLink());

    int rc = QMessageBox::information(NULL,
                             tr("Authorization"),
                             tr("After logging on to Dropbox - click Ok"),
                             QMessageBox::Ok | QMessageBox::Cancel);
    if (rc == QMessageBox::Cancel) {
        return;
    }

    dropbox->requestAccessToken();
}

void STDropboxExporter::authorized()
{
    QSettings settings;
    settings.setValue(SET_TOKEN, dropbox->token());
    settings.setValue(SET_TOKEN_SECRET, dropbox->tokenSecret());

    QDropboxFile dbFile("/sandbox/iStodo.db", dropbox);
    if (isImport) {
        if (!dbFile.open(QIODevice::ReadOnly)) {
            qDebug() << "db file open error";
            emit onError();
            return;
        }

        QString path =
                QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        if (!QDir(path).exists()) {
            QDir().mkpath(path);
        }
        QString name = path + QDir::separator() + "dbTempFile";

        QFile tempFile(name);
        if (!tempFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            qDebug() << "db file open error";
            emit onError();
            return;
        }

        tempFile.write( dbFile.readAll() );
        tempFile.close();

        STDataProvider::getInstance().importDatabase(name);

        emit onImportFinished();
    }
    else {
        if (!dbFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            qDebug() << "db file open error";
            emit onError();
            return;
        }

        QFile istodoFile( STDataProvider::getInstance().getDatabaseName() );
        if (!istodoFile.open(QIODevice::ReadOnly)) {
            qDebug() << "istodo file open error";
            emit onError();
            return;
        }

        dbFile.write( istodoFile.readAll() );

        emit onExportFinished();
    }
}

void STDropboxExporter::dropboxError()
{
    emit onError();
}
