#-------------------------------------------------
#
# Project created by QtCreator 2013-04-06T12:44:59
#
#-------------------------------------------------

QT       += widgets core gui network xml
QTPLUGIN += qsqlite

TARGET = iStodo
TEMPLATE = app
CONFIG += release

unix: !macx {
    LIBS += -ldl
}

mac {
    ICON = $${PWD}/iStodo.icns
    QMAKE_INFO_PLIST= $${PWD}/Info.plist
    QMAKE_CFLAGS += -gdwarf-2
    QMAKE_CXXFLAGS += -gdwarf-2
#    QMAKE_CXXFLAGS += -stdlib=libc++ -std=c++11
}
#!macx:CONFIG += c++11
win32:RC_FILE = iStodo.rc

include(../qtdropbox/libqtdropbox.pri)
include(../dataprovider/dataprovider.pri)
include(../updaterCommon/updaterCommon.pri)

SOURCES += \
    w.stodo.cpp \
    w.settingschedule.cpp \
    w.container.cpp \
    d.settingsemester.cpp \
    d.editteacher.cpp \
    w.calendar.cpp \
    main.cpp \
    d.editpairtype.cpp \
    d.editsubject.cpp \
    w.editschedule.cpp \
    w.dragpaircreate.cpp \
    w.droppaircontainer.cpp \
    w.dragpairmove.cpp \
    w.droppairtrash.cpp \
    w.scheduleonweek.cpp \
    w.filter.cpp \
    w.daystasks.cpp \
    w.taskline.cpp \
    w.taskedit.cpp \
    w.tasksonweek.cpp \
    st.filterelement.cpp \
    st.updaterclient.cpp \
    w.about.cpp \
    w.settinggeneral.cpp \
    w.pageshow.cpp \
    w.tasksstub.cpp \
    st.dropboxexporter.cpp

HEADERS  += \
    include.h \
    w.stodo.h \
    w.settingschedule.h \
    w.container.h \
    d.settingsemester.h \
    d.editteacher.h \
    w.calendar.h \
    d.editpairtype.h \
    d.editsubject.h \
    w.editschedule.h \
    w.dragpaircreate.h \
    w.droppaircontainer.h \
    w.dragpairmove.h \
    w.droppairtrash.h \
    w.scheduleonweek.h \
    w.filter.h \
    w.daystasks.h \
    w.taskline.h \
    w.taskedit.h \
    w.tasksonweek.h \
    st.filterelement.h \
    st.updaterclient.h \
    w.about.h \
    w.settinggeneral.h \
    w.pageshow.h \
    w.tasksstub.h \
    st.dropboxexporter.h

FORMS    += \
    w.stodo.ui \
    w.settingschedule.ui \
    w.container.ui \
    d.settingsemester.ui \
    d.editteacher.ui \
    w.calendar.ui \
    d.editpairtype.ui \
    d.editsubject.ui \
    w.editschedule.ui \
    w.scheduleonweek.ui \
    w.filter.ui \
    w.daystasks.ui \
    w.taskline.ui \
    w.taskedit.ui \
    w.tasksonweek.ui \
    w.about.ui \
    w.settinggeneral.ui \
    w.pageshow.ui \
    w.tasksstub.ui

RESOURCES += \
    res.qrc

OTHER_FILES += \
    style.css \
    tables_frames.css \
    buttons.css \
    scrollbars.css

macx{
    message("Copy translations")
    system(mkdir $$OUT_PWD/iStodo.app/Contents/Languages)
    system(cp -R -v $$PWD/../Languages/*.qm $$OUT_PWD/iStodo.app/Contents/Languages)

    message("copy QtDropbox")
    system(mkdir $$OUT_PWD/iStodo.app/Contents/Frameworks)
    system(cp -R -v $$PWD/../qtdropbox/lib/*.dylib $$OUT_PWD/iStodo.app/Contents/Frameworks)
    system(cp -R -v $$PWD/../qtdropbox/lib/*.dylib $$OUT_PWD/iStodo.app/Contents/MacOS)
    system(cp -R -v $${PWD}/Info.plist $$OUT_PWD/iStodo.app/Contents)
}
unix: !macx{
    message("Copy translations")
    system(mkdir $$OUT_PWD/Languages)
    system(cp -R -v $$PWD/../Languages/*.qm $$OUT_PWD/Languages)
}
