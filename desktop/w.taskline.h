/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_TASKLINE_H
#define W_TASKLINE_H

#include "include.h"

namespace Ui {
    class WTaskLine;
}

class WTaskLine : public QWidget {
        Q_OBJECT
    signals:
        void clicked(int inId);

    public:
        explicit WTaskLine(QWidget *parent = 0);
        ~WTaskLine();

        void fillTask(STTask *inTask);

        int id;

    private:
        void mousePressEvent(QMouseEvent *ev);

        STTask *currTask;
        Ui::WTaskLine *ui;

    private slots:
        void stateChanged(bool inState);
};

#endif  // W_TASKLINE_H
