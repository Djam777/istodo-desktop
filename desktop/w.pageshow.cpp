#include "w.pageshow.h"
#include "ui_w.pageshow.h"

WPageShow::WPageShow(QWidget *parent) : QWidget(parent), ui(new Ui::WPageShow) {
    ui->setupUi(this);

    connect(ui->pbReady, SIGNAL(clicked()), SLOT(close()));

    connect(ui->pbUpdate, SIGNAL(clicked()), SLOT(onUpdateButton()));

    this->setWindowTitle(tr("New version of iStodo avalible"));
    // Qt is smart lib, it covers my mistakes
    this->setWindowFlags(Qt::Sheet);
}

WPageShow::~WPageShow() { delete ui; }

void WPageShow::onUpdateButton() {
    QDesktopServices::openUrl(QUrl(STODO_UPDATER_CHANGELOG));
}
