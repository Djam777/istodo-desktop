#include "w.filter.h"
#include "ui_w.filter.h"

WFilter::WFilter(QWidget* parent) : QWidget(parent), ui(new Ui::WFilter) {
    ui->setupUi(this);
    subjectsChanged();

    connect(ui->pbCheckedLeftBlue, SIGNAL(clicked()),
            SLOT(checkAll()));

    connect(ui->pbUncheckedRightBlue, SIGNAL(clicked()),
            SLOT(uncheckAll()));

    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(subjectsChanged()));
}

WFilter::~WFilter() { delete ui; }

void WFilter::checkAll() {
    currentFilter.unselectedSubjects.clear();

    for (int i = 0; i < ui->twFilters->rowCount(); i++) {

        QWidget* widget = ui->twFilters->cellWidget(i, 0);
        ((QCheckBox*)widget)->blockSignals(true);
        ((QCheckBox*)widget)->setChecked(true);
        ((QCheckBox*)widget)->blockSignals(false);
    }

    emit filtersChanged();
}

void WFilter::uncheckAll() {
    currentFilter.unselectedSubjects.clear();

    for (int i = 0; i < ui->twFilters->rowCount(); i++) {

        QWidget* widget = ui->twFilters->cellWidget(i, 0);
        ((QCheckBox*)widget)->blockSignals(true);
        ((QCheckBox*)widget)->setChecked(false);
        ((QCheckBox*)widget)->blockSignals(false);
    }

    for (int i = 0; i < currentSubjects.size(); i++) {
        currentFilter.unselectedSubjects.push_back(currentSubjects[i]);
    }

    emit filtersChanged();
}

void WFilter::oneSubjChanged() {
    currentFilter.unselectedSubjects.clear();

    for (int i = 0; i < ui->twFilters->rowCount(); i++) {
        QWidget* widget = ui->twFilters->cellWidget(i, 0);
        if (!((QCheckBox*)widget)->isChecked()) {
            currentFilter.unselectedSubjects.push_back(currentSubjects[i]);
        }
    }

    emit filtersChanged();
}

void WFilter::subjectsChanged() {
    // Clear old subjs
    for (int i = 0; i < ui->twFilters->rowCount(); i++) {
        delete ui->twFilters->cellWidget(i, 0);
    }
    ui->twFilters->setRowCount(0);
    currentSubjects.clear();
    currentFilter.unselectedSubjects.clear();

    QCheckBox* buffCheckBox;

    // Add general perposes tasks
    buffCheckBox = new QCheckBox(tr("General"));
    buffCheckBox->setChecked(true);

    connect(buffCheckBox, SIGNAL(toggled(bool)), SLOT(oneSubjChanged()));

    currentSubjects.push_back(INVALID_ID);

    buffCheckBox->setObjectName("cbFilterCheck");

    buffCheckBox->setFixedSize(223, 33);

    int index = ui->twFilters->rowCount();
    ui->twFilters->insertRow(index);
    ui->twFilters->setCellWidget(index, 0, buffCheckBox);

    QVector<STSubject*>* subjects = STDataProvider::getInstance().getSubjects();
    for (int i = 0; i < subjects->size(); i++) {
        buffCheckBox = new QCheckBox((*subjects)[i]->nick);
        buffCheckBox->setChecked(true);

        connect(buffCheckBox, SIGNAL(toggled(bool)), SLOT(oneSubjChanged()));

        currentSubjects.push_back((*subjects)[i]->id);

        buffCheckBox->setObjectName("cbFilterCheck");
        buffCheckBox->setFixedSize(223, 33);

        int index = ui->twFilters->rowCount();
        ui->twFilters->insertRow(index);
        ui->twFilters->setCellWidget(index, 0, buffCheckBox);
    }
    if (subjects != NULL) {
        clearQVector<STSubject>(*subjects);
        delete subjects;
        subjects = NULL;
    }
}
