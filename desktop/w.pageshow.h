/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_UPDATER_H
#define W_UPDATER_H

#include "include.h"

#define STODO_UPDATER_CHANGELOG "http://istodo.ru/changelog.html"

namespace Ui {
    class WPageShow;
}

class WPageShow : public QWidget {
        Q_OBJECT

    public:
        explicit WPageShow(QWidget *parent = 0);
        ~WPageShow();

    private:
        Ui::WPageShow *ui;

    private slots:
        void onUpdateButton();
};

#endif  // W_UPDATER_H
