/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_EDITSCHEDULE_H
#define W_EDITSCHEDULE_H

#include "include.h"
#include "w.droppaircontainer.h"

#define TIME_EDIT_COLUMN 0
#define TIME_SHOW_COLUMN 1
#define FIRST_PAIR_COLUMN 2
#define LAST_PAIR_COLUMN FIRST_PAIR_COLUMN + 6
#define PAIR_BREAK_SIZE_MIN 10

namespace Ui {
    class WEditSchedule;
}

class WEditSchedule : public QWidget {
        Q_OBJECT

    signals:
        void scheduleChanged();

    public:
        explicit WEditSchedule(QWidget *parent = 0);
        ~WEditSchedule();
        void enableFirstLaunchMode();

    public slots:
        void updateSchedule();
        void fullUpdateSchedule();
        void computeNewHeight();
        void computeNewWidth();

    protected:
        void showEvent(QShowEvent *inSE);
        void resizeEvent(QResizeEvent *inRE);

    private:
        Ui::WEditSchedule *ui;

        QAction *addWeek;
        QAction *cloneWeek;
        QAction *delWeek;
        QMenu *weekMenu;

        QVector<QTime> *bells;
        QTimer endResizeTimer;

        int curWeek;
        int nextWeek;

        bool setCurrentWeek(int inWeek);

        void fillScheduleOnWeek(int inWeek);
        void clearSchedule();

    private slots:
        int minutesBetween(QTime first, QTime second);
        void addBell();
        void delBell();
        void modBell();
        void correctBell(const QTime &inTime);
        void setEditEnabled(bool inEnabled);

        void slotAddWeek();
        void slotCloneWeek();
        void slotDelWeek();

        void incCurrentWeek();  // increment
        void decCurrentWeek();

        void sendScheduleChanged();
};

#endif  // W_EDITSCHEDULE_H
