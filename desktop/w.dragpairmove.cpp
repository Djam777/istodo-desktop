#include "w.dragpairmove.h"

WDragPairMove::WDragPairMove(STPair *inPair, QWidget *parent) : QLabel(parent) {
    pair = new STPair;
    setPair(inPair);
    setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

WDragPairMove::~WDragPairMove() { delete pair; }

void WDragPairMove::setPair(STPair *inPair) {
    *pair = *inPair;

    QString strStart = " <html><body><p>";
    QString strEnd = "</p></body></html>";

    QString subject, type, teacher;
    if (pair->subjectId != INVALID_ID) {
        subject = STDataProvider::getInstance().getSubject(pair->subjectId)->nick;
    }
    if (pair->typeId != INVALID_ID) {
        type = STDataProvider::getInstance().getPairType(pair->typeId)->fullName;
    }
    if (pair->teacherId != INVALID_ID) {
        teacher = STDataProvider::getInstance().getTeacher(pair->teacherId)->nick;
    }

    setText(strStart + type + "<br><b>" + subject + "</b><br>" + teacher +
            "<br>" + pair->auditory + "  " + pair->additions + strEnd);
}

void WDragPairMove::mousePressEvent(QMouseEvent *inME) {
    if (inME->button() == Qt::LeftButton) {
        QDrag *drag = new QDrag(this);

        QMimeData *mimeData = new QMimeData;
        QByteArray data((char *)&pair, sizeof(pair));
        mimeData->setData("STPair", data);

#ifdef Q_OS_WIN32
        QPixmap pix(":/img/other/images/others/moveTag.png");
#else
        QPixmap pix(this->size());
        this->render(&pix);

        drag->setHotSpot(inME->pos());
#endif

        drag->setMimeData(mimeData);
        drag->setPixmap(pix);

        Qt::DropAction dropAct = drag->exec(Qt::MoveAction);

        if (dropAct == Qt::MoveAction) {
            close();
        }

        inME->accept();
    }
}
