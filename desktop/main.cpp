#include "include.h"
#include "w.stodo.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

#ifdef Q_OS_LINUX
    // Kludge, remove after Qt 5.4
    qputenv("QT_QPA_FONTDIR", "/usr/share/fonts");

    // Load the embedded font.
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);
    if (fontId != -1) {
        QFont font("Ubuntu-R");
        font.setPixelSize(12);
        a.setFont(font);
    }

    // Force gtk style
    QString desktopSession = qgetenv("DESKTOP_SESSION").constData();
    QString currDesktop = qgetenv("XDG_CURRENT_DESKTOP").constData();
    qDebug() << currDesktop << desktopSession;
    if(currDesktop != "KDE" && currDesktop != "kde-plasma" &&
            desktopSession != "KDE" && desktopSession != "kde-plasma") {
        QApplication::setStyle(QStyleFactory::create("gtk"));
        qDebug() << "Gtk based desktop";
    }
#endif

#ifdef Q_OS_MAC
    QCoreApplication::setOrganizationName("iStodo team");
#endif
    QCoreApplication::setOrganizationDomain("iStodo.ru");
    QCoreApplication::setApplicationName("iStodo");
    QCoreApplication::setApplicationVersion(ST_STR_VERSION);

    QSettings settings;
    if (!settings.contains(SET_DELETE_ITEM)) {
        settings.setValue(SET_DELETE_ITEM, true);
    }
    if (!settings.contains(SET_DELETE_BELL)) {
        settings.setValue(SET_DELETE_BELL, true);
    }

    qSetMessagePattern("[%{file}:%{line}] %{type}: %{message}");

    // Localize app
#ifdef Q_OS_OSX
    QString langsFolder = "../Languages";
#else
    QString langsFolder = "Languages";
#endif
    QString appTranslatorFileName = QLatin1String("iStodo_");
    appTranslatorFileName += QLocale::system().name();
    QTranslator *appTranslator = new QTranslator(&a);
    if (appTranslator->load(appTranslatorFileName, langsFolder))
        a.installTranslator(appTranslator);

    // TODO: translate qt messages and buttons in MessageBoxes

    QFile styleF, buttonsF, tablesF, scrollsF;

    styleF.setFileName(":/qss/style.css");
    buttonsF.setFileName(":/qss/buttons.css");
    tablesF.setFileName(":/qss/tables_frames.css");
    scrollsF.setFileName(":/qss/scrollbars.css");

    styleF.open(QFile::ReadOnly);
    buttonsF.open(QFile::ReadOnly);
    tablesF.open(QFile::ReadOnly);
    scrollsF.open(QFile::ReadOnly);

    QString qssStr = styleF.readAll();
    qssStr += buttonsF.readAll();
    qssStr += tablesF.readAll();

#ifndef Q_OS_MAC
    qssStr +=
            "QLabel#lbCenterBlock, QLabel#lbHeaderTitle, QPushButton#pbShowAll, QPushButton#pbOverDue,\
            QPushButton#pbEditRaspTitle {font-weight:bold;}";
            qssStr += scrollsF.readAll();
#endif

#ifdef Q_OS_LINUX
    qssStr +=
            "QScrollBar:vertical, QScrollBar:horizontal { background: #e4e4e4;}";
#endif

    qApp->setStyleSheet(qssStr);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
    qApp->setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif

    WStodo w;
    if (QApplication::desktop()->width() <= 1024) {
        w.enableSmallScreenSupport();
    } else {
        w.setMinimumWidth(1150);
    }
    return a.exec();
}
