#include "w.stodo.h"
#include "ui_w.stodo.h"

#include "st.dropboxexporter.h"

WStodo::WStodo(QWidget *parent) : QMainWindow(parent), ui(new Ui::WStodo) {
    ui->setupUi(this);

    about = new WAbout();
    settingSchedule = new WSettingSchedule();
    settingSemester = new DSettingSemester(this);
    changeSelectedDate(QDate::currentDate());

    // Mac menu
    ui->actSettingSemester->setMenuRole(QAction::PreferencesRole);
    ui->actAbout->setMenuRole(QAction::AboutRole);
    ui->actQuit->setMenuRole(QAction::QuitRole);

    // Actions
    connect(ui->actAbout, SIGNAL(triggered()), about, SLOT(show()));

    connect(ui->actQuit, SIGNAL(triggered()), SLOT(close()));

    connect(ui->actSettingSchedule, SIGNAL(triggered()), settingSchedule,
            SLOT(show()));

    connect(ui->actSettingSemester, SIGNAL(triggered()), settingSemester,
            SLOT(show()));

    connect(ui->actLoad, SIGNAL(triggered()), SLOT(importDatabase()));

    connect(ui->actSave, SIGNAL(triggered()), SLOT(exportDatabase()));

    connect(ui->actICalSchedule, SIGNAL(triggered()), SLOT(exportICalSchedule()));

    connect(ui->actICalTasks, SIGNAL(triggered()), SLOT(exportICalTasks()));

    connect(ui->actDbExport, SIGNAL(triggered()), SLOT(dbStartExport()));
    connect(ui->actDbImport, SIGNAL(triggered()), SLOT(dbStartImport()));

    connect(ui->actHelp, SIGNAL(triggered()), SLOT(openHelp()));

    connect(ui->actSupport, SIGNAL(triggered()), SLOT(openSupport()));

    connect(ui->actionTwitter, SIGNAL(triggered()), SLOT(openTwitter()));

    connect(ui->actionVk, SIGNAL(triggered()), SLOT(openVk()));

    connect(ui->actQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    // Update schedule
    connect(settingSchedule, SIGNAL(scheduleChanged()), SLOT(updateSheduleUi()));
    connect(&STDataProvider::getInstance(), SIGNAL(bellsChanged()),
            SLOT(updateSheduleUi()));
    connect(&STDataProvider::getInstance(), SIGNAL(subjectsChanged()),
            SLOT(updateTasksUi()));

    // From WSchedule
    connect(ui->wSchedule, SIGNAL(sigScheduleSetting()),
            SLOT(showSettingSchedule()));
    connect(ui->wSchedule, SIGNAL(sigIncWeek()), SLOT(slotIncWeek()));
    connect(ui->wSchedule, SIGNAL(sigDecWeek()), SLOT(slotDecWeek()));
    connect(ui->wSchedule, SIGNAL(sigHome()), SLOT(slotHome()));

    // From WCalendar
    connect(ui->wCalendar, SIGNAL(selectedDateChanged(QDate)),
            SLOT(changeSelectedDate(QDate)));
    connect(ui->wCalendar, SIGNAL(sigIncMonth()), SLOT(slotIncMonth()));
    connect(ui->wCalendar, SIGNAL(sigDecMonth()), SLOT(slotDecMonth()));

    // From WTasks
    connect(ui->wTasks, SIGNAL(sigNeedDialog(int)),
            SLOT(switchStateToDialog(int)));

    // From TaskEdit
    connect(ui->wTaskEdit, SIGNAL(sigNeedSchedule()),
            SLOT(switchStateToSchedule()));
    connect(ui->wTaskEdit, SIGNAL(sigNeedRefresh()), SLOT(updateTasks()));

    // From WFilter
    connect(ui->wFilter, SIGNAL(filtersChanged()), SLOT(updateFilters()));

    // From shedule settings
    connect(settingSchedule, SIGNAL(needHelp()), SLOT(openHelp()));
    show();

    if (STDataProvider::getInstance().isEmpty()) {
        // Add some bells
        STDataProvider::getInstance().addBell(QTime(8, 15));
        STDataProvider::getInstance().addBell(QTime(9, 55));
        STDataProvider::getInstance().addBell(QTime(11, 35));
        STDataProvider::getInstance().addBell(QTime(13, 35));
        // And pair types
        STPair::Type addType;
        addType.id = INVALID_ID;
        addType.shortName = tr("l.");
        addType.fullName = tr("Lection");
        STDataProvider::getInstance().addPairType(addType);
        addType.shortName = tr("l.w.");
        addType.fullName = tr("Lab. work");
        STDataProvider::getInstance().addPairType(addType);
        addType.shortName = tr("pr.");
        addType.fullName = tr("Practise");
        STDataProvider::getInstance().addPairType(addType);
        addType.shortName = tr("sem.");
        addType.fullName = tr("Seminar");
        STDataProvider::getInstance().addPairType(addType);

        settingSchedule->enableFirstLaunchMode();
        // we must remove scrollbar space
#ifndef Q_OS_MAC
        settingSchedule->setGeometry(
                    (QApplication::desktop()->width() / 2) - (settingSchedule->width() / 2),
                    100, settingSchedule->width() - 12, settingSchedule->height());
#endif
        settingSemester->show();
    }

    // Updater
    updater = new STUpdaterClient;
    updaterWidget = new WPageShow(this);

    connect(updater, SIGNAL(needUpdate()), updaterWidget, SLOT(show()));

    updater->checkUpdate();

    // we must remove scrollbar space on OS X because of native scrollbars
#ifdef Q_OS_MAC
    settingSchedule->setGeometry(this->x() + 25, this->y() + 40,
                                 settingSchedule->width() - 12,
                                 settingSchedule->height());
#endif

    currentState = W_STATE_SCHEDULE;
}

WStodo::~WStodo() {
    delete settingSemester;
    delete settingSchedule;
    delete ui;
    delete updater;
    delete updaterWidget;
}

void WStodo::enableSmallScreenSupport() {
    QRect buffRect = this->geometry();
    this->setGeometry(buffRect.x(), buffRect.y(), 1024, 575);
    ui->wSchedule->enableSmallScreenSupport();
    settingSchedule->enableSmallScreenSupport();
    settingSchedule->setGeometry(buffRect.x() + 24, buffRect.y() + 24, 1000, 551);
    settingSchedule->setMinimumSize(1000, 550);
}

void WStodo::showEvent(QShowEvent *inSE) { inSE->accept(); }

void WStodo::closeEvent(QCloseEvent *inCE) {
    settingSchedule->close();
    inCE->accept();
}

void WStodo::showSettingSchedule() {
    settingSchedule->show();
    settingSchedule->activateWindow();
    settingSchedule->raise();
}

void WStodo::changeSelectedDate(QDate inDate) {
    selectedDate = inDate;

    ui->wCalendar->fillCalendarOnDate(selectedDate);
    ui->wSchedule->fillScheduleOnDate(selectedDate);
    ui->wTasks->fillTasksOnDate(selectedDate);
    ui->wTaskEdit->setDate(selectedDate);
}

void WStodo::updateUiOnSelectedDate() { changeSelectedDate(selectedDate); }

void WStodo::updateTasksUi() {
    ui->wTasks->fillTasksOnDate(selectedDate);
    ui->wSchedule->fillScheduleOnDate(selectedDate);
    ui->wTaskEdit->setDate(selectedDate);
}

void WStodo::updateSheduleUi() {
    ui->wSchedule->fillScheduleOnDate(selectedDate);
}

void WStodo::updateTasks() { ui->wTasks->refreshAll(); }

void WStodo::updateFilters() {
    ui->wTasks->updateFilter(ui->wFilter->currentFilter);
}

void WStodo::slotIncWeek() { changeSelectedDate(selectedDate.addDays(7)); }

void WStodo::slotDecWeek() { changeSelectedDate(selectedDate.addDays(-7)); }

void WStodo::slotHome() { changeSelectedDate(QDate::currentDate()); }

void WStodo::slotIncMonth() { changeSelectedDate(selectedDate.addMonths(1)); }

void WStodo::slotDecMonth() { changeSelectedDate(selectedDate.addMonths(-1)); }

void WStodo::switchStateToDialog(int inId) {
    ui->wTaskEdit->fillFields(inId);

    if (currentState != W_STATE_TASK_DIALOG) {
        ui->wSchedule->slideToHide();
        currentState = W_STATE_TASK_DIALOG;
        QTimer::singleShot(W_SLIDE_ANIMATION_TIME, this, SLOT(finishSwitch()));
    }
}

void WStodo::switchStateToSchedule() {
    ui->wTaskEdit->slideToHide();
    currentState = W_STATE_SCHEDULE;
    QTimer::singleShot(W_SLIDE_ANIMATION_TIME, this, SLOT(finishSwitch()));

    ui->wTasks->refreshAll();
}

void WStodo::finishSwitch() {
    if (currentState == W_STATE_TASK_DIALOG) {
        ui->stackedWidget->setCurrentIndex(W_STATE_TASK_DIALOG);
        ui->wTaskEdit->slideToShow();
    } else {
        ui->stackedWidget->setCurrentIndex(W_STATE_SCHEDULE);
        ui->wSchedule->slideToShow();
    }
}

void WStodo::importDatabase() {
    QString fileName = QFileDialog::getOpenFileName(
                this, tr("Load backup"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                "iStodo format (*.std)");

    STDataProvider::getInstance().importDatabase(fileName);

    updateUiOnSelectedDate();
    updateFilters();
}

void WStodo::exportDatabase() {
    QString fileName = QFileDialog::getSaveFileName(
                this, tr("Save backup"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                "iStodo format (*.std)");

    if (fileName.isEmpty() || fileName.isNull()) {
        return;
    }

    if (!QFile::copy(STDataProvider::getInstance().getDatabaseName(), fileName)) {
        QMessageBox::warning(
                    this, tr("Error"),
                    tr("Can't copy data file"));
    }
}

void WStodo::exportICalSchedule() {
    QString fileName = QFileDialog::getSaveFileName(
                this, tr("Export schedule to iCal"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                "iCal file format (*.ics)");

    if (fileName.isEmpty() || fileName.isNull()) {
        return;
    }

    STICalExporter exporter(&STDataProvider::getInstance());
    exporter.exportSchedule(fileName);
}

void WStodo::exportICalTasks() {
    QString fileName = QFileDialog::getSaveFileName(
                this, tr("Export tasks to iCal"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                "iCal file format (*.ics)");

    if (fileName.isEmpty() || fileName.isNull()) {
        return;
    }

    STICalExporter exporter(&STDataProvider::getInstance());
    exporter.exportTasks(fileName);
}

void WStodo::dbStartExport()
{
#if ST_OPENSOURCE
    QMessageBox::warning(
                this, tr("Error"),
                tr("The synchronization is only available in the version from official website for technical reasons"));
    return;
#endif
    STDropboxExporter *exporter = new STDropboxExporter(this);

    connect(exporter, SIGNAL(onExportFinished()), SLOT(dbFinishExport()));
    connect(exporter, SIGNAL(onError()), SLOT(dbOnError()));

    exporter->startExport();
}

void WStodo::dbFinishExport()
{
    QMessageBox::information(NULL,
                             "Dropbox",
                             tr("Export completed successfully"));
    delete sender();
}

void WStodo::dbStartImport()
{
#if ST_OPENSOURCE
    QMessageBox::warning(
                this, tr("Error"),
                tr("The synchronization is only available in the version from official website for technical reasons"));
    return;
#endif
    STDropboxExporter *exporter = new STDropboxExporter(this);

    connect(exporter, SIGNAL(onImportFinished()), SLOT(dbFinishImport()));
    connect(exporter, SIGNAL(onError()), SLOT(dbOnError()));

    exporter->startImport();
}

void WStodo::dbFinishImport()
{
    QMessageBox::information(NULL,
                             "Dropbox",
                             tr("Import completed successfully"));
    delete sender();

    updateUiOnSelectedDate();
    updateFilters();
}

void WStodo::dbOnError()
{
    QMessageBox::warning(NULL,
                         "Dropbox",
                         "Error, please try again later");
}

void WStodo::openTwitter() {
    QDesktopServices::openUrl(QUrl("https://twitter.com/iStodo"));
}

void WStodo::openVk() {
    QDesktopServices::openUrl(QUrl("http://vk.com/istodo"));
}

void WStodo::openHelp() {
    QNetworkConfigurationManager netConMan;
    if (!netConMan.isOnline()) {
        about->show();
    } else {
        QDesktopServices::openUrl(QUrl("http://istodo.ru/knowledgebase"));
    }
}

void WStodo::openSupport() {
    QDesktopServices::openUrl(QUrl("http://istodo.ru/support"));
}
