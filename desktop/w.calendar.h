/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_CALENDAR_H
#define W_CALENDAR_H

#include "include.h"

#define W_CALENDAR_DATE "StodoDate"  // It's need for store user data in label
#define W_CALENDAR_DURATION 300

#define BORDER_WIDTH 2

#define MONTH_TITLE 35
#define WEEK_DAYS_HEIGHT 22
#define CALENDAR_CELL_SIZE 31

#define WEEK_SELECTOR_HEIGHT 32
#define WEEK_SELECTOR_WIDTH 223

namespace Ui {
    class WCalendar;
}

class WCalendar : public QWidget {
        Q_OBJECT

    signals:
        void sigIncMonth();
        void sigDecMonth();
        void selectedDateChanged(QDate inDate);

    public:
        explicit WCalendar(QWidget *parent = 0);
        ~WCalendar();

    public slots:
        void fillCalendarOnDate(QDate inDate);

    protected:
        void mousePressEvent(QMouseEvent *ev);

    private:
        static const int HEADER_ROW;
        static const int FIRST_ROW;
        static const int COUNT_OF_ROW;
        static const QString CSS_HEADER;
        static const QString CSS_OTHER_MONTH;
        static const QString CSS_CURRENT_MONTH;
        static const QString CSS_SELECTED_DAY;
        static const QString CSS_TODAY;

        Ui::WCalendar *ui;
        QDate selectedDate;
        bool isInitialized;

        void setupUiForm();

    private slots:
        void slotIncMonth();
        void slotDecMonth();
};

#endif  // W_CALENDAR_H
