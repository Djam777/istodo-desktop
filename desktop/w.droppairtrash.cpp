#include "w.droppairtrash.h"

WDropPairTrash::WDropPairTrash(QWidget *parent) : QLabel(parent) {
    setAcceptDrops(true);
}

void WDropPairTrash::dragEnterEvent(QDragEnterEvent *inDEE) {
    if (inDEE->mimeData()->hasFormat("STPair") &&
            inDEE->dropAction() == Qt::MoveAction) {
        inDEE->accept();
    } else {
        inDEE->ignore();
    }
}

void WDropPairTrash::dropEvent(QDropEvent *inDE) {
    if (inDE->mimeData()->hasFormat("STPair") &&
            inDE->dropAction() == Qt::MoveAction) {
        const QMimeData *mimeData = inDE->mimeData();
        QByteArray *ba = new QByteArray(mimeData->data("STPair"));
        QDataStream ds(ba, QIODevice::ReadOnly);
        STPair *pair;
        ds.readRawData((char *)&pair, sizeof(pair));
        delete ba;

        STDataProvider::getInstance().delPair(pair->id);
        emit sigPairRemoved();

        inDE->accept();
    } else {
        inDE->ignore();
    }
}
