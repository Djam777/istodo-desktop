#include "w.scheduleonweek.h"
#include "ui_w.scheduleonweek.h"

WScheduleOnWeek::WScheduleOnWeek(QWidget *parent)
    : QFrame(parent), ui(new Ui::WScheduleOnWeek) {
    ui->setupUi(this);

    connect(ui->pbArrowLeftDark, SIGNAL(clicked()), SLOT(slotDecWeek()));
    connect(ui->pbArrowRightDark, SIGNAL(clicked()), SLOT(slotIncWeek()));
    connect(ui->pbTodayLeftDark, SIGNAL(clicked()), SLOT(slotHome()));
    connect(ui->pbSettingsRightDark, SIGNAL(clicked()),
            SLOT(slotScheduleSetting()));
    connect(ui->pbEditRaspTitle, SIGNAL(clicked()), SLOT(slotScheduleSetting()));

    // shortcuts block
    QShortcut *todayShortcut =
            new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_T), this);
#ifdef Q_OS_MAC
    ui->pbTodayLeftDark->setToolTip(tr("Back to today (⌘+T)"));
#else
    ui->pbTodayLeftDark->setToolTip(tr("Back to today (Ctrl+T)"));
#endif

    connect(todayShortcut, SIGNAL(activated()), ui->pbTodayLeftDark,
            SLOT(click()));

    connect(&endResizeTimer, SIGNAL(timeout()), SLOT(computeNewWidth()));

    ui->twScheduleOnWeek->setColumnWidth(0, 30);   // day
    ui->twScheduleOnWeek->setColumnWidth(1, 50);   // time
    ui->twScheduleOnWeek->setColumnWidth(2, 100);  // subj
    ui->twScheduleOnWeek->setColumnWidth(3, 41);   // type
    ui->twScheduleOnWeek->setColumnWidth(4, 90);   // teacher
    ui->twScheduleOnWeek->setColumnWidth(5, 60);   // audit
    ui->twScheduleOnWeek->setColumnWidth(6, 41);   // addition
    ui->twScheduleOnWeek->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    // Blink by button
    blinkCount = 0;
    if (STDataProvider::getInstance().isEmpty()) {  // we need to detect first
        // launch
        blinkCount = W_BLINK_ANIMATION_COUNT;
    }
    blink();
}

WScheduleOnWeek::~WScheduleOnWeek() { delete ui; }

void WScheduleOnWeek::resizeEvent(QResizeEvent *inRE) {
    if (endResizeTimer.isActive()) {
        endResizeTimer.stop();
    }

    endResizeTimer.start(100);
}

void WScheduleOnWeek::enableSmallScreenSupport() {
    ui->twScheduleOnWeek->setMinimumWidth(330);
    ui->twScheduleOnWeek->setColumnWidth(0, 30);  // day
    ui->twScheduleOnWeek->setColumnWidth(1, 40);  // time
    ui->twScheduleOnWeek->setColumnWidth(2, 80);  // subj
    ui->twScheduleOnWeek->setColumnWidth(3, 31);  // type
    ui->twScheduleOnWeek->setColumnWidth(4, 80);  // teacher
    ui->twScheduleOnWeek->setColumnWidth(5, 50);  // audit
    ui->twScheduleOnWeek->setColumnWidth(6, 31);  // addition
    ui->twScheduleOnWeek->horizontalHeader()->setStretchLastSection(true);
    ui->twScheduleOnWeek->setStyleSheet("font-size: 10px;");
}

void WScheduleOnWeek::fillScheduleOnDate(QDate inDate) {
    ui->twScheduleOnWeek->setRowCount(0);
    ui->pbArrowLeftDark->setEnabled(false);
    ui->pbArrowRightDark->setEnabled(false);

    if (STDataProvider::getInstance().isEmpty()) {
        return;
    }

    // Weeks count in cycle
    int cycleWeekCount = STDataProvider::getInstance().getCountOfWeek();
    if (cycleWeekCount <= 0) {
        return;
    }

    // Weeks count in semester
    int semWeekCount = STDataProvider::getInstance().getBegin().daysTo(
                STDataProvider::getInstance().getEnd()) /
            7;

    // Week number in semester
    int semWeekNumber = STDataProvider::getInstance().getWeekFromDate(inDate);
    if (semWeekNumber <= 0 || semWeekNumber > semWeekCount) {
        return;
    }

    // Week number in cycle
    int cycleWeekNumber = semWeekNumber % cycleWeekCount;
    if (cycleWeekNumber == 0) {
        cycleWeekNumber = cycleWeekCount;
    }

    ui->pbArrowLeftDark->setEnabled(semWeekNumber > 1);
    ui->pbArrowRightDark->setEnabled(semWeekNumber < semWeekCount);

    // Fill table with data
    QVector<QTime> *bells = STDataProvider::getInstance().getBells();
    QVector<QVector<STPair *> *> *weekShedule =
            STDataProvider::getInstance().getSheduleOnWeek(cycleWeekNumber);
    QVector<STPair *> *daySchedule;
    for (int i = 1; i <= 7; i++) {
        daySchedule = (*weekShedule)[i - 1];
        if (!daySchedule) {
            continue;
        }

        STPair *pair;
        int index = ui->twScheduleOnWeek->rowCount();
        int dayStartIndex = index;
        QTableWidgetItem *item;
        QBrush brush;

        // Selected date background
        if (inDate ==
                STDataProvider::getInstance().getDateFromWeek(semWeekNumber, i)) {
            brush = QBrush(QImage(":/img/other/images/others/selectedWeek.png"));
        } else {
            brush = QBrush();
        }

        for (int belNum = 0; belNum < bells->size(); belNum++) {
            int bellStartIndex = index;

            for (int j = 0; j < daySchedule->size(); j++) {
                pair = (*daySchedule)[j];
                if (pair->gridNumber != belNum + 1) {
                    continue;
                }

                ui->twScheduleOnWeek->insertRow(index);

                // Subject
                item = new QTableWidgetItem();
                item->setBackground(brush);
                if (pair->subjectId != INVALID_ID) {
                    item->setText(
                                STDataProvider::getInstance().getSubject(pair->subjectId)->nick);
                }
                ui->twScheduleOnWeek->setItem(index, 2, item);

                // PairType
                item = new QTableWidgetItem();
                item->setBackground(brush);
                if (pair->typeId != INVALID_ID) {
                    item->setText(STDataProvider::getInstance()
                                  .getPairType(pair->typeId)
                                  ->shortName);
                }
                ui->twScheduleOnWeek->setItem(index, 3, item);

                // Teacher
                item = new QTableWidgetItem();
                item->setBackground(brush);
                if (pair->teacherId != INVALID_ID) {
                    item->setText(
                                STDataProvider::getInstance().getTeacher(pair->teacherId)->nick);
                }
                ui->twScheduleOnWeek->setItem(index, 4, item);

                // Auditory
                item = new QTableWidgetItem();
                item->setBackground(brush);
                item->setText(pair->auditory);
                ui->twScheduleOnWeek->setItem(index, 5, item);

                // Additions
                item = new QTableWidgetItem();
                item->setBackground(brush);
                item->setText(pair->additions);
                ui->twScheduleOnWeek->setItem(index, 6, item);
                index++;
            }
            if (index - bellStartIndex > 0) {
                item = new QTableWidgetItem();
                item->setBackground(brush);
                QString text;
                text = text.sprintf("%02d:%02d", (*bells)[belNum].hour(),
                                    (*bells)[belNum].minute());
                item->setText(text);
                ui->twScheduleOnWeek->setItem(bellStartIndex, 1, item);
                if (index - bellStartIndex > 1) {
                    ui->twScheduleOnWeek->setSpan(bellStartIndex, 1,
                                                  index - bellStartIndex, 1);
                }
            }
        }

        if (index - dayStartIndex > 0) {
            item = new QTableWidgetItem();
            item->setBackground(brush);
            item->setText(STDataProvider::getInstance().getDaysOfWeek()[i]);
            ui->twScheduleOnWeek->setItem(dayStartIndex, 0, item);
            if (index - dayStartIndex > 1) {
                ui->twScheduleOnWeek->setSpan(dayStartIndex, 0, index - dayStartIndex,
                                              1);
            }
        }

        clearQVector<STPair>(*daySchedule);
    }
    clearQVector<QVector<STPair *> >(*weekShedule);
    bells->clear();

    for (int k = 0; k < ui->twScheduleOnWeek->rowCount(); k++) {
        // Kludge incoming! Because Qt incorrectly cut tw background by Horizontal
        // header
        if (k) {
            ui->twScheduleOnWeek->setRowHeight(k, 20);
        } else {
            ui->twScheduleOnWeek->setRowHeight(k, 24);
        }
    }
}

void WScheduleOnWeek::slideToHide() {
    QPropertyAnimation *moveOutShedule;
    moveOutShedule = new QPropertyAnimation(ui->groupFrame, "geometry");
    moveOutShedule->setDuration(W_SLIDE_ANIMATION_TIME);
    moveOutShedule->setStartValue(ui->groupFrame->geometry());
    moveOutShedule->setEndValue(
                QRect(ui->groupFrame->x() + W_SLIDE_ANIMATION_PATH, ui->groupFrame->y(),
                      ui->groupFrame->width(), ui->groupFrame->height()));
    moveOutShedule->start();
}

void WScheduleOnWeek::slideToShow() {
    QPropertyAnimation *moveInShedule;
    moveInShedule = new QPropertyAnimation(ui->groupFrame, "geometry");
    moveInShedule->setDuration(W_SLIDE_ANIMATION_TIME);
    moveInShedule->setStartValue(
                QRect(W_SLIDE_ANIMATION_PATH, ui->groupFrame->y(),
                      ui->groupFrame->width(), ui->groupFrame->height()));
    moveInShedule->setEndValue(
                QRect(0, 0, ui->groupFrame->width(), ui->groupFrame->height()));
    moveInShedule->start();
}

void WScheduleOnWeek::slotIncWeek() { emit sigIncWeek(); }

void WScheduleOnWeek::slotDecWeek() { emit sigDecWeek(); }

void WScheduleOnWeek::slotHome() {
    blinkCount = 1;
    emit sigHome();
}

void WScheduleOnWeek::slotScheduleSetting() {
    blinkCount = 1;
    emit sigScheduleSetting();
}

void WScheduleOnWeek::blink() {
    if (!blinkCount) {
        return;
    }
    blinkCount--;

    QGraphicsOpacityEffect *effect =
            new QGraphicsOpacityEffect(ui->pbEditRaspTitle);
    ui->pbEditRaspTitle->setGraphicsEffect(effect);
    QPropertyAnimation *anim = new QPropertyAnimation(effect, "opacity");
    if (blinkCount % 2) {
        anim->setStartValue(1.0);
        anim->setEndValue(0.3);
    } else {
        anim->setStartValue(0.3);
        anim->setEndValue(1.0);
    }
    anim->setDuration(W_BLINK_ANIMATION_TIME);
    anim->start();

    connect(anim, SIGNAL(finished()), SLOT(blink()));
}

void WScheduleOnWeek::computeNewWidth() {
    int diff = ui->twScheduleOnWeek->geometry().width() - 412;

    if (diff < 0) {
        diff = 0;
    }
    diff = diff / 20;
    if (diff > 8) {
        diff = 8;
    }

    ui->twScheduleOnWeek->setColumnWidth(0, 30);               // day
    ui->twScheduleOnWeek->setColumnWidth(1, 50);               // time
    ui->twScheduleOnWeek->setColumnWidth(2, 100 + 10 * diff);  // subj
    ui->twScheduleOnWeek->setColumnWidth(3, 41);               // type
    ui->twScheduleOnWeek->setColumnWidth(4, 90 + 10 * diff);   // teacher
    ui->twScheduleOnWeek->setColumnWidth(5, 60);               // audit
    if (diff > 7) {
        ui->twScheduleOnWeek->setColumnWidth(5, 70);  // audit
    }
}
