#include "d.editpairtype.h"
#include "ui_d.editpairtype.h"

DEditPairType::DEditPairType(QWidget *parent)
    : QDialog(parent), ui(new Ui::DEditPairType) {
    ui->setupUi(this);

    // Select only one item
    ui->twPairTypes->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->twPairTypes->setSelectionMode(QAbstractItemView::SingleSelection);

    ui->twPairTypes->installEventFilter(this);

    ui->twPairTypes->setObjectName("twDialog");
    ui->twPairTypes->verticalHeader()->hide();

    ui->twPairTypes->setColumnWidth(0, 41);
    ui->twPairTypes->setColumnWidth(1, 150);
    ui->twPairTypes->setColumnWidth(2, 280);

    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint);

    ui->twPairTypes->hideColumn(0);
    ui->pbDelRightLight->setChecked(false);

    connect(ui->pbReady, SIGNAL(clicked()), SLOT(close()));
    connect(ui->pbAddLeftLight, SIGNAL(clicked()), SLOT(addPairType()));
    connect(ui->pbDelRightLight, SIGNAL(toggled(bool)), SLOT(showDelete(bool)));
}

DEditPairType::~DEditPairType() {
    clearTable();

    delete ui;
}

void DEditPairType::showEvent(QShowEvent *inSE) {
    fillTable();

    ui->twPairTypes->resizeRowsToContents();

    inSE->accept();
}

void DEditPairType::closeEvent(QCloseEvent *inCE) {
    disableTableEditMode();

    QSettings settings;
    if (isHaveFakePairTypes() && settings.value(SET_DELETE_ITEM).toBool()) {
        int rc = QMessageBox::warning(
                    this, tr("Warning"),
                    tr("All classes types with empty short name will be removed, continue?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::YesToAll);
        if (rc == QMessageBox::No) {
            inCE->ignore();
            return;
        }
        if (rc == QMessageBox::YesToAll) {
            settings.setValue(SET_DELETE_ITEM, false);
        }
    }

    inCE->accept();
}

bool DEditPairType::eventFilter(QObject *inObject, QEvent *inEvent) {
    if (inEvent->type() != QEvent::KeyPress) {
        return false;
    }
    QKeyEvent *keyEvent = NULL;
    keyEvent = static_cast<QKeyEvent *>(inEvent);

    QList<QTableWidgetItem *> selectedItems = ui->twPairTypes->selectedItems();
    if (selectedItems.size() != 1) {
        return false;
    }
    QTableWidgetItem *item = selectedItems.first();

    int column = item->column();
    int row = item->row();

    if (keyEvent->key() == Qt::Key_Down) {
        row++;
    }
    if (keyEvent->key() == Qt::Key_Up) {
        row--;
    }

    switch (keyEvent->key()) {
        case Qt::Key_Return:
        case Qt::Key_Enter:
        case Qt::Key_Up:
        case Qt::Key_Down: {
            disableTableEditMode();

            ui->twPairTypes->setCurrentCell(row, column);
            ui->twPairTypes->editItem(ui->twPairTypes->item(row, column));
            keyEvent->accept();
            return true;
        }
        default: { break; }
    }
    return false;
}

bool DEditPairType::isHaveFakePairTypes() {
    STPair::Type *pairType = NULL;
    QVariant var;
    for (int i = 0; i < ui->twPairTypes->rowCount(); i++) {
        var = ui->twPairTypes->item(i, 0)->data(Qt::UserRole);
        pairType = var.value<STPair::Type *>();
        if (pairType->id == INVALID_ID || pairType->shortName == "") {
            return true;
        }
    }

    return false;
}

void DEditPairType::clearTable() {
    ui->twPairTypes->disconnect();

    QVariant var;
    STPair::Type *del = NULL;
    while (ui->twPairTypes->rowCount() > 0) {
        delete ui->twPairTypes->cellWidget(0, 0);

        var = ui->twPairTypes->item(0, 0)->data(Qt::UserRole);
        del = var.value<STPair::Type *>();
        delete del;

        ui->twPairTypes->removeRow(0);
    }
}

void DEditPairType::addPairTypeInTable(STPair::Type *inPairType) {
    disableTableEditMode();

    QPushButton *pb;
    QTableWidgetItem *item;
    QVariant var;

    int index = ui->twPairTypes->rowCount();
    ui->twPairTypes->insertRow(index);

    pb = new QPushButton;
    pb->setFixedSize(40, 24);
    pb->setAutoDefault(false);
    pb->setObjectName("pbDelMiddleDark");
    connect(pb, SIGNAL(clicked()), SLOT(delPairType()));
    ui->twPairTypes->setCellWidget(index, 0, pb);

    item = new QTableWidgetItem();
    var.setValue(inPairType);
    item->setData(Qt::UserRole, var);
    ui->twPairTypes->setItem(index, 0, item);

    item = new QTableWidgetItem();
    item->setText(inPairType->shortName);
    var.setValue(&inPairType->shortName);
    item->setData(Qt::UserRole, var);
    QBrush brsh(QColor(EMPTY_COLOR));
    item->setBackground(brsh);
    ui->twPairTypes->setItem(index, 1, item);
    if (inPairType->id == INVALID_ID) {
        // If it's new pair edit them
        ui->twPairTypes->editItem(item);
        ui->twPairTypes->setCurrentItem(item);
    }

    item = new QTableWidgetItem();
    item->setText(inPairType->fullName);
    var.setValue(&inPairType->fullName);
    item->setData(Qt::UserRole, var);
    ui->twPairTypes->setItem(index, 2, item);
}

void DEditPairType::fillTable() {
    clearTable();

    QVector<STPair::Type *> *pairTypes =
            STDataProvider::getInstance().getPairTypes();
    for (int i = 0; i < pairTypes->size(); i++) {
        addPairTypeInTable((*pairTypes)[i]);
    }
    delete pairTypes;

    connect(ui->twPairTypes, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editPairType(QTableWidgetItem *)));
}

void DEditPairType::disableTableEditMode() {
    // This code need for disable edit mode
    QTableWidgetItem *item = ui->twPairTypes->currentItem();

    ui->twPairTypes->setDisabled(true);
    ui->twPairTypes->setDisabled(false);

    if (item) {
        editPairType(item);
    }
}

void DEditPairType::addPairType() {
    ui->twPairTypes->disconnect();

    STPair::Type *add = new STPair::Type;
    add->id = INVALID_ID;

    addPairTypeInTable(add);

    connect(ui->twPairTypes, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editPairType(QTableWidgetItem *)));
}

void DEditPairType::editPairType(QTableWidgetItem *inItem) {
    if (inItem->column() == 0) {
        return;
    }

    QVariant var = inItem->data(Qt::UserRole);
    QString &str = *var.value<QString *>();

    var = ui->twPairTypes->item(inItem->row(), 0)->data(Qt::UserRole);
    STPair::Type *edit = var.value<STPair::Type *>();

    switch (inItem->column()) {
        // nick
        case 1: {
            if (inItem->text().length() > 4) {
                inItem->setText(inItem->text().mid(0, 4));
            }
            str = inItem->text();
            break;
        }
            // name
        case 2: {
            if (inItem->text() == "") {
                inItem->setText(str);
            } else {
                str = inItem->text();
            }
            break;
        }
            // other fields
        default: { str = inItem->text(); }
    }

    if (edit->shortName != "") {
        if (edit->id == INVALID_ID) {
            edit->id = STDataProvider::getInstance().addPairType(*edit);
        } else {
            STDataProvider::getInstance().modPairType(*edit);
        }
    }
}

void DEditPairType::delPairType() {
    ui->twPairTypes->disconnect();

    STPair::Type *del = NULL;
    for (int i = 0; i < ui->twPairTypes->rowCount(); i++) {
        if (sender() == ui->twPairTypes->cellWidget(i, 0)) {
            QVariant var;
            var = ui->twPairTypes->item(i, 0)->data(Qt::UserRole);
            del = var.value<STPair::Type *>();
            ui->twPairTypes->removeRow(i);
            break;
        }
    }
    if (del == NULL) {
        qDebug() << "Error on delete PairType";
        return;
    }

    STDataProvider::getInstance().delPairType(del->id);
    delete del;

    connect(ui->twPairTypes, SIGNAL(itemChanged(QTableWidgetItem *)),
            SLOT(editPairType(QTableWidgetItem *)));
}

void DEditPairType::showDelete(bool inShow) {
    if (inShow) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(
                    this, tr("Warning"),
                    tr("Type removing causes removing all classes with this type, continue?"),
                    QMessageBox::Yes | QMessageBox::No);
        if (reply != QMessageBox::Yes) {
            ui->pbDelRightLight->blockSignals(true);
            ui->pbDelRightLight->setChecked(false);
            ui->pbDelRightLight->blockSignals(false);
            return;
        }
        ui->twPairTypes->showColumn(0);
    } else {
        ui->twPairTypes->hideColumn(0);
    }
}
