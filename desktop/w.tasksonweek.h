/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_TASKSONWEEK_H
#define W_TASKSONWEEK_H

#include "include.h"
#include "w.tasksstub.h"

namespace Ui {
    class WTasksOnWeek;
}

class WTasksOnWeek : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedDialog(int outId);

    public:
        explicit WTasksOnWeek(QWidget *parent = 0);
        ~WTasksOnWeek();

    public slots:
        void fillTasksOnDate(QDate inDate);
        void refreshAll();
        void updateFilter(STFilterElement inFilter);

    private:
        Ui::WTasksOnWeek *ui;

        int currentState;
        QDate currentDate;
        WDaysTasks *days[7];
        WTasksStub stub;
        STFilterElement currentFilter;

    private slots:
        void addTask();
        void editTask(int inId);

        void showCompleted(bool inState);
        void showOverDue(bool inState);
        void showAll(bool inState);
};

#endif  // W_TASKSONWEEK_H
