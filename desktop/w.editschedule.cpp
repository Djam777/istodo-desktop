#include "w.editschedule.h"
#include "ui_w.editschedule.h"

WEditSchedule::WEditSchedule(QWidget *parent)
    : QWidget(parent), ui(new Ui::WEditSchedule) {
    ui->setupUi(this);

    //  Menu
    addWeek = new QAction(tr("Add week"), this);
    cloneWeek = new QAction(tr("Copy week"), this);
    delWeek = new QAction(tr("Remove week"), this);
    weekMenu = new QMenu(this);
    weekMenu->addAction(addWeek);
    weekMenu->addAction(cloneWeek);
    weekMenu->addAction(delWeek);
    ui->pbWeeksDark->setMenu(weekMenu);

    //  Bells
    bells = NULL;

    //  Week
    nextWeek = STDataProvider::getInstance().getCountOfWeek() + 1;
    if (nextWeek <= 1) {
        nextWeek = 2;
    }

    ui->twDropSchedule->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    ui->twDropSchedule->setColumnWidth(0, 70);
    ui->twDropSchedule->setColumnWidth(1, 70);

    connect(ui->pbAddLeftDark, SIGNAL(clicked()), SLOT(addBell()));
    connect(ui->pbDelMiddleDark, SIGNAL(clicked()), SLOT(delBell()));
    connect(ui->pbClockRightDark, SIGNAL(toggled(bool)), SLOT(setEditEnabled(bool)));
    connect(addWeek, SIGNAL(triggered()), SLOT(slotAddWeek()));
    connect(cloneWeek, SIGNAL(triggered()), SLOT(slotCloneWeek()));
    connect(delWeek, SIGNAL(triggered()), SLOT(slotDelWeek()));
    connect(ui->pbArrowLeftDark, SIGNAL(clicked()), SLOT(decCurrentWeek()));
    connect(ui->pbArrowRightDark, SIGNAL(clicked()), SLOT(incCurrentWeek()));

    connect(&STDataProvider::getInstance(), SIGNAL(pairsChanged()),
            SLOT(computeNewHeight()));

    connect(&STDataProvider::getInstance(), SIGNAL(updated()),
            SLOT(fullUpdateSchedule()));

    connect(&endResizeTimer, SIGNAL(timeout()), SLOT(computeNewWidth()));
}

WEditSchedule::~WEditSchedule() {
    clearSchedule();

    delete ui;

    delete addWeek;
    delete delWeek;
    delete weekMenu;
}

void WEditSchedule::enableFirstLaunchMode() {
    ui->pbClockRightDark->setChecked(true);
}

void WEditSchedule::updateSchedule() { fillScheduleOnWeek(curWeek); }

void WEditSchedule::fullUpdateSchedule() {
    //  Week
    nextWeek = STDataProvider::getInstance().getCountOfWeek() + 1;
    if (nextWeek <= 1) {
        nextWeek = 2;
    }

    fillScheduleOnWeek(1);
}

void WEditSchedule::computeNewHeight() {
    emit scheduleChanged();

    STPair *pair;
    QVector<STPair *> *daySchedule;
    QVector<QVector<STPair *> *> *weekShedule =
            STDataProvider::getInstance().getSheduleOnWeek(curWeek);

    if (!bells) {
        bells = STDataProvider::getInstance().getBells();
    }
    int maxBellSize[bells->size()];
    memset(maxBellSize, 0, sizeof(maxBellSize));

    // for each day
    for (int i = 0; i < 7; i++) {
        daySchedule = (*weekShedule)[i];
        if (!daySchedule) {
            continue;
        }

        int currBellSize[bells->size()];
        memset(currBellSize, 0, sizeof(currBellSize));

        // for each pair in day
        for (int j = 0; j < daySchedule->size(); j++) {
            pair = (*daySchedule)[j];
            currBellSize[pair->gridNumber - 1]++;
        }

        // find max values per row
        for (int k = 0; k < bells->size(); k++) {
            if (maxBellSize[k] < currBellSize[k]) {
                maxBellSize[k] = currBellSize[k];
            }
        }
    }

    for (int k = 0; k < bells->size(); k++) {
        // Cratches incoming! Because Qt incorrectly cut tw background by Horizontal
        // header
        if (k) {
            if (maxBellSize[k]) {
                ui->twDropSchedule->setRowHeight(k, 90 * maxBellSize[k]);
            } else {
                ui->twDropSchedule->setRowHeight(k, 30);
            }
        } else {
            if (maxBellSize[k]) {
                ui->twDropSchedule->setRowHeight(k, 95 * maxBellSize[k]);
            } else {
                ui->twDropSchedule->setRowHeight(k, 35);
            }
        }
    }
}

void WEditSchedule::computeNewWidth() {
    int diff = ui->twDropSchedule->geometry().width() - 670;
    if (diff < 0) {
        diff = 0;
    }
    diff = diff / 60;

    ui->twDropSchedule->setColumnWidth(0, 70);
    ui->twDropSchedule->setColumnWidth(1, 70);
    for (int i = FIRST_PAIR_COLUMN; i < LAST_PAIR_COLUMN; i++) {
        ui->twDropSchedule->setColumnWidth(i, 100 + 10 * diff);
    }
}

void WEditSchedule::showEvent(QShowEvent *inSE) {
    fillScheduleOnWeek(1);
    inSE->accept();
}

void WEditSchedule::resizeEvent(QResizeEvent *inRE) {
    if (endResizeTimer.isActive()) {
        endResizeTimer.stop();
    }

    endResizeTimer.start(200);
}

bool WEditSchedule::setCurrentWeek(int inWeek) {
    if (inWeek <= 0 || inWeek >= nextWeek) {
        return false;
    }

    curWeek = inWeek;

    ui->pbArrowLeftDark->setEnabled((curWeek != 1));
    ui->pbArrowRightDark->setEnabled((curWeek != nextWeek - 1));
    ui->pbWeeksDark->setText(tr("  Week ") + QString::number(curWeek));
    delWeek->setVisible(nextWeek > 2);

    return true;
}

void WEditSchedule::fillScheduleOnWeek(int inWeek) {
    clearSchedule();

    if (!setCurrentWeek(inWeek)) {
        return;
    }

    bells = STDataProvider::getInstance().getBells();
    QTimeEdit *temp;
    QLabel *tempLabel;
    QString tempStr;
    for (int i = 0; i < bells->size(); i++) {
        ui->twDropSchedule->insertRow(ui->twDropSchedule->rowCount());

        temp = new QTimeEdit(QTime((*bells)[i].hour(), (*bells)[i].minute()), this);
        temp->setObjectName("teBellEdit");

        tempLabel = new QLabel(
                    tempStr.sprintf("%02d:%02d", (*bells)[i].hour(), (*bells)[i].minute()),
                    this);
        tempLabel->setObjectName("lbBellShow");
        tempLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        connect(temp, SIGNAL(timeChanged(QTime)), SLOT(correctBell(QTime)));
        connect(temp, SIGNAL(editingFinished()), SLOT(modBell()));

        ui->twDropSchedule->setCellWidget(i, TIME_EDIT_COLUMN, temp);
        ui->twDropSchedule->setCellWidget(i, TIME_SHOW_COLUMN, tempLabel);

        WDropPairContainer *dpc;
        // At this moment we set day and bell, it's base on column and row
        for (int j = FIRST_PAIR_COLUMN; j <= LAST_PAIR_COLUMN; j++) {
            dpc = new WDropPairContainer(curWeek, j - 1, i + 1, this);
            connect(dpc, SIGNAL(sigPairAdded()), SLOT(computeNewHeight()));
            connect(dpc, SIGNAL(sigPairAdded()), SLOT(sendScheduleChanged()));
            ui->twDropSchedule->setCellWidget(i, j, dpc);
        }
    }

    // Fill table with data
    QVector<QVector<STPair *> *> *weekShedule =
            STDataProvider::getInstance().getSheduleOnWeek(curWeek);
    QVector<STPair *> *daySchedule;
    STPair *pair;
    for (int i = FIRST_PAIR_COLUMN; i <= LAST_PAIR_COLUMN; i++) {
        daySchedule = (*weekShedule)[i - FIRST_PAIR_COLUMN];
        if (!daySchedule) {
            continue;
        }

        WDropPairContainer *dpc;
        for (int j = 0; j < daySchedule->size(); j++) {
            pair = (*daySchedule)[j];
            dpc = (WDropPairContainer *)ui->twDropSchedule->cellWidget(
                        pair->gridNumber - 1, pair->dayOfWeek + 1);
            dpc->addPair(pair);
        }

        clearQVector<STPair>(*daySchedule);
    }
    clearQVector<QVector<STPair *> >(*weekShedule);

    computeNewHeight();
    setEditEnabled(ui->pbClockRightDark->isChecked());
}

void WEditSchedule::clearSchedule() {
    if (bells != NULL) {
        delete bells;
        bells = NULL;
    }

    for (int i = 0; i < ui->twDropSchedule->rowCount(); i++) {
        for (int j = 0; j < ui->twDropSchedule->columnCount(); j++) {
            delete ui->twDropSchedule->cellWidget(i, j);
        }
    }
    ui->twDropSchedule->setRowCount(0);
}

int WEditSchedule::minutesBetween(QTime first, QTime second) {
    int fTime = first.hour() * 60 + first.minute();
    int sTime = second.hour() * 60 + second.minute();

    return sTime - fTime;
}

void WEditSchedule::addBell() {
    QTime add;

    if (bells->size() <= 0) {
        add = QTime(8, 0);
    } else {
        add = (*bells)[bells->size() - 1];
        add = add.addSecs(60 * (STDataProvider::getInstance().getPairLength() +
                                PAIR_BREAK_SIZE_MIN));
    }
    STDataProvider::getInstance().addBell(add);

    // TODO: move out to stand alone func

    if (bells != NULL) {
        delete bells;
        bells = NULL;
    }

    bells = STDataProvider::getInstance().getBells();

    QTimeEdit *temp;
    QLabel *tempLabel;
    QString tempStr;
    int i = ui->twDropSchedule->rowCount();

    ui->twDropSchedule->insertRow(i);
    ui->twDropSchedule->setRowHeight(i, 30);

    temp = new QTimeEdit(QTime(add.hour(), add.minute()), this);
    temp->setObjectName("teBellEdit");

    tempLabel =
            new QLabel(tempStr.sprintf("%02d:%02d", add.hour(), add.minute()), this);
    tempLabel->setObjectName("lbBellShow");
    tempLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    connect(temp, SIGNAL(timeChanged(QTime)), SLOT(correctBell(QTime)));
    connect(temp, SIGNAL(editingFinished()), SLOT(modBell()));

    ui->twDropSchedule->setCellWidget(i, TIME_EDIT_COLUMN, temp);
    ui->twDropSchedule->setCellWidget(i, TIME_SHOW_COLUMN, tempLabel);

    WDropPairContainer *dpc;
    // At this moment we set day and bell, it's base on column and row
    for (int j = FIRST_PAIR_COLUMN; j <= LAST_PAIR_COLUMN; j++) {
        dpc = new WDropPairContainer(curWeek, j - 1, i + 1, this);
        connect(dpc, SIGNAL(sigPairAdded()), SLOT(computeNewHeight()));
        connect(dpc, SIGNAL(sigPairAdded()), SLOT(sendScheduleChanged()));
        ui->twDropSchedule->setCellWidget(i, j, dpc);
    }
    // TODO: End of move out

    ui->pbDelMiddleDark->setEnabled(true);
    ui->twDropSchedule->scrollToBottom();
}

void WEditSchedule::delBell() {
    QSettings settings;
    if (settings.value(SET_DELETE_BELL).toBool()) {
        int rc = QMessageBox::warning(
                    this, tr("Warning"),
                    tr("Classes in bottom row will be removed from all weeks, continue?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::YesToAll);
        if (rc == QMessageBox::No) {
            return;
        }
        if (rc == QMessageBox::YesToAll) {
            settings.setValue(SET_DELETE_BELL, false);
        }
    }

    STDataProvider::getInstance().delBell();

    // Clear last row
    int i = ui->twDropSchedule->rowCount();
    for (int j = 0; j < ui->twDropSchedule->columnCount(); j++) {
        delete ui->twDropSchedule->cellWidget(i, j);
    }

    if (bells != NULL) {
        delete bells;
        bells = NULL;
    }

    ui->twDropSchedule->setRowCount(i);

    bells = STDataProvider::getInstance().getBells();

    if (!bells->count()) {
        ui->pbDelMiddleDark->setEnabled(false);
    }
    ui->twDropSchedule->scrollToBottom();
}

void WEditSchedule::modBell() {
    for (int i = 0; i < bells->size(); i++) {
        STDataProvider::getInstance().modBell(i, (*bells)[i]);
        QLabel *lb = (QLabel *)ui->twDropSchedule->cellWidget(i, TIME_SHOW_COLUMN);
        lb->setText((*bells)[i].toString("hh:mm"));
    }
}

void WEditSchedule::correctBell(const QTime &inTime) {
    int pairLength = STDataProvider::getInstance().getPairLength();

    QTimeEdit *edit = NULL;
    int index = -1;
    for (int i = 0; i < bells->size(); i++) {
        edit = (QTimeEdit *)ui->twDropSchedule->cellWidget(i, TIME_EDIT_COLUMN);
        if (edit && edit == sender()) {
            index = i;
            break;
        }
    }

    if (edit == NULL) {
        return;
    }

    /* // I guess it's bad idea

      QTime newTime = inTime;
      if( index != 0 && minutesBetween((*bells)[index-1], newTime) < pairLength
     ) {
          QTimeEdit *oldTimeEdit = (QTimeEdit*)ui->twDropSchedule->cellWidget(
     index-1, 0 );
          oldTimeEdit->setTime( newTime.addSecs( -pairLength*60 ) );
      }
      if( index != bells->size()-1 && minutesBetween( newTime, (*bells)[index+1]
     ) < pairLength ) {
          QTimeEdit *oldTimeEdit = (QTimeEdit*)ui->twDropSchedule->cellWidget(
     index+1, 0 );
          oldTimeEdit->setTime( newTime.addSecs( pairLength*60 ) );
      }*/

    (*bells)[index].setHMS(inTime.hour(), inTime.minute(), 0);
}

void WEditSchedule::setEditEnabled(bool inEnabled) {
    ui->twDropSchedule->setColumnHidden(1, inEnabled);
    ui->twDropSchedule->setColumnHidden(0, !inEnabled);

    ui->pbAddLeftDark->setEnabled(inEnabled);
    ui->pbDelMiddleDark->setEnabled(inEnabled);
}

void WEditSchedule::slotAddWeek() {
    nextWeek++;
    fillScheduleOnWeek(nextWeek - 1);

    emit scheduleChanged();
}

void WEditSchedule::slotCloneWeek() {
    nextWeek++;
    STDataProvider::getInstance().cloneWeek(curWeek);
    fillScheduleOnWeek(curWeek + 1);

    emit scheduleChanged();
}

void WEditSchedule::slotDelWeek() {
    if (nextWeek <= 2) {
        return;
    }

    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(
                this, tr("Warning"),
                tr("All classes of this week will be removed, continue?"),
                QMessageBox::Yes | QMessageBox::No);
    if (reply != QMessageBox::Yes) {
        return;
    }

    STDataProvider::getInstance().delScheduleOnWeek(curWeek);
    nextWeek--;
    if (curWeek != 1) {
        curWeek--;
    }
    fillScheduleOnWeek(curWeek);

    emit scheduleChanged();
}
// TODO: refactor to lambdas
void WEditSchedule::incCurrentWeek() { fillScheduleOnWeek(curWeek + 1); }

void WEditSchedule::decCurrentWeek() { fillScheduleOnWeek(curWeek - 1); }

void WEditSchedule::sendScheduleChanged() { emit scheduleChanged(); }
