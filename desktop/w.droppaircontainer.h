/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_DROPPAIRCONTAINER_H
#define W_DROPPAIRCONTAINER_H

#include "include.h"
#include "w.dragpairmove.h"

class WDropPairContainer : public QFrame {
        Q_OBJECT

    signals:
        void sigPairAdded();

    public:
        explicit WDropPairContainer(int inWeek, int inDay, int inGrid,
                                    QWidget *inParent = 0);
        void addPair(STPair *inPair);

    protected:
        void dragEnterEvent(QDragEnterEvent *inDEE);
        void dropEvent(QDropEvent *inDE);

    private:
        int week, day, grid;
};

#endif  // W_DROPPAIRCONTAINER_H
