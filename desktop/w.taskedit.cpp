#include "w.taskedit.h"
#include "ui_w.taskedit.h"

WTaskEdit::WTaskEdit(QWidget *parent) : QWidget(parent), ui(new Ui::WTaskEdit) {
    ui->setupUi(this);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 2, 0))
    ui->teTaskDescription->setPlaceholderText("Описание");
#endif
    currentId = INVALID_ID;

    // Setup priorities
    QVariant var;
    QString priors[] = {tr(" Low"), tr(" Middle"), tr(" High"), tr(" Alarm!")};
    for (int i = 0; i < 4; i++) {
        var.setValue(i);
        ui->cbTaskPriority->addItem(priors[i], var);
    }

    repeatPeriodChanged(1);

    // setup data controls
    ui->deTaskTime->setDate(QDate::currentDate());
    ui->deRepeatFrom->setDate(QDate::currentDate());
    ui->deRepeatTo->setDate(QDate::currentDate());

    connect(ui->cbTaskRepeat, SIGNAL(toggled(bool)), SLOT(toggleRepeat(bool)));

    connect(ui->sbRepeatEvery, SIGNAL(valueChanged(int)),
            SLOT(repeatPeriodChanged(int)));

    connect(ui->deRepeatTo, SIGNAL(userDateChanged(QDate)),
            SLOT(checkDates(QDate)));

    connect(ui->pbSwitchSingleDark, SIGNAL(clicked()), SLOT(closeEditor()));

    // buttons block
    connect(ui->pbReady, SIGNAL(clicked()), SLOT(addTodo()));

    connect(ui->pbDiaRemove, SIGNAL(clicked()), SLOT(removeTodo()));

    connect(ui->pbDiaMultiRemove, SIGNAL(clicked()), SLOT(removeSeries()));

    // shortcuts block
    QShortcut *doneShortcut =
            new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_D), this);
    QShortcut *saveShortcut =
            new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this);
#ifdef Q_OS_MAC
    ui->pbReady->setToolTip(tr("Save new task (⌘+S)"));
    ui->pbSwitchSingleDark->setToolTip(tr("Back to schedule (⌘+S)"));
    ui->lbHeaderTitle->setToolTip(tr(
                "⌘ + N – add task\n⌘ + D – mark as completed\n⌘ + S – "
                "save and show shcedule"));
#else
    ui->pbReady->setToolTip(tr("Save new task (Ctrl+S)"));
    ui->pbSwitchSingleDark->setToolTip(tr("Back to schedule (Ctrl+S)"));
    ui->lbHeaderTitle->setToolTip(tr(
                "Ctrl + N – add task\nCtrl + D – mark as completed\nCtrl + "
                "S – save and show shcedule"));
#endif

    connect(doneShortcut, SIGNAL(activated()), ui->cbTaskCheck, SLOT(click()));

    connect(saveShortcut, SIGNAL(activated()), ui->pbReady, SLOT(click()));

    // autosave block
    connect(ui->leTaskTitle, SIGNAL(editingFinished()), SLOT(somethingChanged()));

    connect(ui->cbTaskCheck, SIGNAL(clicked()), SLOT(somethingChanged()));

    connect(ui->cbTaskSubj, SIGNAL(currentIndexChanged(int)),
            SLOT(somethingChanged()));

    connect(ui->cbTaskPriority, SIGNAL(currentIndexChanged(int)),
            SLOT(somethingChanged()));
}

WTaskEdit::~WTaskEdit() { delete ui; }

void WTaskEdit::fillFields(int inId) {
    if (currentId != INVALID_ID) {
        saveAll();
        sigNeedRefresh();
    }
    clearFields();

    currentId = inId;

    if (inId == INVALID_ID) {
        ui->lbHeaderTitle->setText(tr("Add"));
        ui->lbRepeat->setHidden(false);
        ui->cbTaskRepeat->setHidden(false);
        ui->frmRepeat->setHidden(false);
        ui->pbReady->setHidden(false);
        return;
    }

    STTask *buffTask = STDataProvider::getInstance().getTask(inId);

    ui->lbHeaderTitle->setText(tr("Edit"));

    ui->leTaskTitle->setText(buffTask->task);
    ui->teTaskDescription->setText(buffTask->description);

    ui->cbTaskCheck->setChecked(buffTask->isComplete);
    ui->deTaskTime->setDate(buffTask->date);

    currentSeries = buffTask->seriesNumber;

    if (currentSeries != INVALID_ID) {
        ui->pbDiaMultiRemove->setHidden(false);
    }
    ui->pbDiaRemove->setHidden(false);

    QVariant var;
    var.setValue(buffTask->subjectId);
    ui->cbTaskSubj->setCurrentIndex(ui->cbTaskSubj->findData(var, Qt::UserRole));

    var.setValue(buffTask->prior);
    ui->cbTaskPriority->setCurrentIndex(
                ui->cbTaskPriority->findData(var, Qt::UserRole));
}

void WTaskEdit::setDate(QDate inDate) {
    if (currentId == INVALID_ID) {
        ui->deTaskTime->setDate(inDate);
        if (ui->cbTaskRepeat->isChecked()) {
            ui->deRepeatFrom->setDate(inDate);
        }
    }
}

void WTaskEdit::slideToHide() {
    QPropertyAnimation *moveOutEdit;
    moveOutEdit = new QPropertyAnimation(ui->frmBackground, "geometry");
    moveOutEdit->setDuration(W_SLIDE_ANIMATION_TIME);
    moveOutEdit->setStartValue(
                QRect(0, 0, ui->frmBackground->width(), ui->frmBackground->height()));
    moveOutEdit->setEndValue(QRect(0, W_SLIDE_ANIMATION_PATH,
                                   ui->frmBackground->width(),
                                   ui->frmBackground->height()));
    moveOutEdit->start();
}

void WTaskEdit::slideToShow() {
    QPropertyAnimation *moveInEdit;
    moveInEdit = new QPropertyAnimation(ui->frmBackground, "geometry");
    moveInEdit->setDuration(W_SLIDE_ANIMATION_TIME);

    moveInEdit->setStartValue(QRect(0, W_SLIDE_ANIMATION_PATH,
                                    ui->frmBackground->width(),
                                    ui->frmBackground->height()));
    moveInEdit->setEndValue(
                QRect(0, 0, ui->frmBackground->width(), ui->frmBackground->height()));
    moveInEdit->start();
}

void WTaskEdit::resizeEvent(QResizeEvent *inRE) {
    // This override for blink protection
    QRect buffRect = this->geometry();
    ui->frmBackground->setGeometry(buffRect);
}

void WTaskEdit::clearFields() {
    currentId = INVALID_ID;
    currentSeries = INVALID_ID;

    ui->leTaskTitle->clear();
    ui->cbTaskCheck->setChecked(false);

    ui->teTaskDescription->clear();

    // Subj block
    ui->cbTaskSubj->clear();

    QVariant var;

    // For tasks without subject
    var.setValue(INVALID_ID);
    ui->cbTaskSubj->addItem("", var);

    QVector<STSubject *> *subjects = STDataProvider::getInstance().getSubjects();
    for (int i = 0; i < subjects->size(); i++) {
        var.setValue((*subjects)[i]->id);
        ui->cbTaskSubj->addItem((*subjects)[i]->nick, var);
    }
    if (subjects != NULL) {
        clearQVector<STSubject>(*subjects);
        delete subjects;
        subjects = NULL;
    }

    ui->cbTaskSubj->setCurrentIndex(0);
    ui->cbTaskPriority->setCurrentIndex(1);
    ui->sbRepeatEvery->setValue(1);

    // Repeat block
    ui->frmRepeat->setEnabled(false);
    ui->frmRepeat->setHidden(true);
    ui->lbRepeat->setHidden(true);
    ui->cbTaskRepeat->setChecked(false);
    ui->cbTaskRepeat->setHidden(true);
    ui->cbTaskPeriod->setCurrentIndex(0);
    ui->cbAddNumber->setChecked(false);

    ui->pbDiaMultiRemove->setHidden(true);
    ui->pbDiaRemove->setHidden(true);
    ui->pbReady->setHidden(true);
}

void WTaskEdit::toggleRepeat(bool inRepeat) {
    ui->deTaskTime->setEnabled(!inRepeat);
    ui->frmRepeat->setEnabled(inRepeat);
    ui->deRepeatFrom->setDate(ui->deTaskTime->date());
    ui->deRepeatTo->setDate(STDataProvider::getInstance().getEnd());
}

// Just for russian locale
void WTaskEdit::repeatPeriodChanged(int inNum) {
    int val;
    QVariant var;
    QString repeatPeriods[4][3] = {{tr("Days","День"), tr("Days","Дня"), ("Days","Дней")},
                                   {tr("Week", "Неделю"), tr("Weeks", "Недели"), tr("Weeks", "Недель")},
                                   {tr("Month", "Месяц"), tr("Months", "Месяца"), tr("Months", "Месяцев")},
                                   {tr("Every", "Каждый"), tr("Every", "Каждые"), ""}};

    int selectedPeriod = ui->cbTaskPeriod->currentIndex();
    ui->lbRepeatEvery->setText(repeatPeriods[3][1]);
    ui->cbTaskPeriod->clear();

    switch (inNum) {
        case 11:
        case 12:
        case 13:
        case 14: {
            for (int i = 0; i < 3; i++) {
                switch (i) {
                    case 0:
                        val = REPEAT_DAY;
                    break;
                    case 1:
                        val = REPEAT_WEEK;
                    break;
                    case 2:
                        val = REPEAT_MONTH;
                    break;
                }
                var.setValue(val);

                ui->cbTaskPeriod->addItem(repeatPeriods[i][2], var);
            }
        }
            goto end;
    }

    if (inNum % 10 == 1) {
        ui->lbRepeatEvery->setText(repeatPeriods[3][0]);
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    val = REPEAT_DAY;
                break;
                case 1:
                    val = REPEAT_WEEK;
                break;
                case 2:
                    val = REPEAT_MONTH;
                break;
            }
            var.setValue(val);

            ui->cbTaskPeriod->addItem(repeatPeriods[i][0], var);
        }
        goto end;
    }

    if (inNum % 10 > 1 && inNum % 10 < 5) {
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    val = REPEAT_DAY;
                break;
                case 1:
                    val = REPEAT_WEEK;
                break;
                case 2:
                    val = REPEAT_MONTH;
                break;
            }
            var.setValue(val);

            ui->cbTaskPeriod->addItem(repeatPeriods[i][1], var);
        }
        goto end;
    } else {
        for (int i = 0; i < 3; i++) {
            switch (i) {
                case 0:
                    val = REPEAT_DAY;
                break;
                case 1:
                    val = REPEAT_WEEK;
                break;
                case 2:
                    val = REPEAT_MONTH;
                break;
            }
            var.setValue(val);

            ui->cbTaskPeriod->addItem(repeatPeriods[i][2], var);
        }
    }
end:
    ui->cbTaskPeriod->setCurrentIndex(selectedPeriod);
    return;
}

void WTaskEdit::saveAll() {
    QString buffTitle;

    if (!ui->leTaskTitle->text().isEmpty()) {
        buffTitle = ui->leTaskTitle->text();
    } else {
        buffTitle = tr(" New task ");
    }

    QVariant var;
    var = ui->cbTaskPeriod->itemData(ui->cbTaskPeriod->currentIndex(),
                                     Qt::UserRole);

    int everyPeriod = var.value<int>();
    int everyValue = ui->sbRepeatEvery->value();
    int titleAddition = 1;
    int dateIncrement;

    if (ui->cbTaskRepeat->isChecked()) {
        int seriesNum = STDataProvider::getInstance().getNewSeriesNumber();
        QDate buffDate = ui->deRepeatFrom->date();

        while (buffDate <= ui->deRepeatTo->date()) {

            // Modify tilte if needed
            if (ui->cbAddNumber->isChecked()) {
                saveTodo(buffTitle + " " + QString::number(titleAddition), buffDate,
                         seriesNum);
                titleAddition++;
            } else {
                saveTodo(buffTitle, buffDate, seriesNum);
            }
            dateIncrement = everyValue * everyPeriod;
            buffDate = buffDate.addDays(+dateIncrement);
        }
    } else {
        QDate buffDate = ui->deTaskTime->date();
        saveTodo(buffTitle, buffDate, currentSeries);
    }
}

void WTaskEdit::saveTodo(QString inTitle, QDate inDate, int inSeries) {
    STTask buffTask;
    buffTask.id = currentId;
    buffTask.isComplete = ui->cbTaskCheck->isChecked();

    buffTask.date = inDate;
    buffTask.task = inTitle;
    buffTask.description = ui->teTaskDescription->toPlainText();

    QVariant var;
    var = ui->cbTaskSubj->itemData(ui->cbTaskSubj->currentIndex(), Qt::UserRole);
    buffTask.subjectId = var.value<int>();

    var = ui->cbTaskPriority->itemData(ui->cbTaskPriority->currentIndex(),
                                       Qt::UserRole);
    buffTask.prior = var.value<int>();

    buffTask.seriesNumber = inSeries;

    if (currentId == INVALID_ID) {
        STDataProvider::getInstance().addTask(buffTask);
    } else {
        STDataProvider::getInstance().modTask(buffTask);
    }
}

void WTaskEdit::addTodo() {
    saveAll();

    finishEdit();
}

void WTaskEdit::removeTodo() {
    if (currentId != INVALID_ID) {
        STDataProvider::getInstance().delTask(currentId);
    }

    currentId = INVALID_ID;
    finishEdit();
}

void WTaskEdit::removeSeries() {
    if (currentSeries != INVALID_ID) {
        STDataProvider::getInstance().delTaskSeries(currentSeries);
    }

    currentId = INVALID_ID;
    finishEdit();
}

void WTaskEdit::finishEdit() {
    if (!ui->pbPinSingleDark->isChecked()) {
        emit sigNeedSchedule();
        currentId = INVALID_ID;
    } else {
        emit sigNeedRefresh();
        fillFields(INVALID_ID);
    }
}

void WTaskEdit::closeEditor() {
    if (currentId != INVALID_ID) {
        saveAll();
    }
    emit sigNeedSchedule();
}

void WTaskEdit::checkDates(QDate inDate) {
    if (inDate < ui->deRepeatFrom->date()) {
        QDate buffDate = ui->deRepeatFrom->date().addDays(+7);
        ui->deRepeatTo->setDate(buffDate);
    }
}

void WTaskEdit::somethingChanged() {
    if (currentId != INVALID_ID) {
        saveAll();
        emit sigNeedRefresh();
    }
}
