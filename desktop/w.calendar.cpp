#include "w.calendar.h"
#include "ui_w.calendar.h"

const int WCalendar::HEADER_ROW = 0;
const int WCalendar::FIRST_ROW = 1;
const int WCalendar::COUNT_OF_ROW = 6;
const QString WCalendar::CSS_HEADER = "color:#fff;";
const QString WCalendar::CSS_OTHER_MONTH = "color:#777;";
const QString WCalendar::CSS_CURRENT_MONTH = "color: #333;";
const QString WCalendar::CSS_SELECTED_DAY = "border:1px solid #3773d9;";
const QString WCalendar::CSS_TODAY = "border:1px solid #eb513d;";

WCalendar::WCalendar(QWidget *parent) : QWidget(parent), ui(new Ui::WCalendar) {
    isInitialized = false;
    ui->setupUi(this);
    setupUiForm();

    connect(ui->pbLeftBlock, SIGNAL(clicked()), SLOT(slotDecMonth()));
    connect(ui->pbRightBlock, SIGNAL(clicked()), SLOT(slotIncMonth()));
}

WCalendar::~WCalendar() { delete ui; }

void WCalendar::fillCalendarOnDate(QDate inDate) {
    QGridLayout *grid = (QGridLayout *)ui->frmCalendarBackground->layout();
    QDate date(inDate.year(), inDate.month(), 1);
    QLabel *ccDate;
        // Qt can't chow russian months correctly
    QString months[] = {tr("January"), tr("February"), tr("March"), tr("April"),
                        tr("May"), tr("June"), tr("June"), tr("August"),
                        tr("September"), tr("October"), tr("November"), tr("December")};

    ui->lbCenterBlock->setText(months[inDate.month() - 1] +
            inDate.toString(" yyyy"));
    // Go on monday
    do {
        date = date.addDays(-1);
    } while (date.dayOfWeek() != Qt::Monday);

    // Fill
    for (int row = FIRST_ROW; row < FIRST_ROW + COUNT_OF_ROW;) {
        int weekDay = date.dayOfWeek();

        ccDate = (QLabel *)grid->itemAtPosition(row, weekDay - 1)->widget();
        ccDate->setText(QString::number(date.day()));
        ccDate->setProperty(W_CALENDAR_DATE, QVariant(date));

        // Goto like a switch, don't kick me
        if (date == inDate) {
            ccDate->setStyleSheet(CSS_SELECTED_DAY);

            QPropertyAnimation *moveWeekSelector;
            moveWeekSelector = new QPropertyAnimation(ui->lbWeekSelector, "geometry");
            moveWeekSelector->setDuration(W_CALENDAR_DURATION);
            moveWeekSelector->setStartValue(ui->lbWeekSelector->geometry());

            QRect frameGeom;
            if (isInitialized) {
                frameGeom = ui->frmCalendarBackground->geometry();
            } else {
                frameGeom = QRect(BORDER_WIDTH, MONTH_TITLE, WEEK_SELECTOR_WIDTH,
                                  WEEK_SELECTOR_HEIGHT);
                isInitialized = true;
            }
            QRect newGeom(BORDER_WIDTH, MONTH_TITLE + WEEK_DAYS_HEIGHT +
                          (CALENDAR_CELL_SIZE + 1) * (row - 1),
                          WEEK_SELECTOR_WIDTH, WEEK_SELECTOR_HEIGHT);
            moveWeekSelector->setEndValue(newGeom);
            moveWeekSelector->start();

            goto fillEnd;
        }
        if (date == QDate::currentDate()) {
            ccDate->setStyleSheet(CSS_TODAY);
            goto fillEnd;
        }
        // default
        if (date.month() == inDate.month()) {
            ccDate->setStyleSheet(CSS_CURRENT_MONTH);
        } else {
            ccDate->setStyleSheet(CSS_OTHER_MONTH);
        }

fillEnd:
        date = date.addDays(1);

        if (weekDay == Qt::Sunday) {
            row++;
        }
    }
}

void WCalendar::mousePressEvent(QMouseEvent *ev) {
    QPoint pressPoint = ev->pos();
    pressPoint.setY(pressPoint.y() - ui->frmCalendarBackground->pos().y());

    QWidget *hit;
    hit = this->ui->frmCalendarBackground->childAt(pressPoint);

    QLabel *lblHit = dynamic_cast<QLabel *>(hit);
    if (lblHit) {
        emit selectedDateChanged(lblHit->property(W_CALENDAR_DATE).toDate());

        ev->accept();
    }

    ev->ignore();
}

void WCalendar::setupUiForm() {
    QGridLayout *grid = (QGridLayout *)ui->frmCalendarBackground->layout();

    // Header
    QLabel *lblHead;
    for (int i = 1; i <= 7; i++) {
        lblHead = new QLabel(STDataProvider::getInstance().getDaysOfWeek()[i], this);
        lblHead->setFixedSize(CALENDAR_CELL_SIZE, WEEK_DAYS_HEIGHT);
        lblHead->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        lblHead->setStyleSheet(CSS_HEADER);
        grid->addWidget(lblHead, HEADER_ROW, i - 1);
    }

    // Calendar
    QLabel *ccDate;
    for (int i = 0; i < COUNT_OF_ROW; i++) {
        for (int j = 0; j < 7; j++) {
            ccDate = new QLabel(this->ui->frmCalendarBackground);
            ccDate->setFixedSize(CALENDAR_CELL_SIZE, CALENDAR_CELL_SIZE);
            ccDate->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            grid->addWidget(ccDate, FIRST_ROW + i, j);
        }
    }
}

void WCalendar::slotIncMonth() { emit sigIncMonth(); }

void WCalendar::slotDecMonth() { emit sigDecMonth(); }
