/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_DAYSTASKS_H
#define W_DAYSTASKS_H

#include "include.h"
#include "w.taskline.h"

enum {
    STATE_CALENDAR,
    STATE_OVERDUE,
    STATE_ALL
};

namespace Ui {
    class WDaysTasks;
}

class WDaysTasks : public QWidget {
        Q_OBJECT

    signals:
        void sigNeedTaskEdit(int);

    public:
        explicit WDaysTasks(QWidget *parent = 0);
        ~WDaysTasks();
        bool fillTasks(QDate inDate, STFilterElement inFilter);
        bool fillOverDue();
        bool fillAllTasks(STFilterElement inFilter);

        int id;

    private:
        Ui::WDaysTasks *ui;

        int currentState;
        QDate currentDate;
        STFilterElement currentFilter;

    private slots:
        void taskSelected(int inId);
        bool refreshAll();
};

#endif  // W_DAYSTASKS_H
