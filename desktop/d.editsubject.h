/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef D_EDITSUBJECT_H
#define D_EDITSUBJECT_H

#include "include.h"

namespace Ui {
    class DEditSubject;
}

class DEditSubject : public QDialog {
        Q_OBJECT

    public:
        explicit DEditSubject(QWidget *parent = 0);
        ~DEditSubject();

    protected:
        void showEvent(QShowEvent *inSE);
        void closeEvent(QCloseEvent *inCE);
        virtual bool eventFilter(QObject *inObject, QEvent *inEvent);

    private:
        Ui::DEditSubject *ui;

        bool isHaveFakeSubjects();
        void clearTable();
        void addSubjectInTable(STSubject *inSubject);
        void fillTable();

        void disableTableEditMode();

    private slots:
        void addSubject();
        void editSubject(QTableWidgetItem *inItem);
        void delSubject();
        void showDelete(bool inShow);
};

#endif  // D_EDITSUBJECT_H
