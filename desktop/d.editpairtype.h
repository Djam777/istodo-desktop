/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef D_EDITPAIRTYPE_H
#define D_EDITPAIRTYPE_H

#include "include.h"

namespace Ui {
    class DEditPairType;
}

// "Pair" can be translated to english as lesson, classes

class DEditPairType : public QDialog {
        Q_OBJECT

    public:
        explicit DEditPairType(QWidget *parent = 0);
        ~DEditPairType();

    protected:
        void showEvent(QShowEvent *inSE);
        void closeEvent(QCloseEvent *inCE);
        virtual bool eventFilter(QObject *inObject, QEvent *inEvent);

    private:
        Ui::DEditPairType *ui;

        bool isHaveFakePairTypes();
        void clearTable();
        void addPairTypeInTable(STPair::Type *inPairType);
        void fillTable();

        void disableTableEditMode();

    private slots:
        void addPairType();
        void editPairType(QTableWidgetItem *inItem);
        void delPairType();
        void showDelete(bool inShow);
};

#endif  // D_EDITPAIRTYPE_H
