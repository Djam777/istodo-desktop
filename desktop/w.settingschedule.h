/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef W_SETTINGSCHEDULE_H
#define W_SETTINGSCHEDULE_H

#include "include.h"
#include "w.container.h"
#include "d.editteacher.h"
#include "d.editpairtype.h"
#include "d.editsubject.h"

#include "w.droppairtrash.h"
#include "w.editschedule.h"

namespace Ui {
    class WSettingSchedule;
}

class WSettingSchedule : public QWidget {
        Q_OBJECT

    signals:
        void scheduleChanged();
        void needHelp();

    public:
        explicit WSettingSchedule(QWidget *parent = 0);
        ~WSettingSchedule();
        void enableFirstLaunchMode();
        void enableSmallScreenSupport();

    protected:
        void showEvent(QShowEvent *inSE);

    private:
        void addSubjectInList(STSubject *inSubject);
        void clearSubjectList();

        void addPairTypeInList(STPair::Type *inPairType);
        void clearPairTypeList();

        void addTeacherInList(STTeacher *inTeacher);
        void clearTeacherList();

        Ui::WSettingSchedule *ui;
        DEditSubject *editSubject;
        DEditPairType *editPairType;
        DEditTeacher *editTeacher;

    private slots:
        void fillSubjectList();
        void fillPairTypeList();
        void fillTeacherList();

        void setDragSubject(int inRow);
        void setDragPairType(int inRow);
        void setDragTeacher(int inRow);
        void setDragAuditory(QString inAuditory);
        void setDragAdditions(QString inAdditions);

        void sendScheduleChanged();
        void helpRequested();
};

#endif  // W_SETTINGSCHEDULE_H
