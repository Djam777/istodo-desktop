/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef D_SETTINGSEMESTER_H
#define D_SETTINGSEMESTER_H

#include "include.h"

namespace Ui {
    class DSettingSemester;
}

class DSettingSemester : public QDialog {
        Q_OBJECT

    public:
        explicit DSettingSemester(QWidget *parent = 0);
        ~DSettingSemester();

    private:
        static const int DSS_PAIR_LENGTH = 90;
        static const int DSS_SEM_LENGTH = 17;
        Ui::DSettingSemester *ui;

        void updateData();
        void updateUi();

    private slots:
        void slotChancel();
        void slotOk();
};

#endif  // D_SETTINGSEMESTER_H
