#include "st.updaterclient.h"

STUpdaterClient::STUpdaterClient() : QObject() {
    connect(&client, SIGNAL(connected()), SLOT(startUpdate()));
}

void STUpdaterClient::checkUpdate() {
    //    Connect to server, function startUpdate will start
    //    automaticly, where socket send connected
    //    QHostAddress serverAddr;
    //    serverAddr.setAddress("127.0.0.1");
    //    client.connectToHost( serverAddr, STODO_UPDATER_PORT,
    // QIODevice::ReadWrite );
    client.connectToHost(STODO_UPDATER_NAME, STODO_UPDATER_PORT,
                         QIODevice::ReadWrite);
}

void STUpdaterClient::startUpdate() {
    QDataStream stream(&client);

    STUpdateRequest request;

    request.buildNumber = STODO_BUILD_NUMBER;
    stream << request.packageId;
    request.write(stream);

    // a bit of magic...
    while (!client.waitForReadyRead(20) &&
           client.state() == QTcpSocket::ConnectedState) {
        qApp->processEvents();
    }

    int packageId;
    stream >> packageId;
    switch (packageId) {
        case STUpdateResponse::packageId: {
            STUpdateResponse response;
            response.read(stream);
            emit needUpdate();
            break;
        }
        case STNotResponse::packageId: {
            STNotResponse response;
            response.read(stream);
            qDebug() << Q_FUNC_INFO << " Update not needed";
            break;
        }
        default: {
            qDebug() << Q_FUNC_INFO << " Unknow packege id: " << packageId;
            break;
        }
    }

    client.disconnectFromHost();
}

void STUpdaterClient::stopUpdate() {
    qDebug() << "Update stoped";
    client.disconnectFromHost();
}
