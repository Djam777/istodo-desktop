TEMPLATE = subdirs

include(dataprovider/dataprovider.pri)
include(updaterCommon/updaterCommon.pri)

TRANSLATIONS += Languages/iStodo_ru.ts

SUBDIRS += \
            desktop \
            updaterServer

HEADERS += \
        const.h \
SOURCES += \
        const.cpp
