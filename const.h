/****************************************************************************
**
** Copyright © 2014 Eremin Yakov and Putintsev Roman.
** All rights reserved.
** Contact: support@istodo.ru
**
**
** This file is part of iStodo.
**
** iStodo is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** iStodo is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with iStodo.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef CONST_H
#define CONST_H

#include <QtGlobal>
#include <QString>

#define ST_OPENSOURCE true
#define ST_BASIC_VERSION 1310

#ifdef Q_OS_WIN32
#define ST_VERSION ST_BASIC_VERSION+1
#endif

#ifdef Q_OS_LINUX
#define ST_VERSION ST_BASIC_VERSION+2
#endif

#ifdef Q_OS_MAC
#define ST_VERSION ST_BASIC_VERSION+3
#endif

#define ST_STR_VERSION QString("%1.%2.%3")\
    .arg((ST_VERSION)/1000%10)\
    .arg((ST_VERSION)/100%10)\
    .arg((ST_VERSION)/10%10)

#define DB_NAME "iStodo.db"
#define DP_VERSION 1
#define INVALID_ID -1
#define EMPTY_COLOR 244,247,252

#define SET_DELETE_ITEM "containers/delete_item"
#define SET_DELETE_BELL "editschedule/delete_bell"
#define SET_TOKEN_SECRET "token/secret"
#define SET_TOKEN "token/token"

#define FIRST_DAY_OF_WEEK Qt::Monday
#define LAST_DAY_OF_WEEK Qt::Sunday

#define W_SLIDE_ANIMATION_TIME 300

#endif // CONST_H
