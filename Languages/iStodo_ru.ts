<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DEditPairType</name>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="14"/>
        <source>Types of classes</source>
        <translation>Типы пар</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="62"/>
        <source>Short name</source>
        <translation>Отображаемое название</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="67"/>
        <source>Full name</source>
        <translation>Полное название</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="88"/>
        <source>Add new classes type</source>
        <translation>Добавить новый тип пары</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="104"/>
        <source>Edit mode</source>
        <translation>Включить режим удаления</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="141"/>
        <source>Tip: convenient to add few rows to start</source>
        <translation>Совет: удобнее добавить сразу несколько строк</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.ui" line="161"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.cpp" line="52"/>
        <source>All classes types with empty short name will be removed, continue?</source>
        <translation>Все типы пар с пустым полем отображемого названия будут удалены, продолжить?</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.cpp" line="51"/>
        <location filename="../desktop/d.editpairtype.cpp" line="287"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../desktop/d.editpairtype.cpp" line="288"/>
        <source>Type removing causes removing all classes with this type, continue?</source>
        <translation>Удаление типа удалит все пары этого типа. Продолжить?</translation>
    </message>
</context>
<context>
    <name>DEditSubject</name>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="14"/>
        <source>Subjects</source>
        <translation>Предметы</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="17"/>
        <source>Edit mode</source>
        <translation>Включить режим удаления</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="65"/>
        <source>Short name</source>
        <translation>Отображаемое название</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="70"/>
        <source>Full Name</source>
        <translation>Полное название</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="91"/>
        <source>Add new subject</source>
        <translation>Добавить новый предмет</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="141"/>
        <source>Tip: convenient to add few rows to start</source>
        <translation>Совет: удобнее добавить сразу несколько строк</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.ui" line="161"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.cpp" line="52"/>
        <source>All subjects with empty short name will be removed, continue?</source>
        <translation>Все предметы с пустым полем отображемого названия будут удалены, продолжить?</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.cpp" line="51"/>
        <location filename="../desktop/d.editsubject.cpp" line="285"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../desktop/d.editsubject.cpp" line="286"/>
        <source>Subject removing causes removing all classes with this subject, continue?</source>
        <translation>Удаление предмета удалит все пары по данному предмету. Продолжить?</translation>
    </message>
</context>
<context>
    <name>DEditTeacher</name>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="14"/>
        <source>Teachers</source>
        <translation>Преподаватели</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="71"/>
        <source>Nickname</source>
        <translation>Отображаемое имя</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="76"/>
        <source>Full name</source>
        <translation>ФИО</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="81"/>
        <source>Additional info</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="102"/>
        <source>Add new teacher</source>
        <translation>Добавить нового преподавателя</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="118"/>
        <source>Edit mode</source>
        <translation>Включить режим удаления</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="155"/>
        <source>Tip: convenient to add few rows to start</source>
        <translation>Совет: удобнее добавить сразу несколько строк</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.ui" line="187"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.cpp" line="52"/>
        <source>All teachers with empty nickname will be removed, continue?</source>
        <translation>Все преподаватели с пустым полем отображемого имени будут удалены, продолжить?</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.cpp" line="51"/>
        <location filename="../desktop/d.editteacher.cpp" line="291"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../desktop/d.editteacher.cpp" line="292"/>
        <source>Teacher removing causes removing all classes with this teacher, continue?</source>
        <translation>Удаление преподавателя удалит все пары, которые он ведет. Продолжить?</translation>
    </message>
</context>
<context>
    <name>DSettingSemester</name>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="20"/>
        <source>Semester settings</source>
        <translation>Настройки семестра</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="58"/>
        <source>Start of semester</source>
        <translation>Начало семестра</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="85"/>
        <source>End of semester</source>
        <translation>Конец семестра</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="125"/>
        <source>Count of weeks</source>
        <translation>Продолжительность
семестра (недель)</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="168"/>
        <source>Lesson duration (min)</source>
        <translation>Продолжительность
пары (минут)</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="242"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../desktop/d.settingsemester.ui" line="274"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
</context>
<context>
    <name>STDataProvider</name>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="978"/>
        <location filename="../dataprovider/st.dataprovider.cpp" line="986"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="979"/>
        <source>Can&apos;t remove file with the old data</source>
        <translation>Не могу удалить файл со старыми данными</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="987"/>
        <source>Can&apos;t copy file with the new data</source>
        <translation>Не могу скопировать файл с новыми данными</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="7"/>
        <source>Mo</source>
        <translation>Пн</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="7"/>
        <source>Tu</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="7"/>
        <source>We</source>
        <translation>Ср</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="8"/>
        <source>Th</source>
        <translation>Чт</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="8"/>
        <source>Fr</source>
        <translation>Пт</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="8"/>
        <source>Sa</source>
        <translation>Сб</translation>
    </message>
    <message>
        <location filename="../dataprovider/st.dataprovider.cpp" line="8"/>
        <source>Su</source>
        <translation>Вс</translation>
    </message>
</context>
<context>
    <name>STDropboxExporter</name>
    <message>
        <location filename="../desktop/st.dropboxexporter.cpp" line="59"/>
        <source>Authorization</source>
        <translation>Авторизация</translation>
    </message>
    <message>
        <location filename="../desktop/st.dropboxexporter.cpp" line="60"/>
        <source>After logging on to Dropbox - click Ok</source>
        <translation>После входа в Dropbox — нажмите Ok</translation>
    </message>
</context>
<context>
    <name>STICalExporter</name>
    <message>
        <location filename="../dataprovider/st.icalexporter.cpp" line="241"/>
        <source>General</source>
        <translation>Общее</translation>
    </message>
</context>
<context>
    <name>WAbout</name>
    <message>
        <location filename="../desktop/w.about.ui" line="14"/>
        <source>About iStodo</source>
        <translation>О программе iStodo</translation>
    </message>
    <message>
        <location filename="../desktop/w.about.ui" line="54"/>
        <source>iStodo</source>
        <translation>iStodo</translation>
    </message>
    <message>
        <location filename="../desktop/w.about.ui" line="68"/>
        <source>cybernetic students assistant </source>
        <translation>кибенетический помощник студента</translation>
    </message>
    <message>
        <location filename="../desktop/w.about.ui" line="82"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../desktop/w.about.ui" line="151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sorry, this text still not translated :/ &lt;a href=&quot;mailto:support@istodo.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Email&lt;/span&gt;&lt;/a&gt; us with any questions!&lt;/p&gt;
&lt;br/&gt;
&lt;p&gt;P.S. &lt;a href=&quot;http://brankic1979.com/icons/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Free Icons Set designed by Brankic1979&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Программа-органайзер &lt;a href=&quot;http://istodo.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;iStodo&lt;/span&gt;&lt;/a&gt; создана для составления и контроля расписания, а так же планирования учебной деятельности на период вплоть до семестра. &lt;/p&gt;&lt;p&gt;При составлении расписания необходимо сначала составить таблицу звоноков, затем добавить преподавателей и предметы. Далее, из списков предметов и преподавателей выбираются нужные, таким образом формируются пары. Для того, чтобы добавить пару в расписание, следует перетянуть ее на сетку звонков. Если расписание состоит из нескольких отличающихся недель(например, четные и нечетные), следует добавить дополнительную неделю, и заполнить ее приведенным выше образом. &lt;/p&gt;&lt;p&gt;Планирование учебной деятельности реализовано системой задач. При создании задачи можно указать выбрать дату выполнения, предмет, а так же приоритет. Реализована возможность автоматического добавления задач с гибко настраиваемым интервалом повторения: каждые несколько дней, недель, месяцев.&lt;/p&gt;&lt;p&gt;Доступно приложение-просмотрщик под &lt;a href=&quot;https://play.google.com/store/apps/details?id=ru.istodo.istodo&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Android&lt;/span&gt;&lt;/a&gt;, также вы можете использовать экспорт в формате iCal для просмотра расписания в календаре вашего устройства. &lt;/p&gt;&lt;p&gt;Из дополнительных возможностей присутствует экспорт/импорт всей информации из приложения, что позволяет делать резервные копии, делиться с одногруппниками. &lt;/p&gt;&lt;p&gt;Нужна помощь? Есть предложение? &lt;a href=&quot;mailto:support@istodo.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Напишите&lt;/span&gt;&lt;/a&gt; нам!&lt;/p&gt;&lt;p&gt;P.S. Выражаем признательность дизайнеру иконок, разместившему в общем доступе свой набор: &lt;a href=&quot;http://brankic1979.com/icons/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Free Icons Set designed by Brankic1979&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../desktop/w.about.ui" line="179"/>
        <source>© 2015 iStodo team</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WCalendar</name>
    <message>
        <location filename="../desktop/w.calendar.ui" line="92"/>
        <location filename="../desktop/w.calendar.cpp" line="31"/>
        <source>September</source>
        <translation>Сентябрь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="29"/>
        <source>January</source>
        <translation>Январь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="29"/>
        <source>February</source>
        <translation>Февраль</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="29"/>
        <source>March</source>
        <translation>Март</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="29"/>
        <source>April</source>
        <translation>Апрель</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="30"/>
        <source>May</source>
        <translation>Май</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="30"/>
        <source>June</source>
        <translation>Июнь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="30"/>
        <source>August</source>
        <translation>Август</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="31"/>
        <source>October</source>
        <translation>Октябрь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="31"/>
        <source>November</source>
        <translation>Ноябрь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.cpp" line="31"/>
        <source>December</source>
        <translation>Декабрь</translation>
    </message>
    <message>
        <location filename="../desktop/w.calendar.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WContainer</name>
    <message>
        <location filename="../desktop/w.container.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.ui" line="100"/>
        <source>Edit list</source>
        <translation>Редактировать список</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.ui" line="256"/>
        <source>To get started</source>
        <translation>Для начала работы</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.ui" line="273"/>
        <source>you need to add teachers</source>
        <translation>добавьте преподавателей</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="22"/>
        <source>Teachers</source>
        <translation>Преподаватели</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="23"/>
        <source>add teachers</source>
        <translation>добавьте преподавателей</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="29"/>
        <source>Types</source>
        <translation>Типы пар</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="30"/>
        <source>add types</source>
        <translation>добавьте типы пар</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="36"/>
        <source>Subjects</source>
        <translation>Предметы</translation>
    </message>
    <message>
        <location filename="../desktop/w.container.cpp" line="37"/>
        <source>add subjects</source>
        <translation>Добавьте предметы</translation>
    </message>
</context>
<context>
    <name>WDaysTasks</name>
    <message>
        <location filename="../desktop/w.daystasks.ui" line="20"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.daystasks.cpp" line="89"/>
        <source>Overdue</source>
        <translation>Задолженности</translation>
    </message>
    <message>
        <location filename="../desktop/w.daystasks.cpp" line="113"/>
        <source>All tasks</source>
        <translation>Все задачи</translation>
    </message>
</context>
<context>
    <name>WDragPairCreate</name>
    <message>
        <location filename="../desktop/w.dragpaircreate.cpp" line="108"/>
        <source>&lt;p style = &quot; margin:5px; margin-top:2px; font-family: Arial; font-size: 16px; color:#333; &quot;&gt;</source>
        <comment>14px for ru locale</comment>
        <translation>&lt;p style = &quot; margin:5px; margin-top:2px; font-family: Arial; font-size: 14px; color:#333; &quot;&gt;</translation>
    </message>
    <message>
        <location filename="../desktop/w.dragpaircreate.cpp" line="113"/>
        <source>Select data&lt;br&gt;from &lt;b&gt;all&lt;/b&gt; lists,&lt;br&gt;and drag&lt;br&gt;&lt;b&gt;this&lt;/b&gt; element to timetable.</source>
        <translation>Выберите&lt;br&gt;нужные данные&lt;br&gt;из &lt;b&gt;всех&lt;/b&gt; списков,&lt;br&gt;и перетяните&lt;br&gt;&lt;b&gt;этот&lt;/b&gt; элемент&lt;br&gt;на таблицу расписания.</translation>
    </message>
</context>
<context>
    <name>WEditSchedule</name>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="105"/>
        <source>Add/remove week</source>
        <translation>Добавить/удалить неделю цикла</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="108"/>
        <source>Week 2</source>
        <translation>Неделя 2</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="156"/>
        <source>Timetable</source>
        <translation>Таблица расписания</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="176"/>
        <source>Add row</source>
        <translation>Добавить строку</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="189"/>
        <source>Remove row</source>
        <translation>Удалить строку</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="202"/>
        <source>Edit table</source>
        <translation>Редактировать звонки</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="264"/>
        <location filename="../desktop/w.editschedule.ui" line="269"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="274"/>
        <source>Monday</source>
        <translation>Понедельник</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="279"/>
        <source>Tuesday</source>
        <translation>Вторник</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="284"/>
        <source>Wednesday</source>
        <translation>Среда</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="289"/>
        <source>Thursday</source>
        <translation>Четверг</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="294"/>
        <source>Friday</source>
        <translation>Пятница</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.ui" line="299"/>
        <source>Saturday</source>
        <translation>Суббота</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="4"/>
        <source>  Week </source>
        <translation>  Неделя </translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="12"/>
        <source>Add week</source>
        <translation>Добавить неделю цикла</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="13"/>
        <source>Copy week</source>
        <translation>Копировать неделю цикла</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="14"/>
        <source>Remove week</source>
        <translation>Удалить неделю цикла</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="325"/>
        <location filename="../desktop/w.editschedule.cpp" line="433"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="326"/>
        <source>Classes in bottom row will be removed from all weeks, continue?</source>
        <translation>Пары всех недель с последним звонком будут удалены, продолжить?</translation>
    </message>
    <message>
        <location filename="../desktop/w.editschedule.cpp" line="434"/>
        <source>All classes of this week will be removed, continue?</source>
        <translation>Все пары этой недели будут удалены. Продолжить?</translation>
    </message>
</context>
<context>
    <name>WFilter</name>
    <message>
        <location filename="../desktop/w.filter.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.filter.ui" line="61"/>
        <source>Show all</source>
        <translation>Показывать все</translation>
    </message>
    <message>
        <location filename="../desktop/w.filter.ui" line="89"/>
        <source>Show</source>
        <translation>Отображать</translation>
    </message>
    <message>
        <location filename="../desktop/w.filter.ui" line="105"/>
        <source>Hide all</source>
        <translation>Не показывать ничего</translation>
    </message>
    <message>
        <location filename="../desktop/w.filter.cpp" line="77"/>
        <source>General</source>
        <translation>Общее</translation>
    </message>
</context>
<context>
    <name>WPageShow</name>
    <message>
        <location filename="../desktop/w.pageshow.ui" line="20"/>
        <source>Form</source>
        <translation>Новая версия доступна</translation>
    </message>
    <message>
        <location filename="../desktop/w.pageshow.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;New version available! &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Тихо и незаметно вышла новая версия кибернетического помощника студента iStodo. Наверняка была улучшена стабильность, исправлены ошибки, добавлены новые(классные) функции. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../desktop/w.pageshow.ui" line="102"/>
        <source>&lt;html&gt; &lt;head/&gt; &lt;body&gt; &lt;p&gt; &lt;a href=&quot;http://istodo.ru/update&quot;&gt; &lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;View a changelog and download&lt;/span&gt; &lt;/a&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</source>
        <translation>&lt;html&gt; &lt;head/&gt; &lt;body&gt; &lt;p&gt; &lt;a href=&quot;http://istodo.ru/update&quot;&gt; &lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Посмотреть список изменений и загрузить&lt;/span&gt; &lt;/a&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../desktop/w.pageshow.ui" line="130"/>
        <source>Show</source>
        <translation>Отображать</translation>
    </message>
    <message>
        <location filename="../desktop/w.pageshow.cpp" line="11"/>
        <source>New version of iStodo avalible</source>
        <translation>Доступна новая версия iStodo</translation>
    </message>
</context>
<context>
    <name>WScheduleOnWeek</name>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="83"/>
        <source>Previous week</source>
        <translation>На неделю назад</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="96"/>
        <source>Next week</source>
        <translation>На неделю вперед</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="131"/>
        <source>Timetable</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="128"/>
        <location filename="../desktop/w.scheduleonweek.ui" line="167"/>
        <source>Show schedule settings</source>
        <translation>Открыть настройки расписания</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="211"/>
        <source>Day</source>
        <translation>День</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="216"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="221"/>
        <source>Subject</source>
        <translation>Предмет</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="226"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="231"/>
        <source>Teacher</source>
        <translation>Препод.</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="236"/>
        <source>Room</source>
        <translation>Аудит.</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="241"/>
        <source>Info</source>
        <translation>Прим.</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.cpp" line="19"/>
        <source>Back to today (⌘+T)</source>
        <translation>Вернуться к сегодняшнему дню (⌘+T)</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.cpp" line="21"/>
        <source>Back to today (Ctrl+T)</source>
        <translation>Вернуться к сегодняшнему дню (Ctrl+T)</translation>
    </message>
    <message>
        <location filename="../desktop/w.scheduleonweek.ui" line="154"/>
        <source>Back to today</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WSettingGeneral</name>
    <message>
        <location filename="../desktop/w.settinggeneral.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.settinggeneral.ui" line="26"/>
        <source>Show news</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WSettingSchedule</name>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="26"/>
        <source>Schedule settings</source>
        <translation>Настройки расписания</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="156"/>
        <source>Classroom</source>
        <translation>Аудитория</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="278"/>
        <source>Select data from all lists, drag to timetable</source>
        <translation>Выберите данные из списков, перетяните на таблицу слева</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="306"/>
        <source>Drag-n-drop wrong classes</source>
        <translation>Перетяните ненужные пары сюда</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="375"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="494"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="510"/>
        <source>Add. Select. Drag to table.</source>
        <translation>Добавить. Выбрать. Перетянуть.</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="520"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../desktop/w.settingschedule.ui" line="538"/>
        <source>Lesson is formed in the orange box below.</source>
        <translation>Пара формируется в оранжевом квадрате ниже.</translation>
    </message>
</context>
<context>
    <name>WStodo</name>
    <message>
        <location filename="../desktop/w.stodo.ui" line="20"/>
        <source>iStodo</source>
        <translation>iStodo</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="204"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="219"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="226"/>
        <location filename="../desktop/w.stodo.ui" line="292"/>
        <source>About</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="242"/>
        <source>Create backup</source>
        <translation>Создать резервную копию</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="247"/>
        <location filename="../desktop/w.stodo.cpp" line="234"/>
        <source>Load backup</source>
        <translation>Загрузить резервную копию</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="252"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="257"/>
        <source>Edit schedule</source>
        <translation>Изменить расписание</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="262"/>
        <source>Edit semester</source>
        <translation>Настройки семестра</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="267"/>
        <source>Support</source>
        <translation>Техническая поддержка</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="277"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="302"/>
        <source>vk.com</source>
        <translation>ВКонтакте</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="307"/>
        <source>About Qt</source>
        <translation>О библиотеке Qt</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="312"/>
        <location filename="../desktop/w.stodo.cpp" line="263"/>
        <source>Export schedule to iCal</source>
        <translation>Экспорт расписания в iCal</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="317"/>
        <location filename="../desktop/w.stodo.cpp" line="277"/>
        <source>Export tasks to iCal</source>
        <translation>Экспорт задач в iCal</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="322"/>
        <source>Export to Dropbox</source>
        <translation>Экспорт копии в Dropbox</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="327"/>
        <source>Import from Dropbox</source>
        <translation>Импорт данных из Dropbox</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="96"/>
        <source>l.</source>
        <translation>л.</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="97"/>
        <source>Lection</source>
        <translation>Лекция</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="100"/>
        <source>Lab. work</source>
        <translation>Лаб. работа</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="102"/>
        <source>pr.</source>
        <translation>пр.</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="103"/>
        <source>Practise</source>
        <translation>Практика</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="105"/>
        <source>sem.</source>
        <translation>сем.</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="106"/>
        <source>Seminar</source>
        <translation>Семинар</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="246"/>
        <source>Save backup</source>
        <translation>Создать резервную копию</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="256"/>
        <location filename="../desktop/w.stodo.cpp" line="293"/>
        <location filename="../desktop/w.stodo.cpp" line="317"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="257"/>
        <source>Can&apos;t copy data file</source>
        <translation>Не могу скопировать файл с данными</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="294"/>
        <location filename="../desktop/w.stodo.cpp" line="318"/>
        <source>The synchronization is only available in the version from official website for technical reasons</source>
        <translation>По техническим причинам синхронизация доступна только в версии с официального сайта</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="309"/>
        <source>Export completed successfully</source>
        <translation>Экспорт успешно завершен</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="333"/>
        <source>Import completed successfully</source>
        <translation>Импорт успешно завершен</translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="272"/>
        <source>Отправить отзыв...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="282"/>
        <source>Загрузить расписание .istd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="287"/>
        <source>Twitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.ui" line="297"/>
        <source>Общие настройки</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop/w.stodo.cpp" line="99"/>
        <source>l.w.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WTaskEdit</name>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="104"/>
        <source>Pin editor</source>
        <translation>Закрепить редактор</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="133"/>
        <source>Add task</source>
        <translation>Добавить задачу</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="210"/>
        <source>Title:</source>
        <translation>Заголовок:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="217"/>
        <source>New task</source>
        <translation>Новая задача</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="325"/>
        <source>Date:</source>
        <translation>Дата выполнения:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="346"/>
        <source>Subject:</source>
        <translation>Предмет:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="356"/>
        <source>Repeat:</source>
        <translation>Повторять:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="379"/>
        <source>Priority:</source>
        <translation>Приоритет:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="421"/>
        <source>Every:</source>
        <translation>Каждый:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="440"/>
        <source>from:</source>
        <translation>с:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="469"/>
        <source>to:</source>
        <translation>по:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="476"/>
        <source>Add number:</source>
        <translation>Добавлять номер:</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="512"/>
        <source>Tip: use the calendar on the left to control the date</source>
        <translation>Совет: используйте календарь слева для управления датой</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="561"/>
        <source>Remove this task</source>
        <translation>Удалить задачу</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="564"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="580"/>
        <source>Remove all autocreated task series</source>
        <translation>Удалить сгенерированные задачи</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="583"/>
        <source>Remove series</source>
        <translation>Удалить всю серию</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="615"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="13"/>
        <source> Low</source>
        <translation> Низкий</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="13"/>
        <source> Middle</source>
        <translation> Средний</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="13"/>
        <source> High</source>
        <translation> Высокий</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="13"/>
        <source> Alarm!</source>
        <translation> Тревога!</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="49"/>
        <source>Save new task (⌘+S)</source>
        <translation>Сохранить новую задачу (⌘+S)</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="50"/>
        <source>Back to schedule (⌘+S)</source>
        <translation>Вернуться к расписанию (⌘+S)</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="51"/>
        <source>⌘ + N – add task
⌘ + D – mark as completed
⌘ + S – save and show shcedule</source>
        <translation>⌘ + N – добавить задачу
⌘ + D – отметить как выполненную
⌘ + S – сохранить и показать расписание</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="55"/>
        <source>Save new task (Ctrl+S)</source>
        <translation>Сохранить новую задачу (Ctrl+S)</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="56"/>
        <source>Back to schedule (Ctrl+S)</source>
        <translation>Вернуться к расписанию (Ctrl+S)</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="57"/>
        <source>Ctrl + N – add task
Ctrl + D – mark as completed
Ctrl + S – save and show shcedule</source>
        <translation>Ctrl + N – добавить задачу
Ctrl + D – отметить как выполненную
Ctrl + S – сохранить и показать расписание</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="90"/>
        <source>Add</source>
        <translation>Добавление</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="100"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="222"/>
        <source>Days</source>
        <comment>День</comment>
        <translation>День</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="222"/>
        <source>Days</source>
        <comment>Дня</comment>
        <translation>Дня</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="223"/>
        <source>Week</source>
        <comment>Неделю</comment>
        <translation>Неделю</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="223"/>
        <source>Weeks</source>
        <comment>Недели</comment>
        <translation>Недели</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="223"/>
        <source>Weeks</source>
        <comment>Недель</comment>
        <translation>Недель</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="224"/>
        <source>Month</source>
        <comment>Месяц</comment>
        <translation>Месяц</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="224"/>
        <source>Months</source>
        <comment>Месяца</comment>
        <translation>Месяца</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="224"/>
        <source>Months</source>
        <comment>Месяцев</comment>
        <translation>Месяцев</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="225"/>
        <source>Every</source>
        <comment>Каждый</comment>
        <translation>Каждый</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="225"/>
        <source>Every</source>
        <comment>Каждые</comment>
        <translation>Каждые</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.cpp" line="324"/>
        <source> New task </source>
        <translation> Новая задача </translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="20"/>
        <source>Редактирование задачи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop/w.taskedit.ui" line="245"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;.Helvetica Neue DeskInterface&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WTaskLine</name>
    <message>
        <location filename="../desktop/w.taskline.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskline.ui" line="96"/>
        <source>Subject</source>
        <translation>Предмет</translation>
    </message>
    <message>
        <location filename="../desktop/w.taskline.ui" line="112"/>
        <source>Task</source>
        <translation>Задача</translation>
    </message>
</context>
<context>
    <name>WTasksOnWeek</name>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="104"/>
        <source>Show completed</source>
        <translation>Показывать выполненные</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="142"/>
        <source>Show all</source>
        <translation>Показать все задачи</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="148"/>
        <source>Tasks</source>
        <translation>Задачи</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="167"/>
        <source>Show outdated</source>
        <translation>Показать просроченные</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.ui" line="173"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.cpp" line="12"/>
        <source>Add new task (⌘+N)</source>
        <translation>Добавить новую задачу (⌘+N)</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksonweek.cpp" line="14"/>
        <source>Add new task (Ctrl+N)</source>
        <translation>Добавить новую задачу (Ctrl+N)</translation>
    </message>
</context>
<context>
    <name>WTasksStub</name>
    <message>
        <location filename="../desktop/w.tasksstub.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksstub.ui" line="106"/>
        <source>
Week without tasks
</source>
        <translation>
Задач на неделю нет
</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksstub.cpp" line="14"/>
        <source>Week without tasks</source>
        <translation>Задач на неделю нет</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksstub.ui" line="131"/>
        <location filename="../desktop/w.tasksstub.cpp" line="15"/>
        <location filename="../desktop/w.tasksstub.cpp" line="26"/>
        <source>or displaying a is blocked by filter</source>
        <translation>или отображение заблокировано фильтром</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksstub.cpp" line="20"/>
        <source>All tasks completed</source>
        <translation>Все задачи выполнены</translation>
    </message>
    <message>
        <location filename="../desktop/w.tasksstub.cpp" line="25"/>
        <source>Tasks not found</source>
        <translation>Задач не найдено</translation>
    </message>
</context>
</TS>
